# lairdmobile

## Getting Started

For web:

    flutter channel beta
    flutter upgrade
    flutter config --enable-web

To build web:

    flutter run -d chrome

To run fastdriver:
  
    sudo apt-get install clang cmake ninja-build pkg-config libgtk-3-dev libblkid-dev
    run flutter pub global activate fast_flutter_driver_tool
    add ~/.pub-cache/bin to path
    run flutter config --enable-linux-desktop --enable-macos-desktop --enable-windows-desktop
    run flutter doctor
    run flutter create .

To work with App Links:
1. add '127.0.0.1 leasebuddy.io' to /etc/hosts
2. execute 'go run \*.go' in scripts/miniserver
3. get debug fingerprint with: keytool -list -v -keystore "$HOME/.android/debug.keystore" -alias androiddebugkey -storepass android -keypass android
4. adb shell 'am start -W -a android.intent.action.VIEW -c android.intent.category.BROWSABLE -d "unilinks://m.leasebuddy.io/listing"'


## Resources
* https://www.allpropertymanagement.com/resources/ask-a-pro/posts/many-employees-needed-120-unit-property/


## Architecture

* restrict widget rebuilds to backmost data updates
* Android App Links and iOS Universal Links since they launch the app without asking the user

### Routing

* The app has 5 parent Navigators managed by navman
* Most navigation is handled with push()
* Navigation within Erectors should be handled with pushReplacement() to create an internal loop for that section of logic


## Thoughts
* could put feed/notifications on explore or we could rename Inbox to Messages and put them there
* 5th nav icon could be for managing the user's current lease/history of leases


## TODO:

* semanticLabels for icons
* ability to ++/-- text size?
* read about handling background messages (notifications):
  * https://pub.dev/packages/firebase_messaging
  * https://developer.android.com/about/versions/oreo/background
  * https://www.businessofapps.com/marketplace/push-notifications/
  * https://www.djamware.com/post/5e4b26e26cdeb308204b427f/flutter-tutorial-firebase-cloud-messaging-fcm-push-notification
  * click_action is required for onResume and onLoad to fire:
    * https://firebase.google.com/docs/cloud-messaging/http-server-ref#notification-payload-support


## Sync files:

* dad/draft_listing_cache & dad/listing_cache


## Before launch:
* google maps api key setup
* check/update app fingerprints?
  * https://developer.squareup.com/docs/pos-api/cookbook/find-your-android-fingerprint
* Android app link change leasebuddy.info to prod site
  * https://simonmarquis.github.io/Android-App-Linking/#app-links
  * For ios, edit ios/Runner/Runner.entitlements
* check package and app name 
  * android: andrdoid/app/build.gradle (and other places)
  * ios: ios/Runner/Info.plist (and other places)
  * (?) can also run with flutter create --org com.yourdomain appname
* Check that all setters in the cache don't check if the pulled data is null
* Figure out how to solve this:
  * AAPT: error: attribute android:requestLegacyExternalStorage not found
	* android:requestLegacyExternalStorage="true"
	* ^ requestLegacyExternalStorage is for imagepicker android under Android API 29
* remove android:usesCleartextTraffic="true" from manifest
* image_picker recovery steps?


## Performance checks
* keepPage in pagecontroller
* const everything
