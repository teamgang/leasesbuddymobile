import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:provider/provider.dart';
import 'package:move_to_background/move_to_background.dart';

import 'package:exe/king/king.dart';
import 'package:exe/l10n/localization.dart';
import 'package:exe/nav_stack.dart';

import 'package:exe/conf.dart';

Conf defaultConf = Conf(whichApi: ApiChoices.web, mockAutoSignIn: true);
King defaultKing = King(conf: defaultConf);

class ExeApp extends StatelessWidget {
  King king;
  ExeApp({King king}) {
    if (king == null) {
      this.king = defaultKing;
    } else {
      this.king = king;
    }
    //this.king = king == null ? defaultKing : king;
  }

  @override
  Widget build(BuildContext context) {
    this.king.initialize();

    return Provider(
      create: (_) => this.king,
      lazy: false,
      child: LocalizationsWrapper(),
    );
  }
}

class LocalizationsWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var user = King.of(context).user;
    return Observer(
      builder: (_) => MaterialApp(
        //home: NavStack(),
        home: WillPopWrapper(),
        localizationsDelegates: [
          AppLocalizationsDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
        ],
        locale: Locale(user.locale, ''),
        supportedLocales: [
          Locale('en', ''),
          Locale('ru', ''),
        ],
        onGenerateTitle: (BuildContext context) =>
            AppLocalizations.of(context).title,
        debugShowCheckedModeBanner: false,
      ),
    );
  }
}

//TODO: we should probably put WillPopWrapper in BasedWrapper, then remove a
// lot of this tricky logic, such as referencing the navkeys via navman
class WillPopWrapper extends StatelessWidget {
  Widget child;
  WillPopWrapper({this.child});
  Future<bool> _onWillPop(BuildContext context) async {
    var navman = King.of(context).navman;
    if (navman.currentKey.currentState.canPop()) {
      navman.currentKey.currentState.pop();
    } else {
      print('i cannot');
      //NOTE: we don't want to background IOS since it's against rules
      if (Platform.isAndroid) {
        return (await showDialog(
              context: context,
              builder: (context) => AlertDialog(
                  title: Text('Exit?'),
                  content: Text('Do you want to close LeaseBuddy?'),
                  actions: <Widget>[
                    FlatButton(
                      onPressed: () => Navigator.of(context).pop(false),
                      child: Text('No'),
                    ),
                    FlatButton(
                      onPressed: () {
                        MoveToBackground.moveTaskToBack();
                      },
                      child: Text('Yes'),
                    ),
                  ]),
            )) ??
            false;
      }
    }
    //NOTE: always return false since we are handling pop() ourselves
    return false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        _onWillPop(context);
      },
      child: NavStack(),
    );
  }
}
