import 'dart:io';

// TODO: create OfflineMockLip for customer use
enum ApiChoices {
  mockAdmin,
  mockAnon,
  mockLandlord,
  mockOrg,
  mockTenant,
  web,
}

class Conf {
  //final String apiUrl = 'http://leasebuddy.io:3000/api/';
  String apiAddress = '';
  final String apiPort = '3000';
  String apiProtocol = '';
  final String apiSuffix = 'api';
  String apiUrl = '';
  String apiUrlNoSuffix = '';

  //final String env = 'dev';
  final String env = 'dev';
  final String logLevel = 'all';
  final String imagesUrl = 'http://127.0.0.1';
  final String mapPreviewUrl = 'http://127.0.0.1';

  final String tzFormat = 'standard';

  final double defaultLat = 40.7228;
  final double defaultLng = -74.0000;
  final double defaultLatDelta = 0.0230;
  final double defaultLngDelta = 0.0105;

  final int requestTimeout = 5;

  final int cacheTimeoutMinutes = 1;

  ApiChoices whichApi;
  bool mockAutoSignIn;

  Conf({this.whichApi, this.mockAutoSignIn = false}) {
    switch (this.env) {
      case 'dev':
        this.apiProtocol = 'http';
        // Other platforms: isFuchsia, isIOS
        if (Platform.isAndroid) {
          this.apiAddress = '10.0.2.2';
        } else if (Platform.isLinux || Platform.isWindows || Platform.isMacOS) {
          this.apiAddress = '127.0.0.1';
        }
        break;
      case 'lab':
      case 'prod':
        this.apiProtocol = 'http';
        this.apiAddress = '192.168.1.227';
        break;
      default:
        throw ('ERROR: env could not be found: $env');
    }
    this.apiUrl = '$apiProtocol://$apiAddress:$apiPort/$apiSuffix/';
    this.apiUrlNoSuffix = '$apiProtocol://$apiAddress:$apiPort/';
    print('Initiating configuration:');
    print('API choice: $whichApi');
    print('API Url: $apiUrl');
  }

  bool get isApiMock {
    switch (this.whichApi) {
      case ApiChoices.mockAdmin:
      case ApiChoices.mockAnon:
      case ApiChoices.mockLandlord:
      case ApiChoices.mockOrg:
      case ApiChoices.mockTenant:
        return true;
      case ApiChoices.web:
        return false;
      default:
        throw ('Invalid api set by conf.whichApi');
        break;
    }
  }
}
