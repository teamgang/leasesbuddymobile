import 'package:mobx/mobx.dart';

import 'package:exe/dad/cache.dart';
import 'package:exe/king/king.dart';
part 'account_cache.g.dart';

class AccountCache extends Cache<Account> {
  King king;
  String getUrl = 'users/++uuid';
  //String updateFieldUrl = 'accounts/field.update';

  AccountCache(this.king) : super(king);

  @override
  Account makeNewCacheItem() {
    return Account();
  }

  @override
  unpackItemFromApi(Map<String, Object> jsonIn, Account t) {
    t.bio = jsonIn['bio'] ?? t.bio;
    t.hasAgentAccount = jsonIn['has_agent_account'] ?? t.hasAgentAccount;
    t.username = jsonIn['username'] ?? t.username;
    t.userUuid = jsonIn['id'] ?? t.userUuid;
  }
}

class Account = AccountBase with _$Account;

abstract class AccountBase with Store {
  @observable
  String bio = 'Loading...';
  @observable
  bool hasAgentAccount = false;
  @observable
  String username = 'Loading...';
  @observable
  String userUuid = 'Loading...';

  @computed
  String get avatarAltText {
    return this.username.substring(0, 2);
  }

  // TODO: finish this
  @computed
  String get avatarUrl {
    return this.username.substring(0, 2);
  }
}
