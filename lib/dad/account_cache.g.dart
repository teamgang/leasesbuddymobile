// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'account_cache.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Account on AccountBase, Store {
  Computed<String> _$avatarAltTextComputed;

  @override
  String get avatarAltText =>
      (_$avatarAltTextComputed ??= Computed<String>(() => super.avatarAltText,
              name: 'AccountBase.avatarAltText'))
          .value;
  Computed<String> _$avatarUrlComputed;

  @override
  String get avatarUrl =>
      (_$avatarUrlComputed ??= Computed<String>(() => super.avatarUrl,
              name: 'AccountBase.avatarUrl'))
          .value;

  final _$bioAtom = Atom(name: 'AccountBase.bio');

  @override
  String get bio {
    _$bioAtom.reportRead();
    return super.bio;
  }

  @override
  set bio(String value) {
    _$bioAtom.reportWrite(value, super.bio, () {
      super.bio = value;
    });
  }

  final _$hasAgentAccountAtom = Atom(name: 'AccountBase.hasAgentAccount');

  @override
  bool get hasAgentAccount {
    _$hasAgentAccountAtom.reportRead();
    return super.hasAgentAccount;
  }

  @override
  set hasAgentAccount(bool value) {
    _$hasAgentAccountAtom.reportWrite(value, super.hasAgentAccount, () {
      super.hasAgentAccount = value;
    });
  }

  final _$usernameAtom = Atom(name: 'AccountBase.username');

  @override
  String get username {
    _$usernameAtom.reportRead();
    return super.username;
  }

  @override
  set username(String value) {
    _$usernameAtom.reportWrite(value, super.username, () {
      super.username = value;
    });
  }

  final _$userUuidAtom = Atom(name: 'AccountBase.userUuid');

  @override
  String get userUuid {
    _$userUuidAtom.reportRead();
    return super.userUuid;
  }

  @override
  set userUuid(String value) {
    _$userUuidAtom.reportWrite(value, super.userUuid, () {
      super.userUuid = value;
    });
  }

  @override
  String toString() {
    return '''
bio: ${bio},
hasAgentAccount: ${hasAgentAccount},
username: ${username},
userUuid: ${userUuid},
avatarAltText: ${avatarAltText},
avatarUrl: ${avatarUrl}
    ''';
  }
}
