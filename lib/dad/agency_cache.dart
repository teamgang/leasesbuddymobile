import 'package:mobx/mobx.dart';

import 'package:exe/dad/cache.dart';
import 'package:exe/king/king.dart';
part 'agency_cache.g.dart';

class AgencyCache extends Cache<Agency> {
  King king;
  String getUrl = 'agency/get';
  String updateFieldUrl = 'agency/field.update';

  AgencyCache(this.king) : super(king);

  @override
  Agency makeNewCacheItem() {
    return Agency();
  }

  unpackItemFromApi(Map<String, Object> jsonIn, Agency t) {
    t.agencyUuid = jsonIn['agencyUuid'] ?? t.agencyUuid;
    t.bio = jsonIn['bio'] ?? t.bio;
    t.name = jsonIn['name'] ?? t.name;
    t.email = jsonIn['email'] ?? t.email;
    t.phone = jsonIn['phone'] ?? t.phone;
    t.reviewCount = jsonIn['reviewCount'] ?? t.reviewCount;
    t.reviewSum = jsonIn['reviewSum'] ?? t.reviewSum;
  }
}

class Agency = AgencyBase with _$Agency;

abstract class AgencyBase with Store {
  @observable
  String agencyUuid = 'Loading...';
  @observable
  String bio = 'Loading...';
  @observable
  String email = 'Loading...';
  @observable
  String name = 'Loading...';
  @observable
  String phone = 'Loading...';

  @observable
  int reviewCount = 0;
  @observable
  int reviewSum = 0;

  @computed
  double get reviewAverage {
    double average = reviewSum / reviewCount;
    return average;
  }

  @computed
  bool get hasReviews {
    if (reviewCount > 0) {
      return true;
    }
    return false;
  }
}
