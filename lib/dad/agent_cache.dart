import 'package:mobx/mobx.dart';

import 'package:exe/dad/cache.dart';
import 'package:exe/king/king.dart';
part 'agent_cache.g.dart';

class AgentCache extends Cache<Agent> {
  King king;
  String getUrl = 'agents/show/++uuid';

  AgentCache(this.king) : super(king);

  @override
  Agent makeNewCacheItem() {
    return Agent();
  }

  unpackItemFromApi(Map<String, Object> jsonIn, Agent t) {
    t.agentUuid = jsonIn['id'] ?? t.agentUuid;
    t.bio = jsonIn['bio'] ?? t.bio;
    t.name = jsonIn['name'] ?? t.name;
    t.email = jsonIn['email'] ?? t.email;
    t.phone = jsonIn['phone'] ?? t.phone;
    t.reviewCount = jsonIn['reviewCount'] ?? t.reviewCount;
    t.reviewSum = jsonIn['reviewSum'] ?? t.reviewSum;
  }
}

class Agent = AgentBase with _$Agent;

abstract class AgentBase with Store {
  @observable
  String agentUuid = 'Loading...';
  @observable
  String bio = 'Loading...';
  @observable
  String email = 'Loading...';
  @observable
  String name = 'Loading...';
  @observable
  String phone = 'Loading...';

  @observable
  int reviewCount = 0;
  @observable
  int reviewSum = 0;

  @computed
  double get reviewAverage {
    double average = reviewSum / reviewCount;
    return average;
  }

  @computed
  bool get hasReviews {
    if (reviewCount > 0) {
      return true;
    }
    return false;
  }
}
