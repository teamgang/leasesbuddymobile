// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'agent_cache.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Agent on AgentBase, Store {
  Computed<double> _$reviewAverageComputed;

  @override
  double get reviewAverage =>
      (_$reviewAverageComputed ??= Computed<double>(() => super.reviewAverage,
              name: 'AgentBase.reviewAverage'))
          .value;
  Computed<bool> _$hasReviewsComputed;

  @override
  bool get hasReviews => (_$hasReviewsComputed ??=
          Computed<bool>(() => super.hasReviews, name: 'AgentBase.hasReviews'))
      .value;

  final _$agentUuidAtom = Atom(name: 'AgentBase.agentUuid');

  @override
  String get agentUuid {
    _$agentUuidAtom.reportRead();
    return super.agentUuid;
  }

  @override
  set agentUuid(String value) {
    _$agentUuidAtom.reportWrite(value, super.agentUuid, () {
      super.agentUuid = value;
    });
  }

  final _$bioAtom = Atom(name: 'AgentBase.bio');

  @override
  String get bio {
    _$bioAtom.reportRead();
    return super.bio;
  }

  @override
  set bio(String value) {
    _$bioAtom.reportWrite(value, super.bio, () {
      super.bio = value;
    });
  }

  final _$emailAtom = Atom(name: 'AgentBase.email');

  @override
  String get email {
    _$emailAtom.reportRead();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.reportWrite(value, super.email, () {
      super.email = value;
    });
  }

  final _$nameAtom = Atom(name: 'AgentBase.name');

  @override
  String get name {
    _$nameAtom.reportRead();
    return super.name;
  }

  @override
  set name(String value) {
    _$nameAtom.reportWrite(value, super.name, () {
      super.name = value;
    });
  }

  final _$phoneAtom = Atom(name: 'AgentBase.phone');

  @override
  String get phone {
    _$phoneAtom.reportRead();
    return super.phone;
  }

  @override
  set phone(String value) {
    _$phoneAtom.reportWrite(value, super.phone, () {
      super.phone = value;
    });
  }

  final _$reviewCountAtom = Atom(name: 'AgentBase.reviewCount');

  @override
  int get reviewCount {
    _$reviewCountAtom.reportRead();
    return super.reviewCount;
  }

  @override
  set reviewCount(int value) {
    _$reviewCountAtom.reportWrite(value, super.reviewCount, () {
      super.reviewCount = value;
    });
  }

  final _$reviewSumAtom = Atom(name: 'AgentBase.reviewSum');

  @override
  int get reviewSum {
    _$reviewSumAtom.reportRead();
    return super.reviewSum;
  }

  @override
  set reviewSum(int value) {
    _$reviewSumAtom.reportWrite(value, super.reviewSum, () {
      super.reviewSum = value;
    });
  }

  @override
  String toString() {
    return '''
agentUuid: ${agentUuid},
bio: ${bio},
email: ${email},
name: ${name},
phone: ${phone},
reviewCount: ${reviewCount},
reviewSum: ${reviewSum},
reviewAverage: ${reviewAverage},
hasReviews: ${hasReviews}
    ''';
  }
}
