import 'dart:async';

import 'package:exe/king/king.dart';
import 'package:exe/lip/lip.dart';

// TODO: if multiple requests for the same item are sent at once, it might be
// good to have logic such that the latest version of the request wins. This
// is probably best achieved by appending a version number to every object. Or,
// this error could be ignored.

// Cache is used as a local single source of truth for data from web requests.
// NEVER access items managed by dad without also instantiating a reference
// to the each item in the cache.
// As of now, the only ways to access dad items is to getItem(), getItemFresh()
// and putItemFromNew()
abstract class Cache<T> {
  // TODO: ability to cancel requests?
  // TODO: implement cachedTimes
  Map<String, T> cachedItems = {};
  Map<String, int> cachedTimes = {};
  Map<String, int> erroredTimes = {};
  Set<String> flaggedForUpdate = {};
  String getUrl;
  King king;
  Set<String> requestInProgress = {};
  String updateFieldUrl;

  Cache(this.king);

  T makeNewCacheItem() {
    throw UnimplementedError;
  }

  void unpackItemFromApi(Map<String, Object> jsonIn, T target) {
    throw UnimplementedError;
  }

  void getItemWithApireq(
    ApiRequest apireq, {
    Function unpackFunction = null,
  }) async {
    String uuid = apireq.uuid;
    requestInProgress.add(uuid);
    ApiResponse ares = await this.king.lip.api(
          apireq.urlRegexed,
          payload: apireq.payload,
          method: apireq.method,
        );
    requestInProgress.remove(uuid);

    this.flaggedForUpdate.remove(uuid);
    this.erroredTimes.remove(uuid);

    if (ares.errored) {
      this.flaggedForUpdate.add(uuid);
      this.erroredTimes[uuid] = DateTime.now().millisecondsSinceEpoch;
    }

    if (ares.isOk()) {
      var target = this.getCachedItem(uuid);
      if (unpackFunction == null) {
        //TODO: deprecate this?
        this.unpackItemFromApi(ares.body, target);
      } else {
        unpackFunction(ares.body, target);
      }
      this.cachedTimes[uuid] = DateTime.now().millisecondsSinceEpoch;
    } else {
      // TODO: display error to user?
      //this.king.showSnackBarRequestFailed();
    }

    //return ares;
  }

  void putItemFromNew(String uuid, T item) {
    cachedItems[uuid] = item;
    this.cachedTimes[uuid] = DateTime.now().millisecondsSinceEpoch;
  }

  //TODO: DEPRECATING
  T getItem(String uuid, {Map<String, Object> payload}) {
    if (this.cachedItems.containsKey(uuid)) {
      if (this.shouldUpdate(uuid)) {
        this.getItemFromApi(uuid, payload: payload);
      }
    } else {
      this.cachedItems[uuid] = makeNewCacheItem();
      this.getItemFromApi(uuid, payload: payload);
    }
    return this.cachedItems[uuid];
  }

  //TODO: DEPRECATING
  void getItemFromApi(
    String uuid, {
    Map<String, Object> payload,
  }) async {
    if (uuid == '' || uuid == 'Loading...') {
      return;
    }

    String url = '${this.getUrl}'.replaceAll(
      new RegExp(r'\+\+uuid'),
      '${uuid}',
    );

    //if (payload == null) {
    //payload = {
    //'uuid': uuid,
    //};
    //}

    requestInProgress.add(uuid);
    ApiResponse ares = await this.king.lip.api(
          url,
          payload: payload,
          method: HttpMethod.get,
        );
    requestInProgress.remove(uuid);

    this.flaggedForUpdate.remove(uuid);
    this.erroredTimes.remove(uuid);

    if (ares.errored) {
      this.flaggedForUpdate.add(uuid);
      this.erroredTimes[uuid] = DateTime.now().millisecondsSinceEpoch;
    }

    if (ares.isOk()) {
      var target = this.cachedItems[uuid];
      this.unpackItemFromApi(ares.body, target);
      this.cachedTimes[uuid] = DateTime.now().millisecondsSinceEpoch;
    } else {
      // TODO: display error to user?
      //this.king.showSnackBarRequestFailed();
    }
  }

  ApiResponse tryUpdateCacheItemSync(
    ApiRequest apireq, {
    Function unpackFunction = null,
  }) {
    // get first to make sure the cacheditem exists before we try and update it
    this.getCachedItem(apireq.uuid);
    if (this.shouldUpdate(apireq.uuid)) {
      //TODO: should I pass unpackFunction? (I think so)
      this.getItemWithApireq(apireq);
    }
  }

  Future<ApiResponse> tryUpdateCacheItem(
    ApiRequest apireq, {
    Function unpackFunction = null,
  }) async {
    // get first to make sure the cacheditem exists before we try and update it
    this.getCachedItem(apireq.uuid);
    if (this.shouldUpdate(apireq.uuid)) {
      //TODO: should I pass unpackFunction? (I think so)
      this.getItemWithApireq(apireq);
    }
  }

  T getCachedItem(String uuid) {
    if (!this.cachedItems.containsKey(uuid)) {
      this.cachedItems[uuid] = makeNewCacheItem();
    }
    return this.cachedItems[uuid];
  }

  T getItemFresh(
    String uuid, {
    Map<String, Object> payload,
  }) {
    this.flagForUpdate(uuid);
    return this.getItem(uuid, payload: payload);
  }

  bool shouldUpdate(String uuid) {
    if (!cachedItems.containsKey(uuid)) {
      print('cachedItem does not exist');
      return true;
    }

    if (!cachedTimes.containsKey(uuid)) {
      print('cachedTime does not exist');
      return true;
    }

    if (uuid == '' || uuid == 'Loading...') {
      print(uuid);
      return false;
    }

    if (requestInProgress.contains(uuid)) {
      print('req in progress');
      return false;
    }

    if (flaggedForUpdate.contains(uuid)) {
      print('flagged');
      return true;
    }

    int timeNow = DateTime.now().millisecondsSinceEpoch;
    int timeCached = this.cachedTimes[uuid] ?? timeNow;
    int timeErrored = this.erroredTimes[uuid] ?? timeNow;
    //TODO: increase times here
    if ((timeNow - timeErrored > 2000) || (timeNow - timeCached > 10000)) {
      print('time now');
      return true;
    }

    print('should not update');
    return false;
  }

  flagForUpdate(String uuid) {
    flaggedForUpdate.add(uuid);
  }
}
