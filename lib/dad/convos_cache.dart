import 'package:mobx/mobx.dart';

import 'package:exe/dad/cache.dart';
import 'package:exe/king/king.dart';
part 'convos_cache.g.dart';

class ConvosCache extends Cache<Convos> {
  King king;
  String getUrl;

  ConvosCache(this.king, {this.getUrl}) : super(king);

  @override
  Convos makeNewCacheItem() {
    return Convos();
  }

  @override
  void unpackItemFromApi(Map<String, Object> jsonIn, Convos target) {
    target.convos.clear();
    for (var item in jsonIn['convos']) {
      var convo = Convo.fromJson(item);
      target.convos.add(convo);
    }
  }
}

class Convos = ConvosBase with _$Convos;

abstract class ConvosBase with Store {
  @observable
  var convos = ObservableList<Convo>();
}

class Convo {
  String agentUuid;
  String convoUuid;
  String lastMessage;
  int lastMessageTime;
  String lastMessageUsername;
  String listingUuid;
  // TODO might not need participants, but let's try and design around it for
  // possible future functionality
  List<String> participants;
  String title;
  int unreadCount;

  Convo({
    this.agentUuid,
    this.convoUuid,
    this.lastMessage,
    this.lastMessageTime,
    this.lastMessageUsername,
    this.listingUuid,
    this.participants,
    this.title,
    this.unreadCount,
  });

  factory Convo.fromJson(Map<String, dynamic> jsonIn) => Convo(
        agentUuid: jsonIn['agentUuid'],
        convoUuid: jsonIn['convoUuid'],
        lastMessage: jsonIn['lastMessage'],
        lastMessageTime: jsonIn['lastMessageTime'],
        lastMessageUsername: jsonIn['lastMessageUsername'],
        listingUuid: jsonIn['listingUuid'],
        participants: jsonIn['participants'],
        title: jsonIn['title'],
        unreadCount: jsonIn['unreadCount'],
      );

  String get lastMessageTimeReadable {
    // TODO: make this a real function
    return 'Tuesday';
  }
}
