// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'convos_cache.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Convos on ConvosBase, Store {
  final _$convosAtom = Atom(name: 'ConvosBase.convos');

  @override
  ObservableList<Convo> get convos {
    _$convosAtom.reportRead();
    return super.convos;
  }

  @override
  set convos(ObservableList<Convo> value) {
    _$convosAtom.reportWrite(value, super.convos, () {
      super.convos = value;
    });
  }

  @override
  String toString() {
    return '''
convos: ${convos}
    ''';
  }
}
