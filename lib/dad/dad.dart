import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:exe/dad/account_cache.dart';
import 'package:exe/dad/agency_cache.dart';
import 'package:exe/dad/agent_cache.dart';
import 'package:exe/dad/convos_cache.dart';
import 'package:exe/dad/google_place_cache.dart';
import 'package:exe/dad/listing_cache.dart';
import 'package:exe/dad/managed_listing_transients_cache.dart';
import 'package:exe/dad/mapsac_cacher.dart';
import 'package:exe/dad/messages_cache.dart';
import 'package:exe/dad/saved_listing_uuids_cache.dart';
import 'package:exe/king/king.dart';
import 'package:exe/lip/lip.dart';

export 'package:exe/dad/account_cache.dart';
export 'package:exe/dad/agency_cache.dart';
export 'package:exe/dad/agent_cache.dart';
export 'package:exe/dad/convos_cache.dart';
export 'package:exe/dad/google_place_cache.dart';
export 'package:exe/dad/listing_cache.dart';
export 'package:exe/dad/managed_listing_transients_cache.dart';
export 'package:exe/dad/mapsac_cacher.dart';
export 'package:exe/dad/messages_cache.dart';
export 'package:exe/dad/saved_listing_uuids_cache.dart';

// data acquisition director (ngl i forgot the original acronym)
// data advisor might be better :thinking:
class Dad {
  King king;

  AccountCache accountCache;
  AgencyCache agencyCache;
  AgentCache agentCache;
  ConvosCache convosCacheAgent;
  ConvosCache convosCacheTenant;
  GooglePlaceCache googlePlaceCache;
  ListingCache listingCache;
  ManagedListingTransientsCache managedListingTransientsCache;
  MapsacPredictionListCache mapsacPredictionListCache;
  MessagesCache messagesCache;
  SavedListingUuidsCache savedListingUuidsCache;

  Dad(this.king) {
    String lbUrl = this.king.conf.apiUrl;
    String lbUrlNoSuffix = this.king.conf.apiUrlNoSuffix;

    this.accountCache = new AccountCache(this.king);

    this.agencyCache = new AgencyCache(this.king);

    this.agentCache = new AgentCache(this.king);

    this.convosCacheAgent = new ConvosCache(
      this.king,
      getUrl: 'agents/conversations',
    );

    this.convosCacheTenant =
        new ConvosCache(this.king, getUrl: 'tenants/conversations');

    this.googlePlaceCache = new GooglePlaceCache(this.king);

    this.listingCache = new ListingCache(this.king);

    this.managedListingTransientsCache =
        new ManagedListingTransientsCache(this.king);

    this.mapsacPredictionListCache = MapsacPredictionListCache(this.king,
        getMethod: HttpMethod.post,
        //TODO: fix this
        getUrl: '${lbUrlNoSuffix}locations/autocomplete');

    this.messagesCache = new MessagesCache(this.king);

    this.savedListingUuidsCache = new SavedListingUuidsCache(this.king);
  }

  static Dad of(BuildContext context) {
    return Provider.of<King>(context, listen: false).dad;
  }
}
