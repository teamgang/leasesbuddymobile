import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mobx/mobx.dart';

import 'package:exe/dad/cache.dart';
import 'package:exe/king/king.dart';
import 'package:exe/shared/explore_query.dart';

part 'explore_cacher.g.dart';

class ExploreListCacher {
  ExploreListCache cache;
  ExploreQuery equery;

  ExploreListCacher(King king, ExploreQuery equery) {
    this.cache = ExploreListCache(king);
    this.equery = equery;
  }

  ExploreList getExploreList() {
    print('+++++++++++ running getitem+++++++++++');
    var exploreList = this.cache.getItem(
          this.equery.generateUuid(),
          payload: this.equery.generateFilterPayload(),
        );
    return exploreList;
  }
}

class ExploreListCache extends Cache<ExploreList> {
  King king;
  String getUrl = 'explore/query';

  ExploreListCache(this.king) : super(king);

  @override
  ExploreList makeNewCacheItem() {
    return ExploreList();
  }

  @override
  void unpackItemFromApi(Map<String, Object> jsonIn, ExploreList target) {
    target.tacs.clear();
    for (var item in jsonIn['tacs']) {
      var tac = Tac.fromJson(item);
      target.tacs.add(tac);
    }
  }
}

class ExploreList = ExploreListBase with _$ExploreList;

abstract class ExploreListBase with Store {
  @observable
  var tacs = ObservableList<Tac>();
}

class Tac {
  String address = "Loading...";
  double lat;
  double lng;
  String title;
  String uuid;

  Tac({
    this.address,
    this.lat,
    this.lng,
    this.title,
    this.uuid,
  });

  factory Tac.fromJson(Map<String, dynamic> jsonIn) => Tac(
        address: jsonIn['address'],
        lat: jsonIn['lat'],
        lng: jsonIn['lng'],
        title: jsonIn['title'],
        uuid: jsonIn['uuid'],
      );
}
