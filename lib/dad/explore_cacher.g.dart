// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'explore_cacher.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ExploreList on ExploreListBase, Store {
  final _$tacsAtom = Atom(name: 'ExploreListBase.tacs');

  @override
  ObservableList<Tac> get tacs {
    _$tacsAtom.reportRead();
    return super.tacs;
  }

  @override
  set tacs(ObservableList<Tac> value) {
    _$tacsAtom.reportWrite(value, super.tacs, () {
      super.tacs = value;
    });
  }

  @override
  String toString() {
    return '''
tacs: ${tacs}
    ''';
  }
}
