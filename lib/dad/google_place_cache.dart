import 'package:mobx/mobx.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:exe/dad/cache.dart';
import 'package:exe/king/king.dart';
import 'package:exe/lip/lip.dart';
part 'google_place_cache.g.dart';

class GooglePlaceCache extends Cache<GooglePlace> {
  King king;
  String getUrl = 'mapsac/lookup.byPlaceId';

  GooglePlaceCache(this.king) : super(king);

  @override
  GooglePlace makeNewCacheItem() {
    return GooglePlace();
  }

  @override
  unpackItemFromApi(Map<String, Object> jsonIn, GooglePlace target) {
    target.placeId = jsonIn['place_id'];
    target.lat = jsonIn['lat'];
    target.long = jsonIn['lng'];
    target.address = jsonIn['formatted_address'];
  }

  getPlaceSync({String placeId}) {
    //TODO: change this
    String url = '${this.king.conf.apiUrl}locations/place_id';
    Map<String, Object> payload = {'place_id': placeId};
    var apireq = ApiRequest(
      method: HttpMethod.get,
      payload: payload,
      url: url,
      uuid: placeId,
    );

    this.tryUpdateCacheItem(
      apireq,
    );
    return this.getCachedItem(apireq.uuid);
  }
}

class GooglePlace = GooglePlaceBase with _$GooglePlace;

abstract class GooglePlaceBase with Store {
  @observable
  String placeId = '';

  @observable
  double lat = 0;

  @observable
  double long = 0;

  @observable
  String address = '';

  @computed
  LatLng get latLng {
    return LatLng(this.lat, this.long);
  }

  @computed
  CameraPosition get camera {
    return CameraPosition(
      target: this.latLng,
      zoom: DEFAULT_ZOOM,
    );
  }
}
