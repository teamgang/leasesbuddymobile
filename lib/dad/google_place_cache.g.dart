// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'google_place_cache.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$GooglePlace on GooglePlaceBase, Store {
  Computed<LatLng> _$latLngComputed;

  @override
  LatLng get latLng => (_$latLngComputed ??=
          Computed<LatLng>(() => super.latLng, name: 'GooglePlaceBase.latLng'))
      .value;
  Computed<CameraPosition> _$cameraComputed;

  @override
  CameraPosition get camera =>
      (_$cameraComputed ??= Computed<CameraPosition>(() => super.camera,
              name: 'GooglePlaceBase.camera'))
          .value;

  final _$placeIdAtom = Atom(name: 'GooglePlaceBase.placeId');

  @override
  String get placeId {
    _$placeIdAtom.reportRead();
    return super.placeId;
  }

  @override
  set placeId(String value) {
    _$placeIdAtom.reportWrite(value, super.placeId, () {
      super.placeId = value;
    });
  }

  final _$latAtom = Atom(name: 'GooglePlaceBase.lat');

  @override
  double get lat {
    _$latAtom.reportRead();
    return super.lat;
  }

  @override
  set lat(double value) {
    _$latAtom.reportWrite(value, super.lat, () {
      super.lat = value;
    });
  }

  final _$longAtom = Atom(name: 'GooglePlaceBase.long');

  @override
  double get long {
    _$longAtom.reportRead();
    return super.long;
  }

  @override
  set long(double value) {
    _$longAtom.reportWrite(value, super.long, () {
      super.long = value;
    });
  }

  final _$addressAtom = Atom(name: 'GooglePlaceBase.address');

  @override
  String get address {
    _$addressAtom.reportRead();
    return super.address;
  }

  @override
  set address(String value) {
    _$addressAtom.reportWrite(value, super.address, () {
      super.address = value;
    });
  }

  @override
  String toString() {
    return '''
placeId: ${placeId},
lat: ${lat},
long: ${long},
address: ${address},
latLng: ${latLng},
camera: ${camera}
    ''';
  }
}
