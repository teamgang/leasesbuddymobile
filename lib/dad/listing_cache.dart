import 'package:mobx/mobx.dart';
import 'package:meta/meta.dart';
import 'package:uuid/uuid.dart';

import 'package:exe/conf.dart';
import 'package:exe/dad/cache.dart';
import 'package:exe/king/king.dart';
part 'listing_cache.g.dart';

//TODO: fill these in
const int DRAFT = 0;

class ListingCache extends Cache<Listing> {
  King king;
  String getUrl = 'tenants/listings/++uuid';

  ListingCache(this.king) : super(king);

  Listing makeNewDraftListing() {
    Listing listing = Listing();
    listing.listingUuid = Uuid().v4();
    listing.isSavedInApi = false;
    this.putItemFromNew(listing.listingUuid, listing);
    return listing;
  }

  @override
  Listing makeNewCacheItem() {
    return Listing();
  }

  unpackItemFromApi(Map<String, Object> jsonIn, Listing t) {
    //TODO: if a single field fails to parse, the rest of the function will fail
    // maybe wrap each line in a try catch?
    t.address = jsonIn['address'] ?? t.address;
    t.agencyUuid = jsonIn['agencyUuid'] ?? t.agencyUuid;
    t.agentUuid = jsonIn['agentUuid'] ?? t.agentUuid;
    t.agentIsOwner = jsonIn['agentIsOwner'] ?? t.agentIsOwner;
    t.bathroomsFull = jsonIn['full_baths'] ?? t.bathroomsFull;
    t.bathroomsHalf = jsonIn['half_baths'] ?? t.bathroomsHalf;
    t.bedrooms = jsonIn['bedrooms'] ?? t.bedrooms;
    t.city = jsonIn['city'] ?? t.city;
    t.deposit = jsonIn['deposit'] ?? t.deposit;
    t.hasAgency = jsonIn['hasAgency'] ?? t.hasAgency;
    t.housingType = jsonIn['housing_type'] ?? t.housingType;
    t.lat = jsonIn['lat'] ?? t.lat;
    t.listingUuid = jsonIn['id'] ?? t.listingUuid;
    t.lng = jsonIn['lng'] ?? t.lng;
    //TODO: remove interior_
    t.description = jsonIn['interior_description'] ?? t.description;
    t.monthlyRent = jsonIn['monthly_rent'] ?? t.monthlyRent;
    t.parkingType = jsonIn['parking_type'] ?? t.parkingType;
    t.shortInfo = jsonIn['shortInfo'] ?? t.shortInfo;
    t.squareFeet = jsonIn['square_feet'] ?? t.squareFeet;
    t.status = jsonIn['status'] ?? t.status;
    t.streetOne = jsonIn['streetOne'] ?? t.streetOne;
    t.title = jsonIn['title'] ?? t.title;

    try {
      t.dateAvailable =
          DateTime.parse(jsonIn['date_available']) ?? t.dateAvailable;
    } on Exception catch (e, s) {
      throw e;
    } catch (error) {
      if (error.toString().contains('Must not be null')) {
        // Do nothing
      }
    }

    if (jsonIn['pictures'] != null) {
      t.pictures.clear();
      t.pictures.addAll(jsonIn['pictures']);
    }

    t.isSavedInApi = true;
    t.completedSteps.clear();
    for (String step in jsonIn['completed_steps']) {
      t.completedSteps.add(step);
    }
  }
}

class Listing = ListingBase with _$Listing;

abstract class ListingBase with Store {
  DateTime dateAvailable = DateTime.now();

  @observable
  int carrousselIndex = 0;

  @observable
  String address = 'Loading...';
  @observable
  String agencyUuid = 'Loading...';
  @observable
  String agentUuid = 'Loading...';
  @observable
  bool agentIsOwner = false;
  @observable
  int bathroomsFull = 0;
  @observable
  int bathroomsHalf = 0;
  @observable
  int bedrooms = 0;
  @observable
  String city = 'Loading...';
  @observable
  ObservableSet<String> completedSteps = ObservableSet<String>();
  @observable
  int deposit = 0;
  @observable
  bool hasAgency = false;
  @observable
  String housingType = '';
  @observable
  bool isSavedInApi = false;
  @observable
  String parkingType = '';

  @observable
  ObservableList<String> pictures = ObservableList<String>();
  @observable
  double lat = 0;
  @observable
  String listingUuid = 'Loading...';
  @observable
  double lng = 0;
  @observable
  String description = 'Loading...';
  @observable
  int monthlyRent = 0;
  @observable
  String shortInfo = 'Loading...';
  @observable
  int squareFeet = 0;
  @observable
  int status = 0;
  @observable
  String streetOne = 'Loading...';
  @observable
  String title = '';

  @computed
  String get printMonthlyRent {
    // TODO: localize
    return '\$${this.monthlyRent}/mo';
  }

  @computed
  String get printSize {
    // TODO: localize square feet or square meters
    return '${this.squareFeet} ft\u00B2';
  }

  @computed
  String get printBathrooms {
    double bathrooms = (this.bathroomsFull + this.bathroomsHalf) / 2;
    return '${bathrooms.toStringAsFixed(1)}';
  }

  @computed
  String get printAddressSectionOne {
    // TODO: localize and format
    return '${this.streetOne}';
  }

  @computed
  String get printAddressSectionTwo {
    // TODO: localize and add state and zip
    return '${this.city}';
  }

  @computed
  String get carrousselIndexStatus {
    return '${this.carrousselIndex + 1}/${this.pictures.length}';
  }

  @computed
  String get mapPreviewPicture {
    return '${this.listingUuid}';
  }

  @computed
  bool get isLoaded {
    if (this.listingUuid == 'Loading...') {
      return false;
    } else {
      return true;
    }
  }

  @computed
  String get firstPicture {
    //TODO: correct picture url
    return 'picture/goes/here';
  }

  @computed
  bool get isDraft {
    if (this.status == DRAFT) {
      return true;
    }
    return false;
  }

  //TODO: come up with a better way to check if these fields are completed - maybe use the backend?
  @computed
  bool get validDescription {
    if (this.description.length < ListingConstants.minDescription &&
        this.description.length > ListingConstants.maxDescription) {
      return false;
    }
    return true;
  }

  @computed
  bool get validTitle {
    if (this.title.length < ListingConstants.minTitle &&
        this.title.length > ListingConstants.maxTitle) {
      return false;
    }
    return true;
  }

  @computed
  bool get validRoomsAndSpaces {
    if (this.bedrooms < ListingConstants.minBedrooms &&
        this.bedrooms > ListingConstants.maxBedrooms) {
      return false;
    }
    if (this.bathroomsFull < ListingConstants.minBathrooms &&
        this.bathroomsFull > ListingConstants.maxBathrooms) {
      return false;
    }
    if (this.bathroomsHalf < ListingConstants.minBathrooms &&
        this.bathroomsHalf > ListingConstants.maxBathrooms) {
      return false;
    }
    return true;
  }

  Set<String> getUpdatedCompletedSteps(String currentStep) {
    Set<String> updatedCompletedSteps = Set.from(this.completedSteps);
    updatedCompletedSteps.add(currentStep);
    return updatedCompletedSteps;
  }

  List<String> getUpdatedCompletedStepsAsList(String currentStep) {
    var completedStepsSet = this.getUpdatedCompletedSteps(currentStep);
    return List.from(completedStepsSet);
  }
}

class ListingConstants {
  static const int maxAddressLength = 240;
  static const int minAddressLength = 4;
  static const int maxBedrooms = 9;
  static const int minBedrooms = 0;
  static const int maxBathrooms = 9;
  static const int minBathrooms = 0;
  static const int maxDeposit = 99999;
  static const int minDeposit = 0;
  static const int maxDescription = 1000;
  static const int minDescription = 20;
  static const int maxMonthlyRent = 999999999999999;
  static const int minMonthlyRent = 0;
  static const int maxSize = 999999;
  static const int minSize = 0;
  static const int maxTitle = 100;
  static const int minTitle = 3;
}
