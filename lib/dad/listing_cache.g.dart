// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'listing_cache.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Listing on ListingBase, Store {
  Computed<String> _$printMonthlyRentComputed;

  @override
  String get printMonthlyRent => (_$printMonthlyRentComputed ??=
          Computed<String>(() => super.printMonthlyRent,
              name: 'ListingBase.printMonthlyRent'))
      .value;
  Computed<String> _$printSizeComputed;

  @override
  String get printSize =>
      (_$printSizeComputed ??= Computed<String>(() => super.printSize,
              name: 'ListingBase.printSize'))
          .value;
  Computed<String> _$printBathroomsComputed;

  @override
  String get printBathrooms =>
      (_$printBathroomsComputed ??= Computed<String>(() => super.printBathrooms,
              name: 'ListingBase.printBathrooms'))
          .value;
  Computed<String> _$printAddressSectionOneComputed;

  @override
  String get printAddressSectionOne => (_$printAddressSectionOneComputed ??=
          Computed<String>(() => super.printAddressSectionOne,
              name: 'ListingBase.printAddressSectionOne'))
      .value;
  Computed<String> _$printAddressSectionTwoComputed;

  @override
  String get printAddressSectionTwo => (_$printAddressSectionTwoComputed ??=
          Computed<String>(() => super.printAddressSectionTwo,
              name: 'ListingBase.printAddressSectionTwo'))
      .value;
  Computed<String> _$carrousselIndexStatusComputed;

  @override
  String get carrousselIndexStatus => (_$carrousselIndexStatusComputed ??=
          Computed<String>(() => super.carrousselIndexStatus,
              name: 'ListingBase.carrousselIndexStatus'))
      .value;
  Computed<String> _$mapPreviewPictureComputed;

  @override
  String get mapPreviewPicture => (_$mapPreviewPictureComputed ??=
          Computed<String>(() => super.mapPreviewPicture,
              name: 'ListingBase.mapPreviewPicture'))
      .value;
  Computed<bool> _$isLoadedComputed;

  @override
  bool get isLoaded => (_$isLoadedComputed ??=
          Computed<bool>(() => super.isLoaded, name: 'ListingBase.isLoaded'))
      .value;
  Computed<String> _$firstPictureComputed;

  @override
  String get firstPicture =>
      (_$firstPictureComputed ??= Computed<String>(() => super.firstPicture,
              name: 'ListingBase.firstPicture'))
          .value;
  Computed<bool> _$isDraftComputed;

  @override
  bool get isDraft => (_$isDraftComputed ??=
          Computed<bool>(() => super.isDraft, name: 'ListingBase.isDraft'))
      .value;
  Computed<bool> _$validDescriptionComputed;

  @override
  bool get validDescription => (_$validDescriptionComputed ??= Computed<bool>(
          () => super.validDescription,
          name: 'ListingBase.validDescription'))
      .value;
  Computed<bool> _$validTitleComputed;

  @override
  bool get validTitle =>
      (_$validTitleComputed ??= Computed<bool>(() => super.validTitle,
              name: 'ListingBase.validTitle'))
          .value;
  Computed<bool> _$validRoomsAndSpacesComputed;

  @override
  bool get validRoomsAndSpaces => (_$validRoomsAndSpacesComputed ??=
          Computed<bool>(() => super.validRoomsAndSpaces,
              name: 'ListingBase.validRoomsAndSpaces'))
      .value;

  final _$carrousselIndexAtom = Atom(name: 'ListingBase.carrousselIndex');

  @override
  int get carrousselIndex {
    _$carrousselIndexAtom.reportRead();
    return super.carrousselIndex;
  }

  @override
  set carrousselIndex(int value) {
    _$carrousselIndexAtom.reportWrite(value, super.carrousselIndex, () {
      super.carrousselIndex = value;
    });
  }

  final _$addressAtom = Atom(name: 'ListingBase.address');

  @override
  String get address {
    _$addressAtom.reportRead();
    return super.address;
  }

  @override
  set address(String value) {
    _$addressAtom.reportWrite(value, super.address, () {
      super.address = value;
    });
  }

  final _$agencyUuidAtom = Atom(name: 'ListingBase.agencyUuid');

  @override
  String get agencyUuid {
    _$agencyUuidAtom.reportRead();
    return super.agencyUuid;
  }

  @override
  set agencyUuid(String value) {
    _$agencyUuidAtom.reportWrite(value, super.agencyUuid, () {
      super.agencyUuid = value;
    });
  }

  final _$agentUuidAtom = Atom(name: 'ListingBase.agentUuid');

  @override
  String get agentUuid {
    _$agentUuidAtom.reportRead();
    return super.agentUuid;
  }

  @override
  set agentUuid(String value) {
    _$agentUuidAtom.reportWrite(value, super.agentUuid, () {
      super.agentUuid = value;
    });
  }

  final _$agentIsOwnerAtom = Atom(name: 'ListingBase.agentIsOwner');

  @override
  bool get agentIsOwner {
    _$agentIsOwnerAtom.reportRead();
    return super.agentIsOwner;
  }

  @override
  set agentIsOwner(bool value) {
    _$agentIsOwnerAtom.reportWrite(value, super.agentIsOwner, () {
      super.agentIsOwner = value;
    });
  }

  final _$bathroomsFullAtom = Atom(name: 'ListingBase.bathroomsFull');

  @override
  int get bathroomsFull {
    _$bathroomsFullAtom.reportRead();
    return super.bathroomsFull;
  }

  @override
  set bathroomsFull(int value) {
    _$bathroomsFullAtom.reportWrite(value, super.bathroomsFull, () {
      super.bathroomsFull = value;
    });
  }

  final _$bathroomsHalfAtom = Atom(name: 'ListingBase.bathroomsHalf');

  @override
  int get bathroomsHalf {
    _$bathroomsHalfAtom.reportRead();
    return super.bathroomsHalf;
  }

  @override
  set bathroomsHalf(int value) {
    _$bathroomsHalfAtom.reportWrite(value, super.bathroomsHalf, () {
      super.bathroomsHalf = value;
    });
  }

  final _$bedroomsAtom = Atom(name: 'ListingBase.bedrooms');

  @override
  int get bedrooms {
    _$bedroomsAtom.reportRead();
    return super.bedrooms;
  }

  @override
  set bedrooms(int value) {
    _$bedroomsAtom.reportWrite(value, super.bedrooms, () {
      super.bedrooms = value;
    });
  }

  final _$cityAtom = Atom(name: 'ListingBase.city');

  @override
  String get city {
    _$cityAtom.reportRead();
    return super.city;
  }

  @override
  set city(String value) {
    _$cityAtom.reportWrite(value, super.city, () {
      super.city = value;
    });
  }

  final _$completedStepsAtom = Atom(name: 'ListingBase.completedSteps');

  @override
  ObservableSet<String> get completedSteps {
    _$completedStepsAtom.reportRead();
    return super.completedSteps;
  }

  @override
  set completedSteps(ObservableSet<String> value) {
    _$completedStepsAtom.reportWrite(value, super.completedSteps, () {
      super.completedSteps = value;
    });
  }

  final _$depositAtom = Atom(name: 'ListingBase.deposit');

  @override
  int get deposit {
    _$depositAtom.reportRead();
    return super.deposit;
  }

  @override
  set deposit(int value) {
    _$depositAtom.reportWrite(value, super.deposit, () {
      super.deposit = value;
    });
  }

  final _$hasAgencyAtom = Atom(name: 'ListingBase.hasAgency');

  @override
  bool get hasAgency {
    _$hasAgencyAtom.reportRead();
    return super.hasAgency;
  }

  @override
  set hasAgency(bool value) {
    _$hasAgencyAtom.reportWrite(value, super.hasAgency, () {
      super.hasAgency = value;
    });
  }

  final _$housingTypeAtom = Atom(name: 'ListingBase.housingType');

  @override
  String get housingType {
    _$housingTypeAtom.reportRead();
    return super.housingType;
  }

  @override
  set housingType(String value) {
    _$housingTypeAtom.reportWrite(value, super.housingType, () {
      super.housingType = value;
    });
  }

  final _$isSavedInApiAtom = Atom(name: 'ListingBase.isSavedInApi');

  @override
  bool get isSavedInApi {
    _$isSavedInApiAtom.reportRead();
    return super.isSavedInApi;
  }

  @override
  set isSavedInApi(bool value) {
    _$isSavedInApiAtom.reportWrite(value, super.isSavedInApi, () {
      super.isSavedInApi = value;
    });
  }

  final _$parkingTypeAtom = Atom(name: 'ListingBase.parkingType');

  @override
  String get parkingType {
    _$parkingTypeAtom.reportRead();
    return super.parkingType;
  }

  @override
  set parkingType(String value) {
    _$parkingTypeAtom.reportWrite(value, super.parkingType, () {
      super.parkingType = value;
    });
  }

  final _$picturesAtom = Atom(name: 'ListingBase.pictures');

  @override
  ObservableList<String> get pictures {
    _$picturesAtom.reportRead();
    return super.pictures;
  }

  @override
  set pictures(ObservableList<String> value) {
    _$picturesAtom.reportWrite(value, super.pictures, () {
      super.pictures = value;
    });
  }

  final _$latAtom = Atom(name: 'ListingBase.lat');

  @override
  double get lat {
    _$latAtom.reportRead();
    return super.lat;
  }

  @override
  set lat(double value) {
    _$latAtom.reportWrite(value, super.lat, () {
      super.lat = value;
    });
  }

  final _$listingUuidAtom = Atom(name: 'ListingBase.listingUuid');

  @override
  String get listingUuid {
    _$listingUuidAtom.reportRead();
    return super.listingUuid;
  }

  @override
  set listingUuid(String value) {
    _$listingUuidAtom.reportWrite(value, super.listingUuid, () {
      super.listingUuid = value;
    });
  }

  final _$lngAtom = Atom(name: 'ListingBase.lng');

  @override
  double get lng {
    _$lngAtom.reportRead();
    return super.lng;
  }

  @override
  set lng(double value) {
    _$lngAtom.reportWrite(value, super.lng, () {
      super.lng = value;
    });
  }

  final _$descriptionAtom = Atom(name: 'ListingBase.description');

  @override
  String get description {
    _$descriptionAtom.reportRead();
    return super.description;
  }

  @override
  set description(String value) {
    _$descriptionAtom.reportWrite(value, super.description, () {
      super.description = value;
    });
  }

  final _$monthlyRentAtom = Atom(name: 'ListingBase.monthlyRent');

  @override
  int get monthlyRent {
    _$monthlyRentAtom.reportRead();
    return super.monthlyRent;
  }

  @override
  set monthlyRent(int value) {
    _$monthlyRentAtom.reportWrite(value, super.monthlyRent, () {
      super.monthlyRent = value;
    });
  }

  final _$shortInfoAtom = Atom(name: 'ListingBase.shortInfo');

  @override
  String get shortInfo {
    _$shortInfoAtom.reportRead();
    return super.shortInfo;
  }

  @override
  set shortInfo(String value) {
    _$shortInfoAtom.reportWrite(value, super.shortInfo, () {
      super.shortInfo = value;
    });
  }

  final _$squareFeetAtom = Atom(name: 'ListingBase.squareFeet');

  @override
  int get squareFeet {
    _$squareFeetAtom.reportRead();
    return super.squareFeet;
  }

  @override
  set squareFeet(int value) {
    _$squareFeetAtom.reportWrite(value, super.squareFeet, () {
      super.squareFeet = value;
    });
  }

  final _$statusAtom = Atom(name: 'ListingBase.status');

  @override
  int get status {
    _$statusAtom.reportRead();
    return super.status;
  }

  @override
  set status(int value) {
    _$statusAtom.reportWrite(value, super.status, () {
      super.status = value;
    });
  }

  final _$streetOneAtom = Atom(name: 'ListingBase.streetOne');

  @override
  String get streetOne {
    _$streetOneAtom.reportRead();
    return super.streetOne;
  }

  @override
  set streetOne(String value) {
    _$streetOneAtom.reportWrite(value, super.streetOne, () {
      super.streetOne = value;
    });
  }

  final _$titleAtom = Atom(name: 'ListingBase.title');

  @override
  String get title {
    _$titleAtom.reportRead();
    return super.title;
  }

  @override
  set title(String value) {
    _$titleAtom.reportWrite(value, super.title, () {
      super.title = value;
    });
  }

  @override
  String toString() {
    return '''
carrousselIndex: ${carrousselIndex},
address: ${address},
agencyUuid: ${agencyUuid},
agentUuid: ${agentUuid},
agentIsOwner: ${agentIsOwner},
bathroomsFull: ${bathroomsFull},
bathroomsHalf: ${bathroomsHalf},
bedrooms: ${bedrooms},
city: ${city},
completedSteps: ${completedSteps},
deposit: ${deposit},
hasAgency: ${hasAgency},
housingType: ${housingType},
isSavedInApi: ${isSavedInApi},
parkingType: ${parkingType},
pictures: ${pictures},
lat: ${lat},
listingUuid: ${listingUuid},
lng: ${lng},
description: ${description},
monthlyRent: ${monthlyRent},
shortInfo: ${shortInfo},
squareFeet: ${squareFeet},
status: ${status},
streetOne: ${streetOne},
title: ${title},
printMonthlyRent: ${printMonthlyRent},
printSize: ${printSize},
printBathrooms: ${printBathrooms},
printAddressSectionOne: ${printAddressSectionOne},
printAddressSectionTwo: ${printAddressSectionTwo},
carrousselIndexStatus: ${carrousselIndexStatus},
mapPreviewPicture: ${mapPreviewPicture},
isLoaded: ${isLoaded},
firstPicture: ${firstPicture},
isDraft: ${isDraft},
validDescription: ${validDescription},
validTitle: ${validTitle},
validRoomsAndSpaces: ${validRoomsAndSpaces}
    ''';
  }
}
