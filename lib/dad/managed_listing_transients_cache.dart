import 'package:mobx/mobx.dart';

import 'package:exe/dad/cache.dart';
import 'package:exe/dad/transient/listing.dart';
import 'package:exe/king/king.dart';
part 'managed_listing_transients_cache.g.dart';

class ManagedListingTransientsCache extends Cache<ManagedListingTransients> {
  King king;
  //String getUrl = 'managed/listingTransients';
  String getUrl = 'agents/listing_previews';

  ManagedListingTransientsCache(this.king) : super(king);

  @override
  ManagedListingTransients makeNewCacheItem() {
    return ManagedListingTransients();
  }

  @override
  void unpackItemFromApi(
      Map<String, Object> jsonIn, ManagedListingTransients target) {
    target.drafts.clear();
    target.rented.clear();
    target.unlisted.clear();
    target.vacant.clear();

//IMPORTANT: keep in sync with api
    const int draft = 0;
    const int rented = 2;
    const int unlisted = 3;
    const int vacant = 1;

    for (var item in jsonIn['collection']) {
      var listing = TransientListing.fromJson(item);
      switch (listing.status as int) {
        case draft:
          target.drafts.add(listing);
          break;
        case rented:
          target.rented.add(listing);
          break;
        case unlisted:
          target.unlisted.add(listing);
          break;
        case vacant:
          target.vacant.add(listing);
          break;
        default:
          throw ('ERROR: unrecognized status for listing');
      }
    }

    print('==========================================');
    print('==========================================');
    print(target.drafts);
    print(target.rented);
    print(target.unlisted);
    print(target.vacant);
  }
}

class ManagedListingTransients = ManagedListingTransientsBase
    with _$ManagedListingTransients;

abstract class ManagedListingTransientsBase with Store {
  @observable
  var drafts = ObservableList<TransientListing>();
  @observable
  var rented = ObservableList<TransientListing>();
  @observable
  var unlisted = ObservableList<TransientListing>();
  @observable
  var vacant = ObservableList<TransientListing>();
}
