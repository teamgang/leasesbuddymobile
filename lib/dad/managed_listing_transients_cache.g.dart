// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'managed_listing_transients_cache.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ManagedListingTransients on ManagedListingTransientsBase, Store {
  final _$draftsAtom = Atom(name: 'ManagedListingTransientsBase.drafts');

  @override
  ObservableList<TransientListing> get drafts {
    _$draftsAtom.reportRead();
    return super.drafts;
  }

  @override
  set drafts(ObservableList<TransientListing> value) {
    _$draftsAtom.reportWrite(value, super.drafts, () {
      super.drafts = value;
    });
  }

  final _$rentedAtom = Atom(name: 'ManagedListingTransientsBase.rented');

  @override
  ObservableList<TransientListing> get rented {
    _$rentedAtom.reportRead();
    return super.rented;
  }

  @override
  set rented(ObservableList<TransientListing> value) {
    _$rentedAtom.reportWrite(value, super.rented, () {
      super.rented = value;
    });
  }

  final _$unlistedAtom = Atom(name: 'ManagedListingTransientsBase.unlisted');

  @override
  ObservableList<TransientListing> get unlisted {
    _$unlistedAtom.reportRead();
    return super.unlisted;
  }

  @override
  set unlisted(ObservableList<TransientListing> value) {
    _$unlistedAtom.reportWrite(value, super.unlisted, () {
      super.unlisted = value;
    });
  }

  final _$vacantAtom = Atom(name: 'ManagedListingTransientsBase.vacant');

  @override
  ObservableList<TransientListing> get vacant {
    _$vacantAtom.reportRead();
    return super.vacant;
  }

  @override
  set vacant(ObservableList<TransientListing> value) {
    _$vacantAtom.reportWrite(value, super.vacant, () {
      super.vacant = value;
    });
  }

  @override
  String toString() {
    return '''
drafts: ${drafts},
rented: ${rented},
unlisted: ${unlisted},
vacant: ${vacant}
    ''';
  }
}
