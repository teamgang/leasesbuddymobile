import 'package:mobx/mobx.dart';

import 'package:exe/dad/cache.dart';
import 'package:exe/king/king.dart';
import 'package:exe/lip/lip.dart';

part 'mapsac_cacher.g.dart';

class MapsacPredictionListCacher {
  MapsacPredictionListCache cache;
  String sessionToken;
  String input = '';
  double lat = 0;
  double long = 0;

  MapsacPredictionListCacher(King king) {
    this.cache = MapsacPredictionListCache(king);
  }

  String get latLngString {
    return this.lat.toString() + ',' + this.long.toString();
  }

  String get uuid {
    return this.input + ',' + this.latLngString;
  }

  PredictionList get currentPredictions {
    if (this.input.length < 3) {
      return cache.makeNewCacheItem();
    }

    Map<String, Object> payload = {
      'input': this.input,
      'lat': this.lat,
      'lng': this.long,
    };
    print('+++++++++++ running getitem+++++++++++');
    return this.cache.getItem(this.uuid, payload: payload);
  }

  void setFields({String input, double lat, double long}) {
    this.input = input;
    this.lat = lat;
    this.long = long;
  }
}

// TODO: do we need to handle expired sessionToken?
class MapsacPredictionListCache extends Cache<PredictionList> {
  King king;
  HttpMethod getMethod;
  String getUrl;

  MapsacPredictionListCache(this.king, {this.getMethod, this.getUrl})
      : super(king);

  PredictionList getPredictions({
    String input,
    double lat,
    double long,
  }) {
    if (input.length < 3) {
      return this.makeNewCacheItem();
    }

    Map<String, Object> payload = {
      'input': input,
      'user_location': '$lat,$long',
      //'lat': lat,
      //'lng': long,
    };
    String uuid = '$input,$lat,$long';
    var apireq = ApiRequest(
      method: this.getMethod,
      payload: payload,
      url: this.getUrl,
      uuid: uuid,
    );
    print(this.getMethod.value);
    print(payload);
    print(this.getUrl);
    print(uuid);
    //return super.getItemApireq(apireq);

    this.tryUpdateCacheItem(
      apireq,
      //TODO: do we want to set unpackFunction?
      unpackFunction: funcUnpackItemFromApi,
    );

    //this.unpackItemFromApi(ares.body, this.getCachedItem(apireq.uuid));

    return this.getCachedItem(apireq.uuid);
  }

  @override
  PredictionList makeNewCacheItem() {
    return PredictionList();
  }

  @override
  void unpackItemFromApi(Map<String, Object> jsonIn, PredictionList target) {
    target.predictions.clear();
    for (var item in jsonIn['collection']) {
      var prediction = Prediction.fromJson(item);
      target.predictions.add(prediction);
    }
  }
}

void funcUnpackItemFromApi(Map<String, Object> jsonIn, PredictionList target) {
  target.predictions.clear();
  for (var item in jsonIn['predictions']) {
    var prediction = Prediction.fromJson(item);
    target.predictions.add(prediction);
  }
}

class PredictionList = PredictionListBase with _$PredictionList;

abstract class PredictionListBase with Store {
  @observable
  var predictions = ObservableList<Prediction>();
}

class Prediction {
  String address;
  int distanceMeters;
  //double lat;
  //double long;
  String placeId;

  Prediction({
    this.address,
    this.distanceMeters,
    //this.lat,
    //this.long,
    this.placeId,
  });

  factory Prediction.fromJson(Map<String, dynamic> jsonIn) => Prediction(
        address: jsonIn['description'],
        distanceMeters: jsonIn['distanceMeters'],
        //lat: jsonIn['lat'],
        //long: jsonIn['lng'],
        placeId: jsonIn['place_id'],
      );

  String toString() {
    return 'address: $address, placeId: $placeId';
  }
}
