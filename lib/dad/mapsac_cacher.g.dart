// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'mapsac_cacher.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$PredictionList on PredictionListBase, Store {
  final _$predictionsAtom = Atom(name: 'PredictionListBase.predictions');

  @override
  ObservableList<Prediction> get predictions {
    _$predictionsAtom.reportRead();
    return super.predictions;
  }

  @override
  set predictions(ObservableList<Prediction> value) {
    _$predictionsAtom.reportWrite(value, super.predictions, () {
      super.predictions = value;
    });
  }

  @override
  String toString() {
    return '''
predictions: ${predictions}
    ''';
  }
}
