import 'package:mobx/mobx.dart';

import 'package:exe/dad/cache.dart';
import 'package:exe/king/king.dart';
part 'messages_cache.g.dart';

class MessagesCache extends Cache<Messages> {
  King king;
  String getUrl;

  MessagesCache(this.king, {this.getUrl}) : super(king);

  @override
  Messages makeNewCacheItem() {
    return Messages();
  }

  @action
  unpackItemFromApi(Map<String, Object> jsonIn, Messages target) {
    target.messages.clear();
    for (var item in jsonIn['messages']) {
      var msg = Message.fromJson(item);
      target.messages.add(msg);
    }
  }
}

class Messages = MessagesBase with _$Messages;

abstract class MessagesBase with Store {
  @observable
  var messages = ObservableList<Message>();
}

class Message {
  String msgUuid;
  String userUuid;
  // TODO: store usernames somepleace else to reduce this data?
  String username;
  String text;
  // TODO: images
  int time;
  // TODO: sort/filter tags
  //List<String> tags;
  // TODO: sort/filter ats. highlight bubbles with a user's at?
  //List<String> ats;
  //bool seen;
  // TODO: reply & quote
  // TODO: forward
  // TODO: edited

  Message({
    this.msgUuid,
    this.text,
    this.time,
    this.username,
    this.userUuid,
  });

  factory Message.fromJson(Map<String, dynamic> jsonIn) => Message(
        msgUuid: jsonIn['msgUuid'],
        text: jsonIn['text'],
        time: jsonIn['time'],
        username: jsonIn['username'],
        userUuid: jsonIn['userUuid'],
      );

  Map<String, dynamic> toJson() => {
        'msgUuid': msgUuid,
        'text': text,
        'time': time,
        'username': username,
        'userUuid': userUuid,
      };

  String get timeReadable {
    return '4:21 pm';
  }
}
