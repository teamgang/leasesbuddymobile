import 'package:mobx/mobx.dart';

import 'package:exe/dad/cache.dart';
import 'package:exe/king/king.dart';
part 'saved_listing_uuids_cache.g.dart';

class SavedListingUuidsCache extends Cache<SavedListingUuids> {
  King king;
  String getUrl = 'saved/listingUuids';

  SavedListingUuidsCache(this.king) : super(king);

  @override
  SavedListingUuids makeNewCacheItem() {
    return SavedListingUuids();
  }

  @override
  void unpackItemFromApi(Map<String, Object> jsonIn, SavedListingUuids target) {
    target.uuids.clear();
    target.uuids.addAll(jsonIn['uuids']);
  }
}

class SavedListingUuids = SavedListingUuidsBase with _$SavedListingUuids;

abstract class SavedListingUuidsBase with Store {
  @observable
  ObservableList<String> uuids = ObservableList<String>();
}
