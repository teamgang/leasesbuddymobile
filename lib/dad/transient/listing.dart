class TransientListing {
  String listingUuid = 'Loading...';
  String agentUuid = 'Loading...';
  int rentalEndDate = 0;
  int status = -1;
  String title = 'Loading...';

  TransientListing({
    this.agentUuid,
    this.listingUuid,
    this.rentalEndDate,
    this.status,
    this.title,
  });

  factory TransientListing.fromJson(Map<String, dynamic> jsonIn) {
    String listingUuid = jsonIn['id'] ?? 'Loading...';
    String agentUuid = jsonIn['agent_id'] ?? 'Loading...';
    int rentalEndDate = jsonIn['rental_end_date'] ?? 0;
    int status = jsonIn['status'] ?? -1;
    String title = jsonIn['title'] ?? 'Loading...';

    return TransientListing(
      listingUuid: listingUuid,
      agentUuid: agentUuid,
      rentalEndDate: rentalEndDate,
      status: status,
      title: title,
    );
  }

  String get firstPicture {
    //TODO: correct picture url
    return 'picture/goes/here';
  }
}
