import 'package:flutter/material.dart';
import 'package:logging/logging.dart';
//import 'package:mobx/mobx.dart';
import 'package:provider/provider.dart';

import 'package:exe/conf.dart';
import 'package:exe/dad/account_cache.dart';
import 'package:exe/dad/dad.dart';
import 'package:exe/king/theme_select.dart';
import 'package:exe/pac/pac.dart';
import 'package:exe/lip/lip.dart';
import 'package:exe/services/auth.dart';
import 'package:exe/shared/explore_query.dart';
import 'package:exe/services/navman.dart';
import 'package:exe/services/push.dart';
import 'package:exe/services/user.dart';

export 'package:exe/king/args.dart';
export 'package:exe/king/consts.dart';
export 'package:exe/king/xkeys.dart';

class King {
  AuthService authService;
  Conf conf;
  Dad dad;
  ExploreQuery equery;
  Logger log;
  Lip lip;
  NavManager navman;
  Pac pac;
  PushService push;
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  ThemeSelect themeSelect;
  User user;

  King({Conf conf}) {
    // no dependencies
    this.conf = conf;
    this.navman = NavManager();
    this.pac = Pac();
    this.themeSelect = ThemeSelect();

    // depends only on conf
    this.log = new Logger('NumaKing');
    this.initLip(conf);
    setupLogger(conf.logLevel);

    // depends on 'this'
    this.authService = AuthService(this);
    this.dad = new Dad(this);
    this.equery = ExploreQuery(this);
    this.push = PushService(this);
    this.user = User(this);

    if (this.conf.mockAutoSignIn) {
      this.authService.signInWithEmail(email: '3@3.3', password: 'password');
    }
  }

  Future initialize() async {
    await this.push.initialize();
  }

  static King of(BuildContext context) {
    return Provider.of<King>(context, listen: false);
  }

  void initLip(Conf conf) {
    switch (conf.whichApi) {
      case ApiChoices.web:
        this.lip = new Lip(this);
        break;
      default:
        throw ('Invalid api set by conf.whichApi');
        break;
    }
  }

  Account get userAccount {
    return this.dad.accountCache.getItem(this.user.userUuid);
  }

  // signOutCleanup should destroy all personal information
  void signOutCleanup() {
    this.pac = Pac();
    this.user = User(this);
    this.dad = new Dad(this);
    this.lip = new Lip(this);
    this.authService = AuthService(this);
    this.initLip(conf);
  }
}

navPush(BuildContext context, Widget widget) {
  Navigator.push(
      context, MaterialPageRoute(builder: (BuildContext context) => widget));
}

navPushReplacement(BuildContext context, Widget widget) {
  Navigator.push(
      context, MaterialPageRoute(builder: (BuildContext context) => widget));
}

void setupLogger(String level) {
  switch (level) {
    case 'off': // 2000
      Logger.root.level = Level.OFF;
      break;
    case 'shout': // 1200
      Logger.root.level = Level.SHOUT;
      break;
    case 'severe': // 1000
      Logger.root.level = Level.SEVERE;
      break;
    case 'warning': // 900
      Logger.root.level = Level.WARNING;
      break;
    case 'info': // 800
      Logger.root.level = Level.INFO;
      break;
    case 'config': // 700
      Logger.root.level = Level.CONFIG;
      break;
    case 'fine': // 500
      Logger.root.level = Level.FINE;
      break;
    case 'finer': // 400
      Logger.root.level = Level.FINER;
      break;
    case 'finest': // 300
      Logger.root.level = Level.FINEST;
      break;
    case 'all': // 0
      Logger.root.level = Level.ALL;
      break;
  }

  Logger.root.onRecord.listen((record) {
    print('${record.level.name}: ${record.time}: ${record.message}');
  });
}
