import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

part 'theme_select.g.dart';

enum Themes { dark, light }

class ThemeSelect = ThemeSelectBase with _$ThemeSelect;

abstract class ThemeSelectBase with Store {
  @observable
  var selectedTheme = Themes.light;

  @action
  setTheme(Themes newTheme) {
    this.selectedTheme = newTheme;
  }

  @computed
  ThemeData get theme {
    switch (this.selectedTheme) {
      case Themes.dark:
        return darkTheme;
      case Themes.light:
        return lightTheme;
      default:
        throw ('ERROR: theme not found');
    }
  }
}

// TODO:
// visualDensity: VisualDensity.adaptivePlatformDensity,
// fontFamily
// brightness
ThemeData lightTheme = ThemeData(
  accentColor: Colors.green,
  //primarySwatch: Colors.grey,
  primaryColor: Colors.white,
  textTheme: TextTheme(
    headline4: TextStyle(fontSize: 30, color: Colors.black),
    subtitle2: TextStyle(fontSize: 24, color: Colors.black),
    headline6: TextStyle(fontSize: 20, color: Colors.black),
  ),
);

ThemeData darkTheme = ThemeData(
  accentColor: Colors.green,
  //primarySwatch: Colors.yellow,
  primaryColor: Colors.grey[850],
  backgroundColor: Colors.grey[850],
  dialogBackgroundColor: Colors.grey[850],
  scaffoldBackgroundColor: Colors.grey[850],
  textTheme: TextTheme(
    body1: TextStyle(color: Colors.grey[100]),
    body2: TextStyle(color: Colors.grey, fontSize: 12),
  ),
  colorScheme: ColorScheme.light(
    primary: Colors.black,
    onPrimary: Colors.black,
    primaryVariant: Colors.black,
    secondary: Colors.red,
  ),
  cardTheme: CardTheme(
    color: Colors.grey[850],
  ),
  // dividers are black
  //Text(this.value, style: TextStyle(color: Colors.grey[100])),
  // subtext is simply grey
);
