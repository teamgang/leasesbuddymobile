// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'theme_select.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ThemeSelect on ThemeSelectBase, Store {
  Computed<ThemeData> _$themeComputed;

  @override
  ThemeData get theme => (_$themeComputed ??=
          Computed<ThemeData>(() => super.theme, name: 'ThemeSelectBase.theme'))
      .value;

  final _$selectedThemeAtom = Atom(name: 'ThemeSelectBase.selectedTheme');

  @override
  Themes get selectedTheme {
    _$selectedThemeAtom.reportRead();
    return super.selectedTheme;
  }

  @override
  set selectedTheme(Themes value) {
    _$selectedThemeAtom.reportWrite(value, super.selectedTheme, () {
      super.selectedTheme = value;
    });
  }

  final _$ThemeSelectBaseActionController =
      ActionController(name: 'ThemeSelectBase');

  @override
  dynamic setTheme(Themes newTheme) {
    final _$actionInfo = _$ThemeSelectBaseActionController.startAction(
        name: 'ThemeSelectBase.setTheme');
    try {
      return super.setTheme(newTheme);
    } finally {
      _$ThemeSelectBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
selectedTheme: ${selectedTheme},
theme: ${theme}
    ''';
  }
}
