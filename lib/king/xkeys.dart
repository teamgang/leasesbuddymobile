//NOTE: importing anything flutter related will crash tests
class XKeys {
  static String authFormRegisterTabButton = 'authForm/registerTab';
  static String authFormSignInTabButton = 'authForm/signInTabButton';
  static String authFormRegisterOrSignInButton =
      'authFormRegisterOrSignInButton';
  static String authFormEmailField = 'authForm/emailField';
  static String authFormPasswordField = 'authForm/passwordField';

  static String erectAgentConfirmNo = 'erectAgentConfirmNo';
  static String erectAgentConfirmYes = 'erectAgentConfirmYes';
  static String erectAgentStart = 'erectAgentStart';

  static String profileOptionsList = 'profileOptionsList';
  static String profileSignOutButton = 'profileSignOutButton';
  static String profileTogglePovButton = 'profileTogglePovButton';

  static String navManage = 'navManage';
  static String navExplore = 'navExplore';
  static String navFavorite = 'navFavorite';
  static String navConnect = 'navConnect';
  static String navProfile = 'navProfile';
}
