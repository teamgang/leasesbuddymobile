import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:http/http.dart' as http;

import 'package:exe/king/king.dart';

enum HttpMethod { delete, get, patch, post }

extension ValueExtension on HttpMethod {
  String get value {
    switch (this) {
      case HttpMethod.delete:
        return 'DELETE';
      case HttpMethod.get:
        return 'GET';
      case HttpMethod.patch:
        return 'PATCH';
      case HttpMethod.post:
        return 'POST';
      default:
        throw ('HttpMethod is invalid, with value: $this');
    }
  }
}

class Lip extends http.BaseClient {
  String apiUrl;
  King king;
  final http.Client _inner = http.Client();
  int timeout;

  Lip(this.king) {
    apiUrl = this.king.conf.apiUrl;
    timeout = this.king.conf.requestTimeout;
  }

  Future<ApiResponse> api(
    String endpoint, {
    Map<String, Object> payload,
    HttpMethod method = HttpMethod.post,
  }) async {
    Map<String, String> headers = {
      'content-type': 'application/json',
    };
    king.authService.attachSessionHeaders(headers);

    // NOTE: if we are passed a full url, use that instead of papi
    String url;
    String protocol = endpoint.split(':').first;
    if (protocol == 'http' || protocol == 'https') {
      url = endpoint;
    } else {
      url = apiUrl + endpoint;
    }

    String payloadAsString = jsonEncode(payload);
    ApiResponse ares = ApiResponse();
    try {
      try {
        http.Response response;
        switch (method) {
          case HttpMethod.get:
            response = await _inner
                .get(url, headers: headers)
                .timeout(Duration(seconds: timeout));
            break;

          case HttpMethod.patch:
            response = await _inner
                .patch(url,
                    headers: headers,
                    body: payloadAsString,
                    encoding: Encoding.getByName('utf-8'))
                .timeout(Duration(seconds: timeout));
            break;

          case HttpMethod.post:
            response = await _inner
                .post(url,
                    headers: headers,
                    body: payloadAsString,
                    encoding: Encoding.getByName('utf-8'))
                .timeout(Duration(seconds: timeout));
            break;
        }
        ares.processHttpResponse(response, king);
        //TODO: incrementally update jwt smarter
        king.authService.getSessionidOrJwt(ares);
      } on SocketException catch (e, s) {
        print(e);
        print(s);
        king.log.info('ERROR: SocketException while connecting to API');
        ares.errored = true;
      } on TimeoutException catch (e, s) {
        print(e);
        print(s);
        king.log.info('ERROR: TimeoutException while connecting to API');
        ares.errored = true;
      } catch (e, s) {
        print(e);
        print(s);
        king.log.info('ERROR: Unknown exception while connecting to API');
        ares.errored = true;
      }
    } catch (e, s) {
      print(e);
      ares.errored = true;
    }

    king.log.fine(
        '${method.value}${method.value}${method.value}${method.value}${method.value}${method.value}${method.value}${method.value}');
    king.log.fine('WWWAPI: ${method.value} request to +$url+ with fields:');
    king.log.fine('Headers: $headers');
    king.log.fine('Payload: $payloadAsString');
    //print('Response headers: ${response.headers['content-type']}');
    king.log.fine('Response status code: ${ares.statusCode}');
    king.log.fine('Response body: ${ares.body}');
    king.log.fine('Did response error?: ${ares.errored}');
    king.log.fine('===================================');

    return ares;
  }

  Future<http.StreamedResponse> send(http.BaseRequest request) {
    // TODO: is this ever reached?
    return _inner.send(request);
  }
}

class ApiResponse {
  int statusCode = 0;
  Map<String, String> headers = {};
  Map<String, Object> body = {};
  bool errored = false;

  void processHttpResponse(http.Response response, King king) {
    if (response == null) {
      return;
    }
    this.statusCode = response.statusCode;
    this.headers = response.headers;
    // TODO: do we need to support variations of spelling content-type?
    String contentType = response.headers["content-type"];
    if (contentType.contains("application/json")) {
      try {
        this.body = json.decode(response.body);
      } on FormatException catch (e, s) {
        king.log.info('FormatException while reading API response');
        print(e);
        print(s);
        this.errored = true;
      }
      //TODO: server should return 404 or something instead of an errors array
      if (this.body.containsKey('errors')) {
        this.errored = true;
      }
    }
  }

  isOk() {
    if (this.errored) {
      return false;
    }
    if (this.body == null) {
      return false;
    }
    return (statusCode >= 200) && (statusCode <= 299);
  }

  isOkWithoutBody() {
    if (this.errored) {
      return false;
    }
    return statusCode == HttpStatus.ok;
  }

  String toString() {
    return '$statusCode $headers $body $errored';
  }
}

class ApiRequest {
  HttpMethod method;
  Map<String, Object> payload;
  String url;
  String uuid;

  ApiRequest({
    this.method = HttpMethod.post,
    this.payload = const {},
    this.url,
    this.uuid = '',
  });

  String get urlRegexed {
    return '${this.url}'.replaceAll(
      new RegExp(r'\+\+uuid'),
      '${this.uuid}',
    );
  }
}
