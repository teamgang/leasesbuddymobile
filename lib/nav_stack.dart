import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:exe/king/king.dart';
import 'package:exe/services/user.dart';
import 'package:exe/widgets/account/account_page.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/connect/connect_page.dart';
import 'package:exe/widgets/explore/explore.dart';
//import 'package:exe/widgets/manage/manage_page.dart';
import 'package:exe/widgets/manage/router.dart';
import 'package:exe/widgets/saved/saved_page.dart';

class NavStack extends StatefulWidget {
  @override
  NavStackState createState() => NavStackState();
}

class NavStackState extends State<NavStack> {
  @override
  initState() {
    super.initState();
    var king = King.of(context);
    king.navman.initPlatformStateForUriUniLinks();
  }

  @override
  Widget build(BuildContext context) {
    var king = King.of(context);
    return Observer(
      builder: (_) => Theme(
        data: king.themeSelect.theme,
        child: IndexedStack(
          index: king.navman.currentNavIndex,
          children: <Widget>[
            king.user.povIsTenant
                ? Navigator(
                    key: king.navman.exploreNavKey,
                    onGenerateRoute: (RouteSettings route) => MaterialPageRoute(
                        settings: route, builder: (context) => ExplorePage()),
                  )
                : ManageRouter(manageNavKey: king.navman.manageNavKey),
            //: Navigator(
            //key: king.navman.manageNavKey,
            //onGenerateRoute: (RouteSettings route) => MaterialPageRoute(
            //settings: route, builder: (context) => ManageRouter()),
            //),
            Navigator(
              key: king.navman.savedNavKey,
              onGenerateRoute: (RouteSettings route) => MaterialPageRoute(
                  settings: route, builder: (context) => SavedPage()),
            ),
            Navigator(
              key: king.navman.connectNavKey,
              onGenerateRoute: (RouteSettings route) => MaterialPageRoute(
                  settings: route, builder: (context) => ConnectPage()),
            ),
            Navigator(
              key: king.navman.profileNavKey,
              onGenerateRoute: (RouteSettings route) => MaterialPageRoute(
                  settings: route, builder: (context) => AccountPage()),
            ),
          ],
        ),
      ),
    );
  }
}
