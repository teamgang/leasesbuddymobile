import 'package:meta/meta.dart';

import 'package:exe/pac/page_cache.dart';

class ContactAgentViaListingPac
    extends PageCache<ContactAgentViaListingFields> {
  @override
  ContactAgentViaListingFields makeNewCacheItem() {
    return ContactAgentViaListingFields();
  }
}

makeUuidForContactAgentViaListingPac({@required String listingUuid}) {
  return listingUuid;
}

class ContactAgentViaListingFields {
  String message = '';
  bool useDefaultMessage = true;
}
