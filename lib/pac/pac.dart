import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'package:exe/king/king.dart';
import 'package:exe/pac/contact_agent_via_listing_pac.dart';

// Pac: page cache
class Pac {
  ContactAgentViaListingPac contactAgentViaListingPac;

  Pac() {
    this.contactAgentViaListingPac = ContactAgentViaListingPac();
  }
}

enum PacTypes {
  contactAgentViaListing,
}

fromPac(BuildContext context, PacTypes pacType) {
  final King king = Provider.of<King>(context, listen: false);
  switch (pacType) {
    case (PacTypes.contactAgentViaListing):
      return king.pac.contactAgentViaListingPac;
  }
}

getItemFromPac(BuildContext context, PacTypes pacType, String uuid) {
  final King king = Provider.of<King>(context, listen: false);
  var pac = fromPac(context, pacType);
  return pac.getItem(uuid);
}
