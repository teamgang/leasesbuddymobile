abstract class PageCache<T> {
  Map<String, T> cachedItems = {};

  T getItem(String uuid) {
    if (!this.cachedItems.containsKey(uuid)) {
      this.cachedItems[uuid] = makeNewCacheItem();
    }
    return this.cachedItems[uuid];
  }

  T resetCacheItem(String uuid) {
    this.cachedItems[uuid] = makeNewCacheItem();
  }

  T makeNewCacheItem() {
    throw UnimplementedError;
  }
}
