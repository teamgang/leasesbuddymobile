import 'package:mobx/mobx.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:exe/dad/cache.dart';
import 'package:exe/king/king.dart';
import 'package:exe/lip/lip.dart';

part 'auth.g.dart';

enum AuthStatus {
  //admin,
  //anon,
  errored,
  initialized,
  loading,
  signedIn,
  signedOut,
}

class AuthService = AuthServiceBase with _$AuthService;

// TODO: set timeouts for these processes
abstract class AuthServiceBase with Store {
  King king;
  @observable
  AuthStatus status = AuthStatus.initialized;
  //@observable
  String jwt = '';
  String cookie = '';

  AuthServiceBase(this.king);

  //void pushToExploreNav() {
  //final dispose = autorun((_) {
  //king.navman.switchToNav(Navs.explore);
  //});
  //}

  @action
  void getSessionidOrJwt(ApiResponse ares) {
    // Set cookie if it's here
    String rawCookie = ares.headers['set-cookie'];
    if (rawCookie != null) {
      this.cookie = rawCookie.split('sessionid=')[1].split(';')[0];
    } else {
      this.cookie = '';
    }

    // Set jwt if it's here
    if (ares.body.containsKey('token')) {
      this.jwt = ares.body['token'];
    }
  }

  @action
  void processSignInResponse(ApiResponse ares) {
    this.getSessionidOrJwt(ares);

    this.king.user.userUuid = ares.body['id'];
    print('user ID from server is ${king.user.userUuid}');

    if ((this.jwt.length > 0 || this.cookie.length > 0) &&
        this.king.user.userUuid.length > 0) {
      this.status = AuthStatus.signedIn;
      this.king.dad.accountCache.flagForUpdate(king.user.userUuid);
      this.king.dad.accountCache.getItem(king.user.userUuid);
      print('process sign in success');
    } else {
      this.status = AuthStatus.errored;
      print('process sign in errored');
    }
  }

  void attachSessionHeaders(Map<String, String> headers) {
    if (this.cookie != '' && this.cookie != null) {
      headers['cookie'] = this.cookie;
    }
    if (this.jwt != '' && this.jwt != null) {
      headers['Authorization'] = 'Bearer ${this.jwt}';
    }
  }

  @computed
  bool get isSignedIn {
    return this.status == AuthStatus.signedIn;
  }

  @action
  void signOut() {
    // TODO: send json to server to destroy session
    this.status = AuthStatus.signedOut;
    this.king.signOutCleanup();
  }

  @action
  registerWithEmailAndSignIn({String email, String password}) async {
    String endpoint = 'users';
    Map<String, Object> payload = {
      'email': email,
      'password': password,
    };
    this.status = AuthStatus.loading;
    ApiResponse ares = await this.king.lip.api(endpoint, payload: payload);

    print(ares);
    if (ares.isOk()) {
      print('registered with email and now signing in');
      this.processSignInResponse(ares);
    } else {
      // TODO: if we hit this case, something odd is going on so just sign the user out
      print('register with email failed, so forcing sign out');
      this.status = AuthStatus.signedOut;
    }
  }

  @action
  signInWithEmail({String email, String password}) async {
    String endpoint = 'login';
    Map<String, Object> payload = {
      'email': email,
      'password': password,
    };
    this.status = AuthStatus.loading;
    ApiResponse ares = await this.king.lip.api(endpoint, payload: payload);

    if (ares.isOk()) {
      print('signing in with email');
      this.processSignInResponse(ares);
    } else {
      // TODO: if we hit this case, something odd is going on so just sign the user out
      print('sign in with email failed');
      this.status = AuthStatus.signedOut;
    }
  }

  @action
  Future<AuthStatus> signInWithPhone(String phone, String password) async {
    String endpoint = 'user/signin.phone';
    Map<String, Object> payload = {
      'phone': phone,
      'password': password,
    };
    this.status = AuthStatus.loading;
    ApiResponse ares = await this.king.lip.api(endpoint, payload: payload);

    if (ares.isOk()) {
      this.status = AuthStatus.signedIn;
    } else {
      // TODO: if we hit this case, something odd is going on so just sign the user out
      this.status = AuthStatus.signedOut;
    }
  }

  //@action
  //Future<bool> authAnon(String phone, String password) async {
  //String endpoint = 'user/signin';
  //http.Response response = await this.king.lip.api(endpoint);
  //return response.statusCode == HttpStatus.ok;
  //}

  //@action
  //Future<bool> authApple() async {
  //String endpoint = 'user/signin';
  //http.Response response = await this.king.lip.api(endpoint);
  //return response.statusCode == HttpStatus.ok;
  //}

  //@action
  //Future<bool> authGoogle() async {
  //String endpoint = 'user/signin';
  //http.Response response = await this.king.lip.api(endpoint);
  //return response.statusCode == HttpStatus.ok;
  //}
}
