// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'auth.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$AuthService on AuthServiceBase, Store {
  Computed<bool> _$isSignedInComputed;

  @override
  bool get isSignedIn =>
      (_$isSignedInComputed ??= Computed<bool>(() => super.isSignedIn,
              name: 'AuthServiceBase.isSignedIn'))
          .value;

  final _$statusAtom = Atom(name: 'AuthServiceBase.status');

  @override
  AuthStatus get status {
    _$statusAtom.reportRead();
    return super.status;
  }

  @override
  set status(AuthStatus value) {
    _$statusAtom.reportWrite(value, super.status, () {
      super.status = value;
    });
  }

  final _$registerWithEmailAndSignInAsyncAction =
      AsyncAction('AuthServiceBase.registerWithEmailAndSignIn');

  @override
  Future registerWithEmailAndSignIn({String email, String password}) {
    return _$registerWithEmailAndSignInAsyncAction.run(() =>
        super.registerWithEmailAndSignIn(email: email, password: password));
  }

  final _$signInWithEmailAsyncAction =
      AsyncAction('AuthServiceBase.signInWithEmail');

  @override
  Future signInWithEmail({String email, String password}) {
    return _$signInWithEmailAsyncAction
        .run(() => super.signInWithEmail(email: email, password: password));
  }

  final _$signInWithPhoneAsyncAction =
      AsyncAction('AuthServiceBase.signInWithPhone');

  @override
  Future<AuthStatus> signInWithPhone(String phone, String password) {
    return _$signInWithPhoneAsyncAction
        .run(() => super.signInWithPhone(phone, password));
  }

  final _$AuthServiceBaseActionController =
      ActionController(name: 'AuthServiceBase');

  @override
  void getSessionidOrJwt(ApiResponse ares) {
    final _$actionInfo = _$AuthServiceBaseActionController.startAction(
        name: 'AuthServiceBase.getSessionidOrJwt');
    try {
      return super.getSessionidOrJwt(ares);
    } finally {
      _$AuthServiceBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void processSignInResponse(ApiResponse ares) {
    final _$actionInfo = _$AuthServiceBaseActionController.startAction(
        name: 'AuthServiceBase.processSignInResponse');
    try {
      return super.processSignInResponse(ares);
    } finally {
      _$AuthServiceBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void signOut() {
    final _$actionInfo = _$AuthServiceBaseActionController.startAction(
        name: 'AuthServiceBase.signOut');
    try {
      return super.signOut();
    } finally {
      _$AuthServiceBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
status: ${status},
isSignedIn: ${isSignedIn}
    ''';
  }
}
