import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:mobx/mobx.dart';
import 'package:uni_links/uni_links.dart';

import 'package:exe/widgets/agent/agent_page.dart';
import 'package:exe/widgets/connect/connect_page.dart';
import 'package:exe/widgets/listing/listing_page.dart';

part 'navman.g.dart';

enum Navs { profile, connect, explore, manage, saved }

class NavManager = NavManagerBase with _$NavManager;

abstract class NavManagerBase with Store {
  @observable
  var currentNav = Navs.profile;
  StreamSubscription sub;
  // TODO: do we need uri to be an object variable
  Uri uri = Uri();

  final profileNavKey = GlobalKey<NavigatorState>();
  final connectNavKey = GlobalKey<NavigatorState>();
  final exploreNavKey = GlobalKey<NavigatorState>();
  final manageNavKey = GlobalKey<NavigatorState>();
  final savedNavKey = GlobalKey<NavigatorState>();

  /// An implementation using the [Uri] convenience helpers
  Future<void> initPlatformStateForUriUniLinks() async {
    // Get the initial Uri
    try {
      print('getting uri');
      this.uri = await getInitialUri();
      print('initial uri: ${this.uri?.path}'
          ' ${this.uri?.queryParametersAll}');
    } on PlatformException {
      print('ERROR: Failed to get initial uri');
    } on FormatException {
      print('ERROR: Bad parse the initial link as uri');
    }

    // Attach a listener to the Uri links stream
    this.sub = getUriLinksStream().listen((Uri uri) {
      print('first stream');
      print('got uri: ${uri?.path} ${uri?.queryParametersAll}');
      // TODO: save the link so it can be accessed again in the feed
      this.uri = uri;
      this.processAppLink(this.uri);
    }, onError: (Object err) {
      print('got err: $err');
      this.uri = Uri();
    });
  }

  void processAppLink(Uri appLink) {
    if (!appLink.hasEmptyPath) {
      print('============');
      print(appLink.path);
      switch (appLink.path) {
        case '/listing':
          this.currentKey.currentState.push(MaterialPageRoute(
              builder: (BuildContext context) => ListingPage(
                  listingUuid: appLink.queryParameters['listingId'])));
          break;

        case '/agent':
          this.currentKey.currentState.push(MaterialPageRoute(
              builder: (BuildContext context) =>
                  AgentPage(agentUuid: appLink.queryParameters['agentId'])));
          break;

        case '/connect':
          this.currentKey.currentState.push(MaterialPageRoute(
              builder: (BuildContext context) => ConnectPage()));
          break;

        default:
          throw ('ERROR: no matching uri path for appLink. uri: $uri');
      }
    }
  }

  GlobalKey<NavigatorState> get currentKey {
    switch (currentNav) {
      case Navs.profile:
        return profileNavKey;
        break;
      case Navs.connect:
        return connectNavKey;
        break;
      case Navs.explore:
        return exploreNavKey;
        break;
      case Navs.manage:
        return manageNavKey;
        break;
      case Navs.saved:
        return savedNavKey;
        break;
      default:
        throw ('ERROR: missing key from navman');
    }
  }

  int get currentNavIndex {
    switch (currentNav) {
      case Navs.explore:
      case Navs.manage:
        return 0;
        break;
      case Navs.saved:
        return 1;
        break;
      case Navs.connect:
        return 2;
        break;
      case Navs.profile:
        return 3;
        break;
      default:
        throw ('ERROR: missing key from navman');
    }
  }

  void switchToNavIndex(int newIndex, bool povIsTenant) {
    switch (newIndex) {
      case 0:
        if (povIsTenant) {
          this.switchToNav(Navs.explore);
        } else {
          this.switchToNav(Navs.manage);
        }
        break;
      case 1:
        this.switchToNav(Navs.saved);
        break;
      case 2:
        this.switchToNav(Navs.connect);
        break;
      case 3:
        this.switchToNav(Navs.profile);
        break;
      //case 4:
      //this.switchToNav(Navs.profile);
      //break;
      default:
        throw ('ERROR: unknown navman navIndex');
    }
  }

  @action
  void switchToNav(Navs newNav) {
    currentNav = newNav;
  }
}
