// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'navman.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$NavManager on NavManagerBase, Store {
  final _$currentNavAtom = Atom(name: 'NavManagerBase.currentNav');

  @override
  Navs get currentNav {
    _$currentNavAtom.reportRead();
    return super.currentNav;
  }

  @override
  set currentNav(Navs value) {
    _$currentNavAtom.reportWrite(value, super.currentNav, () {
      super.currentNav = value;
    });
  }

  final _$NavManagerBaseActionController =
      ActionController(name: 'NavManagerBase');

  @override
  void switchToNav(Navs newNav) {
    final _$actionInfo = _$NavManagerBaseActionController.startAction(
        name: 'NavManagerBase.switchToNav');
    try {
      return super.switchToNav(newNav);
    } finally {
      _$NavManagerBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
currentNav: ${currentNav}
    ''';
  }
}
