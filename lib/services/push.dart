import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';

import 'package:exe/king/king.dart';

class PushService {
  final FirebaseMessaging _fcm = FirebaseMessaging();
  King king;

  PushService(King king) {
    this.king = king;
  }

  Future initialize() async {
    if (Platform.isIOS) {
      _fcm.requestNotificationPermissions(IosNotificationSettings());
    }
    _fcm.configure(
        // App is in foreground when push is received
        onMessage: (Map<String, dynamic> message) async {
      print('onMessage: $message');
      Map<String, String> data = message['data'];
      this.processNotification(data);
    },

        // App is opened when the push notification is clicked by the user
        onLaunch: (Map<String, dynamic> message) async {
      print('onLaunch: $message');
      Map<String, String> data = message['data'];
      this.processNotification(data);
    },

        // App is in background and brought to foreground when it is clicked
        // by the user
        onResume: (Map<String, dynamic> message) async {
      print('onResume: $message');
      Map<String, String> data = message['data'];
      this.processNotification(data);
    });
  }

  void processNotification(Map<String, String> data) {
    Uri uri = uriFromMessageData(data);
    switch (data['action']) {
      case 'app_link':
        this.king.navman.processAppLink(uri);
        break;

      default:
        throw ('ERROR: unrecognized push notification with data $data');
    }
  }

  Uri uriFromMessageData(Map<String, dynamic> data) {
    Uri uri = Uri(
      host: data['host'],
      path: data['path'],
      queryParameters: data['queryParameters'],
    );
  }
}
