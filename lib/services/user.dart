import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mobx/mobx.dart';

import 'package:exe/dad/dad.dart';
import 'package:exe/king/king.dart';

part 'user.g.dart';

double ZOOM = 14;
double ASHEVILLELAT = 35.5951;
double ASHEVILLELONG = -82.5515;

class User = UserBase with _$User;

abstract class UserBase with Store {
  //List<String> managesListings = [];
  King king;
  Gps gps = Gps();
  @observable
  String locale = 'en';
  // TODO: ? track user's last pov and set to that on login
  @observable
  bool povIsTenant = true;
  String userUuid = '';

  UserBase(this.king);

  Account get account {
    return this.king.dad.accountCache.getItem(this.userUuid);
  }

  void refreshAccount() {
    this.king.dad.accountCache.getItemFresh(this.userUuid);
  }
}

class Gps {
  BigInt expiration;
  @observable
  double lat;
  @observable
  double long;
  double speed;

  Gps() {
    // TODO: default these closer to user, at least in user's country
    this.lat = ASHEVILLELAT;
    this.long = ASHEVILLELONG;
  }

  void update() async {}

  @computed
  LatLng get latLng {
    return LatLng(this.lat, this.long);
  }

  //@computed
  CameraPosition get camera {
    return CameraPosition(
      target: this.latLng,
      zoom: ZOOM,
    );
  }
}
