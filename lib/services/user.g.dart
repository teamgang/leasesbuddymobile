// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$User on UserBase, Store {
  final _$localeAtom = Atom(name: 'UserBase.locale');

  @override
  String get locale {
    _$localeAtom.reportRead();
    return super.locale;
  }

  @override
  set locale(String value) {
    _$localeAtom.reportWrite(value, super.locale, () {
      super.locale = value;
    });
  }

  final _$povIsTenantAtom = Atom(name: 'UserBase.povIsTenant');

  @override
  bool get povIsTenant {
    _$povIsTenantAtom.reportRead();
    return super.povIsTenant;
  }

  @override
  set povIsTenant(bool value) {
    _$povIsTenantAtom.reportWrite(value, super.povIsTenant, () {
      super.povIsTenant = value;
    });
  }

  @override
  String toString() {
    return '''
locale: ${locale},
povIsTenant: ${povIsTenant}
    ''';
  }
}
