//import 'dart:collection';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:mobx/mobx.dart';

import 'package:exe/dad/dad.dart';
import 'package:exe/dad/explore_cacher.dart';
import 'package:exe/dad/google_place_cache.dart';
import 'package:exe/king/king.dart';
import 'package:exe/shared/oprows.dart';

part 'explore_query.g.dart';

void getGooglePlaceAndSetEquery(
  BuildContext context,
  String placeId,
  ExploreQuery equery,
) {
  var place = Dad.of(context).googlePlaceCache.getPlaceSync(placeId: placeId);
  equery.lat = place.lat;
  equery.long = place.long;
}

class ExploreQuery = ExploreQueryBase with _$ExploreQuery;

// TODO: build markers as on airbnb, with the price in the marker
abstract class ExploreQueryBase with Store {
  // initialize to true since on first load we must call api
  bool needToCallApi = true;
  bool dontTriggerShowRefresh = true;
  double lat = 35;
  double long = -80;
  bool isPageAnimating = false;
  @observable
  bool showRefresh = false;
  int selectedMarker = 0;

  ExploreFilterGang filterGang = ExploreFilterGang();
  ExploreListCacher exploreCacher;

  ExploreList exploreList;
  @observable
  ObservableSet<Marker> markers = ObservableSet<Marker>();
  Map<MarkerId, int> markerIdToIndex = <MarkerId, int>{};

  Function animateToPage;

  ExploreQueryBase(King king) {
    this.exploreCacher = ExploreListCacher(king, this);
  }

  clearFilterGang() {
    this.filterGang = ExploreFilterGang();
  }

  ExploreFilterGang cloneFilterGang() {
    var clone = ExploreFilterGang();
    var oprows = this.filterGang.oprows;
    oprows.keys.forEach((key) {
      clone.copyOprow(oprows[key]);
    });
    return clone;
  }

  generateUuid() {
    return '001';
  }

  generateFilterPayload() {
    Map<String, Object> payload = {};
    return payload;
  }

  @action
  onCameraIdle() {
    if (!this.dontTriggerShowRefresh) {
      this.showRefresh = true;
    }
  }

  @action
  tapRefresh() {
    this.needToCallApi = true;
    this.showRefresh = false;
    this.loadExploreList();
  }

  @action
  clearMarkers() {
    this.markers.clear();
  }

  @action
  addTacToMarkers(Tac tac, int selectedIndex) {
    var tac = this.exploreList.tacs[selectedIndex];
    var markerId = MarkerId(tac.uuid);
    var marker = Marker(
        markerId: markerId,
        position: LatLng(tac.lat, tac.lng),
        consumeTapEvents: true,
        icon: selectedIndex == this.selectedMarker
            ? BitmapDescriptor.defaultMarkerWithHue(BitmapDescriptor.hueOrange)
            : BitmapDescriptor.defaultMarker,
        onTap: () {
          this._onMarkerTap(markerId);
        });
    this.markers.add(marker);
  }

  // NOTE: make api calls through this function so that we can avoid making
  // excesssive api calls due to many build calls
  // TODO: does this need to be async?
  loadExploreList() async {
    // TODO: needToCallApi is meant to limit the number of times we process
    // the entire filter list for api calls. We should check this this is a
    // good idea and not a dumb idea
    if (this.needToCallApi) {
      this.needToCallApi = false;
      this.exploreList = await this.exploreCacher.getExploreList();

      this.clearMarkers();
      this.markerIdToIndex = <MarkerId, int>{};

      for (int i = 0; i < this.exploreList.tacs.length; i++) {
        var tac = this.exploreList.tacs[i];
        var markerId = MarkerId(tac.uuid);
        this.markerIdToIndex[markerId] = i;
        this.addTacToMarkers(tac, i);
      }
    }
  }

  _onMarkerTap(MarkerId markerId) {
    //print(markerId.value);
    int index = this.markerIdToIndex[markerId];
    this.selectedMarker = index;
    // TODO
    this.animateToPage(index);
    this.rebuildMarkersWithSelected(index);
  }

  rebuildMarkersWithSelected(int index) {
    this.clearMarkers();
    for (int i = 0; i < this.exploreList.tacs.length; i++) {
      var tac = this.exploreList.tacs[i];
      this.addTacToMarkers(tac, i);
    }
  }

  Tac getMarkerInfoFromIndex(int index) {
    return this.exploreList.tacs[index];
  }

  Tac getMarkerInfoFromMarkerId(MarkerId markerId) {
    var index = this.markerIdToIndex[markerId];
    return this.exploreList.tacs[index];
  }

  bool get hasMarkers {
    return this.markers.isNotEmpty;
  }
}

class ExploreFilterGang extends OprowGangBase {
  ExploreFilterGang() {
    this.addOprow('apartment');
    this.addOprow('house');
    this.addOprow('townhome');

    this.addOprow('bedrooms');
    this.addOprow('bathrooms');
  }
}
