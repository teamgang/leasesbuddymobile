// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'explore_query.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ExploreQuery on ExploreQueryBase, Store {
  final _$showRefreshAtom = Atom(name: 'ExploreQueryBase.showRefresh');

  @override
  bool get showRefresh {
    _$showRefreshAtom.reportRead();
    return super.showRefresh;
  }

  @override
  set showRefresh(bool value) {
    _$showRefreshAtom.reportWrite(value, super.showRefresh, () {
      super.showRefresh = value;
    });
  }

  final _$markersAtom = Atom(name: 'ExploreQueryBase.markers');

  @override
  ObservableSet<Marker> get markers {
    _$markersAtom.reportRead();
    return super.markers;
  }

  @override
  set markers(ObservableSet<Marker> value) {
    _$markersAtom.reportWrite(value, super.markers, () {
      super.markers = value;
    });
  }

  final _$ExploreQueryBaseActionController =
      ActionController(name: 'ExploreQueryBase');

  @override
  dynamic onCameraIdle() {
    final _$actionInfo = _$ExploreQueryBaseActionController.startAction(
        name: 'ExploreQueryBase.onCameraIdle');
    try {
      return super.onCameraIdle();
    } finally {
      _$ExploreQueryBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic tapRefresh() {
    final _$actionInfo = _$ExploreQueryBaseActionController.startAction(
        name: 'ExploreQueryBase.tapRefresh');
    try {
      return super.tapRefresh();
    } finally {
      _$ExploreQueryBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic clearMarkers() {
    final _$actionInfo = _$ExploreQueryBaseActionController.startAction(
        name: 'ExploreQueryBase.clearMarkers');
    try {
      return super.clearMarkers();
    } finally {
      _$ExploreQueryBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic addTacToMarkers(Tac tac, int selectedIndex) {
    final _$actionInfo = _$ExploreQueryBaseActionController.startAction(
        name: 'ExploreQueryBase.addTacToMarkers');
    try {
      return super.addTacToMarkers(tac, selectedIndex);
    } finally {
      _$ExploreQueryBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
showRefresh: ${showRefresh},
markers: ${markers}
    ''';
  }
}
