import 'dart:math';

final _random = new Random();

final double maxLat = 36;
final double maxLong = -78;
final double minLat = 35;
final double minLong = -81;

double randLat() {
  return minLat + _random.nextDouble() * (maxLat - minLat);
}

double randLong() {
  return minLong + _random.nextDouble() * (maxLong - minLong);
}
