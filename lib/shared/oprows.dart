import 'package:mobx/mobx.dart';

part 'oprows.g.dart';

// Classes for managing the values in option rows. Oprows are used in pages
// such as setting filters and setting the number of rooms in a listing.
class OprowGangBase {
  Map<String, Oprow> oprows = {};

  Oprow getOprow(String name) {
    return this.oprows[name];
  }

  dynamic getOprowValue(String name) {
    return this.getOprow(name).getValue();
  }

  setOprow(String name, dynamic value) {
    var opr = this.getOprow(name);
    if (opr == null) {
      print('ERROR: oprowsGang with name, value is null: $name, $value');
    }

    //NOTE: if opr.type is null, check the oprow defining file
    switch (opr.type) {
      case 'bool':
        this.oprows[name].value.b = value;
        break;
      case 'int':
        this.oprows[name].value.i = value;
        break;
      default:
        throw ('ERROR: oprowsGang has no type: ${opr.type}');
    }
  }

  newOprowFromName(String name) {
    Oprow opr;
    switch (name) {
      case 'apartment':
      case 'house':
      case 'townhome':
        opr = BoolOprow(name);
        break;
      case 'bedrooms':
      case 'bathrooms':
      case 'bathroomsFull':
      case 'bathroomsHalf':
        opr = IntOprow(name);
        break;
      default:
        throw ('ERROR: oprowsGang has no type for: $name');
    }
    return opr;
  }

  addOprow(String name) {
    Oprow opr = newOprowFromName(name);
    opr.name = name;
    this.oprows[name] = opr;
  }

  copyOprow(Oprow original) {
    Oprow opr = newOprowFromName(original.name);
    opr.name = original.name;
    opr.value.b = original.value.b;
    opr.value.i = original.value.i;
    this.oprows[original.name] = opr;
  }

  bool get isValid {
    //TODO: run an actual validation of fields
    return true;
  }
}

class Oprow {
  String name;
  String type;
  OprowValue value = OprowValue();

  Oprow(this.name);

  dynamic getValue() {
    return this.value.getValue();
  }
}

class OprowValue = OprowValueBase with _$OprowValue;

abstract class OprowValueBase with Store {
  String type;

  @observable
  bool b = false;

  @observable
  int i = 0;

  dynamic getValue() {
    throw UnimplementedError;
  }
}

class BoolOprow = BoolOprowBase with _$BoolOprow;

abstract class BoolOprowBase extends Oprow with Store {
  @override
  String type = 'bool';
  BoolOprowBase(name) : super(name);

  @action
  toggle() {
    this.value.b = !this.value.b;
  }

  @override
  bool getValue() {
    return this.value.b;
  }
}

class IntOprow = IntOprowBase with _$IntOprow;

abstract class IntOprowBase extends Oprow with Store {
  @override
  String type = 'int';
  int maxValue = 9;
  int minValue = 0;
  IntOprowBase(name, {this.minValue = 0, this.maxValue = 9}) : super(name);

  @action
  increment() {
    if (this.value.i < this.maxValue) {
      this.value.i++;
    }
  }

  @action
  decrement() {
    if (this.value.i > this.minValue) {
      this.value.i--;
    }
  }

  @override
  int getValue() {
    return this.value.i;
  }
}
