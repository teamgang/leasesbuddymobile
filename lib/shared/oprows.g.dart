// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'oprows.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$OprowValue on OprowValueBase, Store {
  final _$bAtom = Atom(name: 'OprowValueBase.b');

  @override
  bool get b {
    _$bAtom.reportRead();
    return super.b;
  }

  @override
  set b(bool value) {
    _$bAtom.reportWrite(value, super.b, () {
      super.b = value;
    });
  }

  final _$iAtom = Atom(name: 'OprowValueBase.i');

  @override
  int get i {
    _$iAtom.reportRead();
    return super.i;
  }

  @override
  set i(int value) {
    _$iAtom.reportWrite(value, super.i, () {
      super.i = value;
    });
  }

  @override
  String toString() {
    return '''
b: ${b},
i: ${i}
    ''';
  }
}

mixin _$BoolOprow on BoolOprowBase, Store {
  final _$BoolOprowBaseActionController =
      ActionController(name: 'BoolOprowBase');

  @override
  dynamic toggle() {
    final _$actionInfo = _$BoolOprowBaseActionController.startAction(
        name: 'BoolOprowBase.toggle');
    try {
      return super.toggle();
    } finally {
      _$BoolOprowBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''

    ''';
  }
}

mixin _$IntOprow on IntOprowBase, Store {
  final _$IntOprowBaseActionController = ActionController(name: 'IntOprowBase');

  @override
  dynamic increment() {
    final _$actionInfo = _$IntOprowBaseActionController.startAction(
        name: 'IntOprowBase.increment');
    try {
      return super.increment();
    } finally {
      _$IntOprowBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic decrement() {
    final _$actionInfo = _$IntOprowBaseActionController.startAction(
        name: 'IntOprowBase.decrement');
    try {
      return super.decrement();
    } finally {
      _$IntOprowBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''

    ''';
  }
}
