import 'package:exe/shared/oprows.dart';

class RoomsOprowGang extends OprowGangBase {
  RoomsOprowGang() {
    this.addOprow('bedrooms');
    this.addOprow('bathroomsFull');
    this.addOprow('bathroomsHalf');
  }
}
