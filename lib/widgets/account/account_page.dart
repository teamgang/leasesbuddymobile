import 'package:meta/meta.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:exe/dad/account_cache.dart';
import 'package:exe/king/king.dart';
import 'package:exe/services/user.dart';
import 'package:exe/widgets/account/auth_page.dart';
import 'package:exe/widgets/account/change_bio.dart';
import 'package:exe/widgets/account/change_language.dart';
import 'package:exe/widgets/account/change_username.dart';
import 'package:exe/widgets/account/erect_agent.dart';
import 'package:exe/widgets/erectors/verify/set_name.dart';
import 'package:exe/widgets/based/images.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/based/image_upload_page.dart';

//TODO: make this a TenantPage which is analogous to an AgentPage
class AccountPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var king = King.of(context);
    var user = king.user;
    var account = king.userAccount;

    return BasedScaffold(
      body: Observer(
        builder: (_) => !king.authService.isSignedIn
            ? AuthPage()
            : Container(
                child: LayoutBuilder(builder:
                    (BuildContext context, BoxConstraints viewportConstraints) {
                  return SingleChildScrollView(
                    key: Key(XKeys.profileOptionsList),
                    child: ConstrainedBox(
                      constraints: BoxConstraints(
                        minHeight: viewportConstraints.maxHeight,
                      ),
                      child: Column(
                        children: <Widget>[
                          SizedBox(height: 20),
                          UserAvatarHeader(user),
                          SizedBox(height: 20),

                          //
                          AccountCluster(account),
                          SizedBox(height: 20),
                          SettingsCluster(account),
                          SizedBox(height: 20),
                          MoreCluster(account),
                        ],
                      ),
                    ),
                  );
                }),
              ),
      ),
    );
  }
}

class OptionCluster extends StatelessWidget {
  String title;
  List<Widget> children;
  OptionCluster({this.title, this.children});

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(16, 16, 16, 4),
            child: Text(this.title, style: TextStyle(color: Colors.green)),
          ),
          ...this.children,
        ],
      ),
    );
  }
}

class OptionRow extends StatelessWidget {
  String option;
  String value;
  Function onTap;
  OptionRow({this.option, this.value, this.onTap});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        this.onTap();
      },
      child: Padding(
        padding: EdgeInsets.fromLTRB(16, 14, 16, 14),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(this.value, style: Theme.of(context).textTheme.body1),
            SizedBox(height: 4),
            Text(this.option, style: Theme.of(context).textTheme.body2),
          ],
        ),
      ),
    );
  }
}

class OptionRowIcon extends StatelessWidget {
  IconData icon;
  Key tapKey;
  Function onTap;
  String option;
  // TODO: semanticLabel
  OptionRowIcon(
      {@required this.icon,
      @required this.onTap,
      @required this.option,
      this.tapKey});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      key: this.tapKey,
      onTap: () {
        this.onTap();
      },
      child: Padding(
        padding: EdgeInsets.fromLTRB(16, 14, 16, 14),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(height: 4),
            Row(
              children: <Widget>[
                Icon(icon),
                SizedBox(width: 12),
                Text(this.option, style: Theme.of(context).textTheme.body1),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class ChangeNotifications extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BasedScaffold(body: Text('Change notifications'));
  }
}

class UserAvatarHeader extends StatelessWidget {
  User user;
  UserAvatarHeader(this.user);

  @override
  Widget build(BuildContext context) {
    var king = King.of(context);
    return Observer(
      builder: (_) => Padding(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Row(children: <Widget>[
          Expanded(
            flex: 1,
            child: Stack(children: <Widget>[
              Center(
                child: BasedCircleAvatar(
                  src: user.account.avatarUrl,
                  radius: 44,
                  altText: user.account.avatarAltText,
                ),
              ),
              Positioned(
                bottom: 0,
                right: 0,
                child: IconButton(
                  icon: Icon(Icons.edit, color: Colors.grey),
                  onPressed: () {
                    Navigator.of(context).push(
                        MaterialPageRoute(builder: (_) => ImageUploadPage()));
                  },
                ),
              ),
            ]),
          ),
          Expanded(
            flex: 2,
            child: Column(children: <Widget>[
              SizedBox(height: 10),
              user.povIsTenant
                  ? Text('You are in tenant mode.')
                  : Text('You are in agent mode.'),
              SizedBox(height: 8),
              user.account.hasAgentAccount
                  ? user.povIsTenant
                      ? FlatButton(
                          key: Key(XKeys.profileTogglePovButton),
                          padding: EdgeInsets.fromLTRB(16, 12, 20, 12),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                          child: Text('Switch to agent mode',
                              style:
                                  TextStyle(fontSize: 14, color: Colors.black)),
                          onPressed: () {
                            user.povIsTenant = false;
                          },
                        )
                      //

                      : FlatButton(
                          key: Key(XKeys.profileTogglePovButton),
                          padding: EdgeInsets.fromLTRB(16, 12, 20, 12),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(8)),
                          child: Text('Switch to tenant mode',
                              style:
                                  TextStyle(fontSize: 14, color: Colors.black)),
                          onPressed: () {
                            user.povIsTenant = true;
                          },
                        )
                  //

                  : FlatButton(
                      key: Key(XKeys.erectAgentStart),
                      padding: EdgeInsets.fromLTRB(16, 12, 20, 12),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                      child: Text('Become an agent and list your property!',
                          style: TextStyle(fontSize: 14, color: Colors.black)),
                      onPressed: () {
                        king.user.refreshAccount;
                        Navigator.of(context).push(MaterialPageRoute(
                            builder: (_) => BecomeAgentPage()));
                      },
                    ),
            ]),
          ),
        ]),
      ),
    );
  }
}

class AccountCluster extends StatelessWidget {
  Account account;
  AccountCluster(this.account);

  @override
  Widget build(BuildContext context) {
    return OptionCluster(
      title: 'Account',
      children: <Widget>[
        OptionRow(
            option: 'Username',
            value: account.username,
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          ChangeUsername(account)));
            }),
        Divider(color: Colors.black, height: 0),
        OptionRow(
            option: 'Bio',
            value: account.bio,
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => ChangeBio(account)));
            }),
        Divider(color: Colors.black, height: 0),
        OptionRow(
            option: 'My Reviews',
            value: account.bio,
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => ChangeBio(account)));
            }),
      ],
    );
  }
}

class SettingsCluster extends StatelessWidget {
  Account account;
  SettingsCluster(this.account);

  @override
  Widget build(BuildContext context) {
    return OptionCluster(
      title: 'Settings',
      children: <Widget>[
        OptionRowIcon(
            option: 'Notifications',
            icon: Icons.notifications_none,
            onTap: () {
              //Navigator.push(
              //context,
              //MaterialPageRoute(
              //builder: (BuildContext context) =>
              //ChangeUsername()));
            }),
        Divider(color: Colors.black, height: 0),
        OptionRowIcon(
            option: 'Language',
            icon: Icons.language,
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) =>
                          ChangeLanguage(account)));
            }),
      ],
    );
  }
}

class MoreCluster extends StatelessWidget {
  Account account;
  MoreCluster(this.account);

  @override
  Widget build(BuildContext context) {
    var king = King.of(context);

    return OptionCluster(
      title: 'More',
      children: <Widget>[
        OptionRowIcon(
            option: 'Ask a Question',
            icon: Icons.notifications_none,
            onTap: () {
              //Navigator.push(
              //context,
              //MaterialPageRoute(
              //builder: (BuildContext context) =>
              //ChangeUsername()));
            }),
        Divider(color: Colors.black, height: 0),
        OptionRowIcon(
            option: 'Laird FAQ',
            icon: Icons.language,
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => ChangeBio(account)));
            }),
        //

        Divider(color: Colors.black, height: 0),
        OptionRowIcon(
            option: 'Privacy Policy',
            icon: Icons.language,
            onTap: () {
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (BuildContext context) => ChangeBio(account)));
            }),
        Divider(color: Colors.black, height: 0),
        OptionRowIcon(
            option: 'Sign Out',
            icon: Icons.language,
            tapKey: Key(XKeys.profileSignOutButton),
            onTap: () {
              king.authService.signOut();
            }),
      ],
    );
  }
}
