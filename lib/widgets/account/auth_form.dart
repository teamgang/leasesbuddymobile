import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';

import 'package:exe/king/king.dart';
import 'package:exe/widgets/account/auth_page.dart';
import 'package:exe/widgets/based/forms.dart';

class AuthFormFields {
  String email;
  String isoCode = '+1';
  String password;
}

class AuthForm extends StatefulWidget {
  AuthPages whichForm;
  AuthForm(this.whichForm);

  @override
  AuthFormState createState() => AuthFormState();
}

class AuthFormState extends State<AuthForm> {
  final _formKey = GlobalKey<FormState>();
  bool _isLoading = false;
  bool _submitFailed = false;
  AuthFormFields fields = AuthFormFields();

  String validateEmail(String email) {
    if (email != '') {
      if (email.length < 5) {
        return 'Email is too short';
      }
      int atPosition = email.indexOf('@');
      int dotPosition = email.lastIndexOf('.');
      if (dotPosition == -1 || atPosition == -1 || dotPosition <= atPosition) {
        return 'Email format is invalid';
      }
    }
    if (email == '') {
      return 'You must provide an email.';
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    String banner = '';
    switch (widget.whichForm) {
      case AuthPages.register:
        banner = 'Create your account.';
        break;
      case AuthPages.signIn:
        banner = 'Sign in to your account.';
        break;
    }

    return Container(
      color: Colors.grey[150],
      child: Form(
        key: _formKey,
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              SizedBox(height: 20),
              Text(banner, style: TextStyle(fontSize: 16)),
              TextFormField(
                key: Key(XKeys.authFormEmailField),
                decoration: InputDecoration(
                  labelText: 'Email',
                ),
                maxLength: 100,
                autovalidate: true,
                autocorrect: false,
                onChanged: (value) {
                  if (this._submitFailed) {
                    setState(() {
                      this._submitFailed = false;
                    });
                  }
                  fields.email = value.trim();
                },
                validator: (value) {
                  return validateEmail(value);
                },
              ),
              SizedBox(height: 14),
              BasedCreatePassword(
                textFieldKey: Key(XKeys.authFormPasswordField),
                onChanged: (password) {
                  if (this._submitFailed) {
                    setState(() {
                      this._submitFailed = false;
                    });
                  }
                  fields.password = password;
                },
              ),
              SizedBox(height: 10),
              _buildButton(context),
              SizedBox(height: 24),
              _buildErrors(context),
            ],
          ),
        ),
      ),
    );
  }

  Widget _buildErrors(BuildContext context) {
    if (!_submitFailed) {
      return SizedBox(height: 0);
    } else {
      switch (widget.whichForm) {
        case AuthPages.register:
          // TODO: if one of these fails, don't show the error message for the
          // other
          return Text('Registration failed.',
              style: TextStyle(color: Colors.red));
          break;

        case AuthPages.signIn:
          return Text('Sign in failed.', style: TextStyle(color: Colors.red));
          break;
      }
    }
  }

  Widget _buildButton(BuildContext context) {
    if (_isLoading) {
      return Center(child: CircularProgressIndicator());
    } else {
      return RaisedButton(
        key: Key(XKeys.authFormRegisterOrSignInButton),
        onPressed: () => _onSubmit(context),
        child: widget.whichForm == AuthPages.register
            ? Text('Register')
            : Text('Sign in'),
      );
    }
  }

  void _onSubmit(BuildContext context) async {
    var auth = King.of(context).authService;
    setState(() {
      _isLoading = true;
    });
    if (_formKey.currentState.validate()) {
      switch (widget.whichForm) {
        case AuthPages.register:
          await auth.registerWithEmailAndSignIn(
              email: fields.email, password: fields.password);
          break;

        case AuthPages.signIn:
          await auth.signInWithEmail(
              email: fields.email, password: fields.password);
          break;
      }
    }

    setState(() {
      _isLoading = false;
    });

    if (!auth.isSignedIn) {
      this._submitFailed = true;
    }
  }
}
