import 'dart:io';
import 'dart:async';
import 'package:flutter/material.dart';

import 'package:exe/king/king.dart';
import 'package:exe/widgets/account/auth_form.dart';

enum AuthPages { signIn, register }

class AuthPage extends StatefulWidget {
  @override
  AuthPageState createState() => AuthPageState();
}

class AuthPageState extends State<AuthPage> {
  AuthPages viewingForm = AuthPages.register;

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.grey[150],
      child: Padding(
        padding: EdgeInsets.fromLTRB(12, 0, 12, 0),
        child: LayoutBuilder(
          builder: (BuildContext context, BoxConstraints viewportConstraints) {
            return SingleChildScrollView(
              child: ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight: viewportConstraints.maxHeight,
                ),
                child: Column(
                  children: <Widget>[
                    SizedBox(height: 60),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        InkWell(
                            key: Key(XKeys.authFormRegisterTabButton),
                            child: PageButton('Register',
                                selected: this.viewingForm == AuthPages.register
                                    ? true
                                    : false),
                            onTap: () {
                              if (this.viewingForm != AuthPages.register) {
                                setState(() {
                                  this.viewingForm = AuthPages.register;
                                });
                              }
                            }),
                        SizedBox(width: 2),
                        InkWell(
                            key: Key(XKeys.authFormSignInTabButton),
                            child: PageButton('Sign in',
                                selected: this.viewingForm == AuthPages.signIn
                                    ? true
                                    : false),
                            onTap: () {
                              if (this.viewingForm != AuthPages.signIn) {
                                setState(() {
                                  this.viewingForm = AuthPages.signIn;
                                });
                              }
                            }),
                      ],
                    ),
                    AuthForm(this.viewingForm),
                    SizedBox(height: 20),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}

class PageButton extends StatelessWidget {
  String text;
  bool selected;
  PageButton(this.text, {this.selected});

  @override
  Widget build(BuildContext context) {
    Color bottomColor = this.selected ? Colors.yellow[700] : Colors.grey[400];

    return Container(
      width: 140,
      padding: EdgeInsets.all(10.0),
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(width: 8.0, color: bottomColor),
        ),
      ),
      child: Text(this.text,
          textAlign: TextAlign.center, style: TextStyle(fontSize: 30)),
    );
  }
}
