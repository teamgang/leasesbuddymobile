import 'package:flutter/material.dart';

import 'package:exe/king/king.dart';
import 'package:exe/lip/lip.dart';
import 'package:exe/dad/dad.dart';
import 'package:exe/widgets/based/styles.dart';
import 'package:exe/widgets/gibs/progress_indicator.dart';

class ChangeBio extends StatefulWidget {
  Account account;
  ChangeBio(this.account);

  @override
  _ChangeBioState createState() => _ChangeBioState();
}

class _ChangeBioState extends State<ChangeBio> {
  bool requestInProgress = false;
  String failure = '';
  String endpoint;
  String currentValue = '';
  int maxLength = 800;

  @override
  initState() {
    super.initState();
    this.endpoint = 'tenants/${widget.account.userUuid}';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          this.requestInProgress
              ? ProgressIndicatorGib()
              : IconButton(
                  icon: Icon(Icons.check),
                  onPressed: () {
                    this._onSubmit(context);
                  },
                ),
        ],
      ),
      body: SizedBox.expand(
        child: Container(
          child: Padding(
            padding: EdgeInsets.all(24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                //

                TextFormField(
                  autovalidate: true,
                  enabled: !this.requestInProgress,
                  initialValue: widget.account.bio,
                  keyboardType: TextInputType.multiline,
                  maxLength: this.maxLength,
                  maxLines: 12,
                  minLines: 6,
                  style: Theme.of(context).textTheme.headline6,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Biography',
                    counterText: '${this.currentValue.length}/$maxLength',
                  ),
                  onChanged: (newValue) {
                    this.currentValue = newValue;
                  },
                  validator: (value) {
                    if (value.length > this.maxLength) {
                      return 'Description must be under $maxLength characters';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 12),
                Text('Edit your bio.', style: optionInfoStyle()),
                SizedBox(height: 12),
                (this.failure != '')
                    ? Text(failure, style: failureStyle())
                    : SizedBox(height: 0),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<String> _submitChange(BuildContext context) async {
    String failure = null;
    var payload = {
      'bio': this.currentValue,
    };

    ApiResponse ares = await King.of(context).lip.api(
          this.endpoint,
          payload: payload,
          method: HttpMethod.patch,
        );

    if (ares.isOk()) {
    } else {
      failure = 'Failed to change ${this.endpoint} to ${this.currentValue}';
    }
    return failure;
  }

  _onSubmit(BuildContext context) async {
    this.failure = '';
    if (!this.requestInProgress) {
      this.setState(() {
        this.requestInProgress = true;
      });

      String failure = await this._submitChange(context);

      this.setState(() {
        this.requestInProgress = false;
      });
      if (failure != null) {
        var king = King.of(context);
        king.dad.accountCache.flagForUpdate(widget.account.userUuid);
        Navigator.of(context).pop();
      } else {
        this.failure = 'Failed to submit change.';
      }
    }
  }
}
