import 'package:flutter/material.dart';

import 'package:exe/king/king.dart';
import 'package:exe/lip/lip.dart';
import 'package:exe/dad/dad.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/gibs/progress_indicator.dart';

Map<String, String> localeName = {
  'en': 'English',
  'ru': 'Russia',
};

class ChangeLanguage extends StatefulWidget {
  Account account;
  ChangeLanguage(this.account);

  @override
  _ChangeLanguageState createState() => _ChangeLanguageState();
}

class _ChangeLanguageState extends State<ChangeLanguage> {
  bool requestInProgress = false;
  String failure = '';
  String endpoint;
  String currentValue = '';
  int maxLength = 800;

  @override
  initState() {
    super.initState();
    this.endpoint = 'tenants/${widget.account.userUuid}';
  }

  @override
  Widget build(BuildContext context) {
    var user = King.of(context).user;
    return BasedScaffold(
      useBasedAppBar: true,
      body: SizedBox.expand(
        child: Container(
          child: Padding(
            padding: EdgeInsets.all(24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                SelectLocaleButton('en'),
                SizedBox(height: 14),
                SelectLocaleButton('ru'),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class SelectLocaleButton extends StatelessWidget {
  String locale;
  SelectLocaleButton(this.locale);

  changeLocale(BuildContext context, String newLocale) {
    var user = King.of(context).user;
    user.locale = newLocale;
    Navigator.of(context).pop();
  }

  @override
  Widget build(BuildContext context) {
    var user = King.of(context).user;

    return FlatButton.icon(
      padding: EdgeInsets.fromLTRB(16, 12, 20, 12),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      icon: Icon(Icons.refresh),
      label: Text(localeName[this.locale]),
      onPressed: user.locale == this.locale
          ? null
          : () {
              changeLocale(context, this.locale);
            },
    );
  }
}
