import 'package:flutter/material.dart';

import 'package:exe/king/king.dart';
import 'package:exe/dad/dad.dart';
import 'package:exe/lip/lip.dart';
import 'package:exe/widgets/based/styles.dart';
import 'package:exe/widgets/gibs/progress_indicator.dart';

class ChangeUsername extends StatefulWidget {
  Account account;
  ChangeUsername(this.account);

  @override
  _ChangeUsernameState createState() => _ChangeUsernameState();
}

class _ChangeUsernameState extends State<ChangeUsername> {
  bool requestInProgress = false;
  String failure = '';
  String endpoint;
  String currentValue = '';

  @override
  initState() {
    super.initState();
    this.endpoint = 'tenants/${widget.account.userUuid}';
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: <Widget>[
          this.requestInProgress
              ? ProgressIndicatorGib()
              : IconButton(
                  icon: Icon(Icons.check),
                  onPressed: () {
                    this._onSubmit(context);
                  },
                ),
        ],
      ),
      body: SizedBox.expand(
        child: Container(
          child: Padding(
            padding: EdgeInsets.all(24.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                //

                TextFormField(
                    initialValue: widget.account.username,
                    enabled: !this.requestInProgress,
                    style: TextStyle(fontSize: 20),
                    onChanged: (newValue) {
                      this.currentValue = newValue;
                    }),
                SizedBox(height: 12),
                Text('Edit your username.', style: optionInfoStyle()),
                SizedBox(height: 12),
                Text('Username must be at least 4 characters.',
                    style: optionInfoStyle()),
                SizedBox(height: 12),
                (this.failure != '')
                    ? Text(failure, style: failureStyle())
                    : SizedBox(height: 0),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<String> _submitChange(BuildContext context) async {
    String failure = null;
    var payload = {
      'username': this.currentValue,
    };

    ApiResponse ares = await King.of(context).lip.api(
          this.endpoint,
          payload: payload,
          method: HttpMethod.patch,
        );

    if (ares.isOk()) {
    } else {
      failure = 'Failed to change ${this.endpoint} to ${this.currentValue}';
    }
    return failure;
  }

  _onSubmit(BuildContext context) async {
    this.failure = '';
    if (!this.requestInProgress) {
      this.setState(() {
        this.requestInProgress = true;
      });

      String failure = await this._submitChange(context);

      this.setState(() {
        this.requestInProgress = false;
      });
      if (failure != '') {
        var king = King.of(context);
        king.dad.accountCache.flagForUpdate(widget.account.userUuid);
        Navigator.of(context).pop();
      } else {
        this.failure = 'Failed to submit change.';
      }
    }
  }
}
