import 'dart:async';
import 'package:flutter/material.dart';

import 'package:exe/king/king.dart';
import 'package:exe/lip/lip.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/based/styles.dart';
import 'package:exe/widgets/gibs/progress_indicator.dart';

class BecomeAgentPage extends StatelessWidget {
  String failure = '';

  @override
  Widget build(BuildContext context) {
    var king = King.of(context);

    if (king.user.account.hasAgentAccount) {
      Navigator.of(context).pop();
    }

    return BasedScaffold(
      body: CustomScrollView(
        shrinkWrap: true,
        slivers: <Widget>[
          SliverAppBar(
            floating: true,
            pinned: false,
            snap: true,
            leading: InkWell(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 14),
                  child: Icon(Icons.close),
                ),
                onTap: () {
                  Navigator.pop(context);
                }),
          ),
          SliverPadding(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            sliver: SliverList(
              delegate: SliverChildListDelegate([
                SizedBox(height: 8),
                Text(
                  'Become an Agent',
                  style: Theme.of(context).textTheme.headline4,
                ),
                SizedBox(height: 14),
                Text(
                    'Becoming an agent gives you the power to create and manage property listings.',
                    style: Theme.of(context).textTheme.subtitle1),
                SizedBox(height: 14),
                Text(
                    'An agent and tenants are encouraged to record their lease with LeaseBuddy. Doing so allows the agent and tenants to review each other at the end of a lease.',
                    style: Theme.of(context).textTheme.subtitle1),
                SizedBox(height: 14),
                Text(
                    'In order to unlock most agent actions, you are required to verify your identity with LeaseBuddy and to accept our terms & conditions.',
                    style: Theme.of(context).textTheme.subtitle1),
                SizedBox(height: 14),
                Text(
                    'You can switch between agent and tenant modes from the Account screen.',
                    style: Theme.of(context).textTheme.subtitle1),
                SizedBox(height: 14),
                Text('Do you wish to become an agent?',
                    style: Theme.of(context).textTheme.subtitle1),
                SizedBox(height: 14),
                SizedBox(height: 8),
                //Text(this.failure),
                this.failure == '' ? SizedBox.shrink() : Text(this.failure),

                Row(children: <Widget>[
                  RaisedButton(
                    key: Key(XKeys.erectAgentConfirmNo),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                    child: Text('No'),
                  ),
                  RaisedButton(
                    key: Key(XKeys.erectAgentConfirmYes),
                    onPressed: () async {
                      var king = King.of(context);
                      ApiResponse ares = await king.lip
                          .api('agents/create', method: HttpMethod.post);
                      if (ares.isOk()) {
                        king.user.account.hasAgentAccount = true;
                        Navigator.of(context).pop();
                      } else if (ares.statusCode == 422) {
                        // TODO: use a code other than 422
                        king.user.account.hasAgentAccount = true;
                        Navigator.of(context).pop();
                      } else {
                        return 'Request failed - please try again.';
                      }
                      // TODO: if agent already exists, notify user and close
                      // screen
                      // if agent failed to be created, notify user and ask
                      // to try again
                    },
                    child: Text('Yes'),
                  ),
                ]),
              ]),
            ),
          ),
        ],
      ),
    );
  }
}
