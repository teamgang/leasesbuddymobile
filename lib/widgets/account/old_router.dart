import 'dart:async';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
//import 'package:google_sign_in/google_sign_in.dart';

import 'package:exe/king/king.dart';
import 'package:exe/widgets/based/wrappers.dart';
import 'package:exe/widgets/auth/register.dart';

class AuthRouterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Navigator(
      initialRoute: 'auth/select',
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;
        switch (settings.name) {
          case 'auth/select':
            builder = (c) => BasedPaddedWrapper(AuthSelectPage());
            break;
          case 'auth/register':
            builder = (c) => BasedPaddedWrapper(RegisterPage());
            break;
          default:
            throw Exception('Invalid route: ${settings.name}');
        }
        return MaterialPageRoute(builder: builder, settings: settings);
      },
    );
  }
}

class AuthSelectPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var user = King.of(context).user;
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          RaisedButton(
            child: Text('Register'),
            onPressed: () {
              //Navigator.pushReplacementNamed(context, 'auth/register');
              Navigator.pushNamed(context, 'auth/register');
            },
          ),
          RaisedButton(
            child: Text('Sign in anonymously'),
            onPressed: () {
              user.signInAnonymously();
            },
          ),
          RaisedButton(
            child: Text('Sign in with email'),
            onPressed: () async {
              await user.signInWithEmail("ok", "aer");
              if (user.signedIn()) {
                user.pushToHomeNav();
              } else {
                Scaffold.of(context)
                    .showSnackBar(SnackBar(content: Text('Could not sign in')));
              }
            },
          ),
          RaisedButton(
            child: Text('Sign in as 2'),
            onPressed: () async {
              await user.signInWithEmail("2@2.com", "1234");
              if (user.signedIn()) {
                user.pushToHomeNav();
              } else {
                Scaffold.of(context)
                    .showSnackBar(SnackBar(content: Text('Could not sign in')));
              }
            },
          ),
          RaisedButton(
            child: Text('Sign in as 9'),
            onPressed: () async {
              await user.signInWithEmail("9@9.com", "1234");
              if (user.signedIn()) {
                user.pushToHomeNav();
              } else {
                Scaffold.of(context)
                    .showSnackBar(SnackBar(content: Text('Could not sign in')));
              }
            },
          ),
        ],
      ),
    );
  }
}
