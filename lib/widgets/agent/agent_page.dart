import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:exe/dad/dad.dart';
import 'package:exe/king/king.dart';
import 'package:exe/widgets/based/scaffold.dart';

class AgentPage extends StatelessWidget {
  String agentUuid = "";
  AgentPage({this.agentUuid});

  @override
  Widget build(BuildContext context) {
    var agent = King.of(context).dad.agentCache.getItem(this.agentUuid);

    return BasedScaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            floating: true,
            pinned: false,
            snap: true,
            leading: IconButton(
              tooltip: 'Back',
              icon: Icon(Icons.arrow_back),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            //flexibleSpace: FlexibleSpaceBar(
            //title: Text('Search Filters'),
            //),
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              AgentName(agent),
              SizedBox(height: 20),
              Text('${agent.phone}'),
              SizedBox(height: 20),
              Text('${agent.email}'),
              SizedBox(height: 20),
              AgentBio(agent),
              SizedBox(height: 20),
            ]),
          ),
        ],
      ),
    );
  }
}

class AgentName extends StatelessWidget {
  Agent agent;
  AgentName(this.agent);

  @override
  Widget build(BuildContext context) {
    return Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Column(children: <Widget>[
            Text('listed by ${agent.name}', style: TextStyle(fontSize: 20)),
            Text('${agent.reviewAverage}')
          ]),
          Text('agentPic', style: TextStyle(fontSize: 20)),
        ]);
  }
}

class AgentBio extends StatelessWidget {
  Agent agent;
  AgentBio(this.agent);

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Text('About', style: TextStyle(fontSize: 20)),
      Text('${agent.bio}'),
      // TODO
      Text('Expand...'),
    ]);
  }
}
