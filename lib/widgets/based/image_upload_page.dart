import 'dart:io';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'package:exe/widgets/based/image_crop_page.dart';

class ImageUploadPage extends StatefulWidget {
  @override
  _ImageUploadPageState createState() => _ImageUploadPageState();
}

class _ImageUploadPageState extends State<ImageUploadPage> {
  File _imageFile;
  final picker = ImagePicker();

  Future getImageFromCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        _imageFile = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  Future getImageFromGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        _imageFile = File(pickedFile.path);
      } else {
        print('No image selected.');
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(height: 28),
            Row(
              //crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Flexible(
                  flex: 2,
                  child: FlatButton(
                    child: Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
                Flexible(
                  flex: 4,
                  child: RaisedButton(
                    child: Text('Use camera'),
                    onPressed: getImageFromCamera,
                  ),
                ),
                Flexible(
                  flex: 4,
                  child: RaisedButton(
                    child: Text('Select from gallery'),
                    onPressed: getImageFromGallery,
                  ),
                ),
                Flexible(
                  flex: 2,
                  child: FlatButton(
                    child: Icon(Icons.check),
                    onPressed: () {
                      //Navigator.of(context).pop();
                      print('nav to crop');
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (BuildContext context) =>
                                  ImageCropPage(imageFile: _imageFile)));
                    },
                  ),
                ),
              ],
            ),
            SizedBox(height: 10),
            Center(
              child: Padding(
                padding: EdgeInsets.all(16),
                child: _imageFile == null
                    ? Text('No image selected')
                    : Image.file(_imageFile),
              ),
            ),
            SizedBox(height: 10),
          ],
        ),
      ),
    );
  }
}
