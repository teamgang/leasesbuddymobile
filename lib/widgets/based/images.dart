import 'package:meta/meta.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';

import 'package:exe/king/king.dart';

class BasedCircleAvatar extends StatelessWidget {
  final String altText;
  final double radius;
  final String src;
  BasedCircleAvatar(
      {@required this.altText, this.radius = 80, @required this.src});

  @override
  Widget build(BuildContext context) {
    var king = King.of(context);
    return Container(
      height: this.radius * 2,
      width: this.radius * 2,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(const Radius.circular(50.0)),
      ),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(this.radius),
        child: BasedFadeInImage(
          altText: this.altText,
          backgroundColor: Colors.orange,
          src: this.src,
        ),
      ),
    );
  }
}

class BasedSizedImage extends StatelessWidget {
  final String altText;
  final Color backgroundColor;
  final double height;
  final bool usePhotoview;
  final String src;
  final double width;
  BasedSizedImage(
      {@required this.altText,
      this.backgroundColor = Colors.blue,
      this.height = 120,
      this.usePhotoview = false,
      @required this.src,
      this.width = null});

  @override
  Widget build(BuildContext context) {
    var fadeInImage = BasedFadeInImage(
        altText: this.altText,
        backgroundColor: this.backgroundColor,
        height: this.height,
        src: this.src,
        width: this.width);

    if (this.usePhotoview) {
      return PhotoView.customChild(
        child: Container(
          height: this.height,
          width: this.width,
          color: this.backgroundColor,
          child: fadeInImage,
        ),
      );
    } else {
      return fadeInImage;
    }
  }
}

class BasedFadeInImage extends StatelessWidget {
  final String altText;
  final Color backgroundColor;
  final double height;
  final String src;
  final double width;
  BasedFadeInImage(
      {@required this.altText,
      this.backgroundColor = Colors.blue,
      this.height = 120,
      @required this.src,
      this.width = null});

  @override
  Widget build(BuildContext context) {
    var king = King.of(context);

    String url;
    if (this.src.length >= 4 && this.src.substring(0, 4) == 'http') {
      url = this.src;
    } else {
      url = '${king.conf.imagesUrl}/${this.src}';
    }

    // TODO: either catch the socketexception or only use this when we are offline
    url = 'https://via.placeholder.com/350x150';

    return FadeInImage.assetNetwork(
      placeholder: 'assets/default/loading.png',
      height: this.height,
      width: this.width,
      image: url,
      placeholderErrorBuilder: (
        BuildContext context,
        Object error,
        StackTrace stackTrace,
      ) {
        return Container(
          height: this.height,
          width: this.width,
          color: this.backgroundColor,
          child: Center(
            child: Text(this.altText, style: TextStyle(fontSize: 20)),
          ),
        );
      },
      imageErrorBuilder: (
        BuildContext context,
        Object error,
        StackTrace stackTrace,
      ) {
        return Container(
          height: this.height,
          width: this.width,
          color: this.backgroundColor,
          child: Center(
            child: Text(this.altText, style: TextStyle(fontSize: 20)),
          ),
        );
      },
    );
  }
}
