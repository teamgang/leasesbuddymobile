import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:exe/king/king.dart';

class BasedNavBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var navman = King.of(context).navman;
    var user = King.of(context).user;
    return Observer(
      builder: (_) => BottomNavigationBar(
          currentIndex: navman.currentNavIndex,
          backgroundColor: Colors.white,
          selectedItemColor: Colors.blue[700],
          unselectedItemColor: Colors.grey[800],
          onTap: (int newIndex) {
            navman.switchToNavIndex(newIndex, user.povIsTenant);
          },
          type: BottomNavigationBarType.fixed,
          items: <BottomNavigationBarItem>[
            user.povIsTenant
                ? BottomNavigationBarItem(
                    icon: Icon(
                      Icons.search,
                      key: Key(XKeys.navExplore),
                    ),
                    label: 'Explore',
                  )
                : BottomNavigationBarItem(
                    icon: Icon(
                      Icons.dashboard,
                      key: Key(XKeys.navManage),
                    ),
                    label: 'Manage',
                  ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.favorite,
                key: Key(XKeys.navFavorite),
              ),
              label: 'Saved',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.chat_bubble,
                key: Key(XKeys.navConnect),
              ),
              label: 'Connect',
            ),
            BottomNavigationBarItem(
              icon: Icon(
                Icons.account_circle,
                key: Key(XKeys.navProfile),
              ),
              label: 'Profile',
            ),
          ]),
    );
  }
}
