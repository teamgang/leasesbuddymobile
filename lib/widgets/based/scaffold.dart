import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:exe/widgets/based/nav_bar.dart';

class BasedScaffold extends StatelessWidget {
  PreferredSizeWidget appBar;
  Widget body;
  bool useBasedAppBar;
  BasedScaffold({this.appBar = null, this.body, this.useBasedAppBar = false});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      bottomNavigationBar: BasedNavBar(),
      body: SafeArea(
        //color: Colors.yellow[100],  // background color
        child: this.body,
      ),
      appBar: this.useBasedAppBar ? BasedAppBar() : this.appBar,
    );
  }
}

class BasedAppBar extends PreferredSize {
  final Widget child;
  final double height;

  BasedAppBar({@required this.child, this.height = kToolbarHeight});

  @override
  Size get preferredSize => Size.fromHeight(height);

  @override
  AppBar build(BuildContext context) {
    return AppBar(
      leading: IconButton(
        tooltip: 'Back',
        icon: Icon(Icons.close),
        onPressed: () {
          Navigator.of(context).pop();
        },
      ),
    );
  }
}
