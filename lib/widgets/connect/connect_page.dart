import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:exe/dad/dad.dart';
import 'package:exe/dad/convos_cache.dart';
import 'package:exe/king/king.dart';
import 'package:exe/widgets/based/images.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/connect/convos_tab.dart';

class ConnectPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var userUuid = King.of(context).user.userUuid;

    return BasedScaffold(
      body: Observer(
        // NOTE: observer is for TabBar()
        builder: (_) => DefaultTabController(
          length: 3,
          child: CustomScrollView(
            shrinkWrap: true,
            slivers: <Widget>[
              SliverAppBar(
                floating: true,
                pinned: false,
                snap: true,
                bottom: TabBar(isScrollable: true, tabs: <Widget>[
                  Tab(text: 'Inbox'),
                  Tab(text: 'Notifications'),
                  //TODO: tenant search if pov is agent
                  Tab(text: 'Agent Search'),
                ]),
              ),
              SliverToBoxAdapter(child: SizedBox(height: 10)),
              SliverFillRemaining(
                child: TabBarView(children: <Widget>[
                  ConvosTab(userUuid: userUuid),
                  ConvosTab(userUuid: userUuid),
                  ConvosTab(userUuid: userUuid),
                ]),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
