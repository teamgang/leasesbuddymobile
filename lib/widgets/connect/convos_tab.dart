import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';

import 'package:exe/dad/dad.dart';
import 'package:exe/dad/convos_cache.dart';
import 'package:exe/king/king.dart';
import 'package:exe/widgets/based/images.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/connect/messages_page.dart';
import 'package:exe/widgets/listing/summary_row.dart';

class ConvosTab extends StatelessWidget {
  String userUuid;
  ConvosTab({this.userUuid = 'userUuid-01'});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
      child: Container(
        color: Colors.white,
        child: ConvosWidget(userUuid: this.userUuid),
      ),
    );
  }
}

class ConvosWidget extends StatelessWidget {
  String userUuid;
  ConvosWidget({this.userUuid});

  @override
  Widget build(BuildContext context) {
    ObservableList<Convo> convos;
    if (King.of(context).user.povIsTenant) {
      convos = Dad.of(context).convosCacheTenant.getItem(this.userUuid).convos;
    } else {
      convos = Dad.of(context).convosCacheAgent.getItem(this.userUuid).convos;
    }

    // TODO: should we have a search function here?
    return Observer(builder: (_) {
      return ListView.separated(
          itemCount: convos?.length ?? 0,
          separatorBuilder: (BuildContext context, int index) =>
              const SizedBox(height: 4),
          itemBuilder: (context, index) {
            Convo convo = convos[index];
            return Card(elevation: 5, child: ConvoSummary(convo));
          });
    });
  }
}

class ConvoSummary extends StatefulWidget {
  Convo convo;
  ConvoSummary(this.convo);

  @override
  ConvoSummaryState createState() => ConvoSummaryState();
}

class ConvoSummaryState extends State<ConvoSummary> {
  bool isExpanded = false;

  @override
  Widget build(BuildContext context) {
    var listing =
        King.of(context).dad.listingCache.getItem(widget.convo.listingUuid);
    return Material(
      color: Colors.white,
      child: InkWell(
        onTap: () {
          navPush(context, MessagesPage(convoUuid: widget.convo.convoUuid));
        },
        child: Column(children: <Widget>[
          ListingSummaryRow(listing, pushToPage: 'none'),
          SizedBox(height: 2),
          ConvoSummaryRow(widget.convo),
        ]),
      ),
    );
  }
}

class ConvoSummaryRow extends StatelessWidget {
  Convo convo;
  ConvoSummaryRow(this.convo);

  @override
  Widget build(BuildContext context) {
    double imgHeight = 90, imgWidth = 90;
    return Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
      BasedSizedImage(
        src: convo.agentUuid,
        altText: 'agpic',
        height: imgHeight,
        width: imgWidth,
      ),
      SizedBox(width: 8),
      Expanded(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      convo.title,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(fontSize: 16),
                    ),
                    Container(
                      decoration: BoxDecoration(
                          color: Colors.orange,
                          borderRadius: BorderRadius.all(Radius.circular(20))),
                      child: Padding(
                        padding:
                            EdgeInsets.symmetric(horizontal: 6, vertical: 2),
                        child: Text(
                          '${convo.unreadCount} new',
                          style: TextStyle(fontSize: 12),
                        ),
                      ),
                    ),
                  ]),
              SizedBox(height: 2),
              Text(
                '${convo.lastMessageUsername} said:',
                overflow: TextOverflow.ellipsis,
                style: TextStyle(fontSize: 14),
              ),
              SizedBox(height: 2),
              Text(
                '${convo.lastMessage}',
                overflow: TextOverflow.ellipsis,
                maxLines: 3,
                style: TextStyle(fontSize: 14),
              ),
            ]),
      ),
    ]);
  }
}
