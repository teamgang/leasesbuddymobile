import 'package:bubble/bubble.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:exe/dad/dad.dart';
import 'package:exe/king/king.dart';
import 'package:exe/widgets/based/images.dart';

class MessagesPage extends StatefulWidget {
  String convoUuid;
  MessagesPage({this.convoUuid});

  @override
  MessagesPageState createState() => MessagesPageState();
}

class MessagesPageState extends State<MessagesPage> {
  //ScrollController scrollController;

  //@override
  //void initState() {
  //scrollController = ScrollController();
  ////textController = widget.textController ?? TextEditingController();
  ////inputFocusNode = widget.focusNode ?? FocusNode();
  ////WidgetsBinding.instance.addPostFrameCallback(widgetBuilt);
  //super.initState();
  //}

  @override
  Widget build(BuildContext context) {
    var messages =
        King.of(context).dad.messagesCache.getItem(widget.convoUuid).messages;

    //print('got here');
    //print('got here');
    //ObservableList<Message> messageList =
    //(messageListOrdered.length > 0) ? messageListOrdered.reversed : [];
    //print('got here');
    //print('got here');

    //double initPos = scrollController.position.maxScrollExtent + 25.0;

    return Observer(
      builder: (_) => Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.green,
          title: Row(
            children: <Widget>[
              Text('IM'),
              SizedBox(width: 16),
              Text('title goes'),
            ],
          ),
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.more_vert, color: Colors.white),
              onPressed: () {},
            ),
          ],
        ),
        body: SizedBox.expand(
          child: Container(
            color: Colors.grey[900],
            child: Padding(
              padding: EdgeInsets.fromLTRB(8, 0, 8, 0),
              child: ListView.separated(
                  // TODO: do we need to render the messages in reverse? we'll need to make sure logic and message spacing lines up
                  //reverse: true,
                  itemCount: messages.length,
                  reverse: true,
                  separatorBuilder: (BuildContext context, int index) =>
                      SizedBox(height: 2),
                  itemBuilder: (context, index) {
                    int revIndex = messages.length - 1 - index;
                    var message = messages[revIndex];

                    //var isNextMessageSameUser = false;
                    //if ((index + 1) < messages.length) {
                    //var nextMessage = messages[index + 1];
                    //isNextMessageSameUser =
                    //message.userUuid == nextMessage.userUuid;
                    //}

                    //var isPrevMessageSameUser = false;
                    //if ((index - 1) > 0) {
                    //var prevMessage = messages[index - 1];
                    //isPrevMessageSameUser =
                    //message.userUuid == prevMessage.userUuid;
                    //}

                    var isNextMessageSameUser = false;
                    if ((revIndex - 1) > 0) {
                      var nextMessage = messages[revIndex - 1];
                      isNextMessageSameUser =
                          message.userUuid == nextMessage.userUuid;
                    }

                    var isPrevMessageSameUser = false;
                    if ((revIndex + 1) < messages.length) {
                      var prevMessage = messages[revIndex + 1];
                      isPrevMessageSameUser =
                          message.userUuid == prevMessage.userUuid;
                    }

                    return MessageBubble(message,
                        isNextMessageSameUser: isNextMessageSameUser,
                        isPrevMessageSameUser: isPrevMessageSameUser);
                  }),
            ),
          ),
        ),
      ),
    );
  }
}

class MessageBubble extends StatelessWidget {
  Message message;
  bool isMe;
  bool isNextMessageSameUser;
  bool isPrevMessageSameUser;

  MessageBubble(Message message,
      {this.isNextMessageSameUser = false,
      this.isPrevMessageSameUser = false}) {
    this.message = message;
    if (this.message.userUuid == '1111') {
      this.isMe = true;
    } else {
      this.isMe = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    // TODO: smaller margin for messages from same user
    return Row(
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisAlignment:
          this.isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
      children: <Widget>[
        (!this.isMe && !this.isPrevMessageSameUser)
            ? BasedCircleAvatar(
                src: '',
                radius: 18,
                altText: message.username.substring(0, 2),
              )
            : SizedBox(height: 0, width: 36),
        Flexible(
          child: Bubble(
            margin: this.isNextMessageSameUser
                ? BubbleEdges.only(top: 0, bottom: 0)
                : BubbleEdges.only(top: 4, bottom: 0),
            nip: this.isMe ? BubbleNip.rightBottom : BubbleNip.leftBottom,
            color: this.isMe ? Colors.green[600] : Colors.grey[850],
            child: MessageBody(
                this.message, this.isMe, this.isPrevMessageSameUser),
            //child:
            //Text(this.message.text, style: TextStyle(color: Colors.white)),
          ),
        ),
      ],
    );
  }
}

// TODO: do we want to make this fancy message body thing?
class MessageBody extends StatelessWidget {
  Message message;
  bool isMe;
  bool isPrevMessageSameUser = false;
  MessageBody(this.message, this.isMe, this.isPrevMessageSameUser);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment:
          this.isMe ? CrossAxisAlignment.end : CrossAxisAlignment.start,
      children: <Widget>[
        (!this.isPrevMessageSameUser && !this.isMe)
            ? Text(this.message.username, style: TextStyle(color: Colors.blue))
            : SizedBox(height: 0),
        Row(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            SizedBox(width: 2),
            Flexible(
                child: Text(this.message.text,
                    style: TextStyle(color: Colors.white))),
            SizedBox(width: 2),
          ],
        ),
        SizedBox(height: 4),
        // TODO: do we want/need a timestamp?
        //IntrinsicWidth(
        //child: Row(
        //children: <Widget>[
        //Text(this.message.timeReadable,
        //style: TextStyle(color: Colors.grey[300], fontSize: 10)),
        //],
        //),
        //),
      ],
    );
  }
}
