import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';

import 'package:exe/dad/dad.dart';
import 'package:exe/dad/listing_cache.dart';
import 'package:exe/dad/transient/listing.dart';
import 'package:exe/king/king.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/erectors/listing/listing_erector_widgets.dart';
import 'package:exe/widgets/erectors/listing/set_title_page.dart';
import 'package:exe/widgets/listing/summary_row.dart';

class CreateListingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var user = King.of(context).user;

    return BasedScaffold(
      body: CustomScrollView(
        shrinkWrap: true,
        slivers: <Widget>[
          SliverAppBar(
            floating: true,
            pinned: false,
            snap: true,
            leading: InkWell(
                child: Padding(
                  padding: EdgeInsets.symmetric(horizontal: 14),
                  child: Icon(Icons.close),
                ),
                onTap: () {
                  Navigator.popUntil(context, ModalRoute.withName('manage'));
                }),
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              InkWell(
                  child: Row(children: <Widget>[
                    Text('Make new'),
                    Icon(Icons.arrow_right),
                  ]),
                  onTap: () {
                    var listing =
                        King.of(context).dad.listingCache.makeNewDraftListing();
                    Navigator.pushNamed(
                      context,
                      ListingErectorWidgets.firstStepRoute,
                      arguments: Args(listing: listing),
                    );
                  }),
              Text('Copy an existing listing'),
            ]),
          ),
        ],
      ),
    );
  }
}
