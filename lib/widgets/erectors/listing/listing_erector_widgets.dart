import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:meta/meta.dart';

import 'package:exe/dad/listing_cache.dart';
import 'package:exe/king/king.dart';

class ListingErectorWidgets {
  //NOTE: order matters here, as it sets the order of items presented to the user
  static final List<String> steps = [
    'setTitle',
    'setDescription',
    'setPriceAndSize',
    'setLocation',
    'setRoomsAndSpaces',
    'setPhotos',
    'setAmenities',
  ];

  static String get firstStepRoute {
    return 'manage/${steps[0]}';
  }

  static String makeRouteName(String stepName) {
    return 'manage/$stepName';
  }

  static String getNextStep(String currentStep) {
    int nextIndex = steps.indexOf(currentStep) + 1;
    if (nextIndex < steps.length) {
      return steps[nextIndex];
    } else {
      return '';
    }
  }

  static String getPreviousStep(String currentStep) {
    int prevIndex = steps.indexOf(currentStep) - 1;
    if (prevIndex > -1) {
      return steps[prevIndex];
    } else {
      return '';
    }
  }

  static upperSliverAppBar(
    BuildContext context, {
    bool canSave = false,
    @required String currentStep,
    @required Listing listing,
    @required Function saveFields,
  }) {
    var king = King.of(context);
    Listing memListing = king.dad.listingCache.getItem(listing.listingUuid);

    return SliverAppBar(
      floating: true,
      pinned: false,
      snap: true,
      leading: InkWell(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 14),
            child: Icon(Icons.close),
          ),
          onTap: () {
            if (memListing.isSavedInApi) {
              King.of(context)
                  .dad
                  .listingCache
                  .getItemFresh(listing.listingUuid);
              Navigator.of(context).pushNamedAndRemoveUntil(
                makeRouteName('overview'),
                ModalRoute.withName(makeRouteName('overview')),
                arguments: Args(listing: listing),
              );
            } else {
              Navigator.of(context).pushNamedAndRemoveUntil(
                  'manage', ModalRoute.withName('manage'));
            }
          }),
      actions: <Widget>[
        //

        Observer(
          builder: (_) => memListing.isSavedInApi
              ? navButton(context,
                  canSave: canSave,
                  children: <Widget>[
                    Icon(Icons.arrow_left),
                    Text('Save for later'),
                  ],
                  onPressed: canSave
                      ? () async {
                          bool saved = await saveFields();
                          king.dad.listingCache
                              .getItemFresh(listing.listingUuid);
                          if (saved) {
                            //Navigator.of(context).pop();
                            Navigator.of(context).pushNamed(
                              makeRouteName('overview'),
                              arguments: Args(listing: listing),
                            );

                            //Navigator.pushNamedAndRemoveUntil(
                            //context,
                            //makeRouteName('overview'),
                            //ModalRoute.withName(makeRouteName('overview')),
                            //arguments: Args(listing: listing),
                            //);
                          }
                        }
                      : null)
              : SizedBox.shrink(),
        ),
        SizedBox(width: 8),
      ],
    );
  }

  static lowerSliverAppBar(
    BuildContext context, {
    bool canSave = false,
    @required String currentStep,
    @required Listing listing,
    @required Function saveFields,
  }) {
    String nextStep = getNextStep(currentStep);
    String previousStep = getPreviousStep(currentStep);
    return SliverAppBar(
      floating: true,
      pinned: false,
      snap: true,
      automaticallyImplyLeading: false,
      actions: <Widget>[
        previousStep != ''
            ? navButton(context,
                canSave: canSave,
                children: <Widget>[
                  Icon(Icons.arrow_left),
                  Text('Previous'),
                ],
                onPressed: canSave
                    ? () async {
                        bool saved = await saveFields();
                        if (saved) {
                          Navigator.of(context).pushReplacementNamed(
                            makeRouteName(previousStep),
                            arguments: Args(listing: listing),
                          );
                        }
                      }
                    : null)
            : SizedBox.shrink(),
        //

        nextStep != ''
            ? navButton(context,
                canSave: canSave,
                children: <Widget>[
                  Text('Next'),
                  Icon(Icons.arrow_right),
                ],
                onPressed: canSave
                    ? () async {
                        bool saved = await saveFields();
                        if (saved) {
                          Navigator.of(context).pushReplacementNamed(
                            makeRouteName(nextStep),
                            arguments: Args(listing: listing),
                          );
                        }
                      }
                    : null)
            : navButton(context,
                canSave: canSave,
                children: <Widget>[
                  Text('Finish'),
                ],
                onPressed: canSave
                    ? () async {
                        bool saved = await saveFields();
                        if (saved) {
                          //Navigator.pushNamedAndRemoveUntil(
                          //context,
                          //'manage/overview',
                          //ModalRoute.withName('manage/overview'),
                          //arguments: Args(listing: listing),
                          //);
                          Navigator.of(context).pop();
                          //Navigator.of(context).pushNamed(
                          //context,
                          //makeRouteName('overview'),
                          //arguments: Args(listing: listing),
                          //);
                        }
                      }
                    : null),
      ],
    );
  }

  static Widget navButton(
    BuildContext context, {
    bool canSave,
    @required List<Widget> children,
    @required Function onPressed,
  }) {
    return FlatButton(
      color: Colors.blue,
      textColor: Colors.white,
      //disabledColor: Colors.grey,
      disabledTextColor: Colors.grey,
      padding: EdgeInsets.all(8.0),
      splashColor: Colors.blueAccent,
      onPressed: onPressed,
      child: Row(children: <Widget>[
        ...children,
      ]),
    );
  }
}
