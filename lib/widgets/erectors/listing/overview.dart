import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:exe/dad/listing_cache.dart';
import 'package:exe/king/king.dart';
import 'package:exe/lip/lip.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/based/widgets.dart';
import 'package:exe/widgets/erectors/listing/listing_erector_widgets.dart';

class ListingDraftOverviewPage extends StatefulWidget {
  Listing listing;
  ListingDraftOverviewPage({this.listing});

  @override
  ListingDraftOverviewPageState createState() =>
      ListingDraftOverviewPageState();
}

class ListingDraftOverviewPageState extends State<ListingDraftOverviewPage> {
  String failure = '';
  bool requestInProgress = false;

  @override
  Widget build(BuildContext context) {
    return BasedScaffold(
      body: CustomScrollView(slivers: <Widget>[
        SliverAppBar(
          floating: true,
          pinned: false,
          snap: true,
          leading: InkWell(
              child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 14),
                child: Icon(Icons.close),
              ),
              onTap: () {
                //Navigator.of(context).pop();
                Navigator.pushReplacementNamed(
                  context,
                  'manage',
                );
              }),
        ),
        //

        SliverPadding(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          sliver: SliverList(
            delegate: SliverChildListDelegate([
              SizedBox(height: 8),
              OverviewRow(widget.listing, title: 'Preview'),
              BasedDivider(),
              OverviewRow(widget.listing, title: 'Title'),
              BasedDivider(),
              OverviewRow(widget.listing, title: 'Description'),
              BasedDivider(),
              OverviewRow(widget.listing, title: 'Price, Size, and Dates'),
              BasedDivider(),
              OverviewRow(widget.listing, title: 'Location'),
              BasedDivider(),
              OverviewRow(widget.listing, title: 'Rooms and Spaces'),
              BasedDivider(),
              OverviewRow(widget.listing, title: 'Photos'),
              BasedDivider(),
              OverviewRow(widget.listing, title: 'Amenities'),

              //TODO: grey out if validation fails
              RaisedButton(
                onPressed: () => _publishListing(context, widget.listing),
                child: Text('Publish Listing'),
              ),
            ]),
          ),
        ),
      ]),
    );
  }

  //TODO: implement
  bool get canSave {
    if (true) {
      return true;
    }
    return false;
  }

  Future<bool> _publishListing(
    BuildContext context,
    Listing listing,
  ) async {
    //TODO: block submissions if all steps are not complete
    String failureText = '';
    var king = King.of(context);
    if (!this.requestInProgress) {
      this.setState(() {
        this.requestInProgress = true;
        this.failure = '';
      });

      failureText = await this._submitForm(context, listing);

      this.setState(() {
        this.requestInProgress = false;
      });

      if (failureText != '') {}

      this.setState(() {
        this.failure = failureText;
      });
    }

    return failureText == '' ? true : false;
  }

  Future<String> _submitForm(
    BuildContext context,
    Listing listing,
  ) async {
    //====================================================================
    //TODO: remove this code after we get mapsac maps autocomplete working
    //====================================================================
    try {
      var king = King.of(context);
      String endpoint = 'agents/listings/${widget.listing.listingUuid}';
      var payload = {
        'state': 'NC',
        'city': 'Raleighmaybe',
        'zip_code': 27606,
      };

      ApiResponse ares = await king.lip.api(
        endpoint,
        payload: payload,
        method: HttpMethod.patch,
      );

      String failureText = '';
      if (!ares.isOk()) {
        failureText = 'Form failed to save';
      }
    } catch (e, s) {
      print('caught something');
    }
    //====================================================================
    //TODO: remove this code after we get mapsac maps autocomplete working
    //====================================================================

    String endpoint = 'agent/listings';
    var king = King.of(context);

    endpoint = '$endpoint/${listing.listingUuid}/publish';

    ApiResponse ares = await king.lip.api(
      endpoint,
      method: HttpMethod.post,
    );

    if (ares.isOk()) {
      // update the listing in memory; this will update completedSteps
      listing = await king.dad.listingCache.getItemFresh(ares.body['id']);
    } else {
      failure = 'Form failed to save new listing';
    }
    return failure;
  }
}

class OverviewRow extends StatelessWidget {
  String title;
  Listing listing;
  OverviewRow(this.listing, {this.title});

  void doPush(BuildContext context) {
    switch (this.title) {
      case 'Amenities':
        Navigator.of(context).pushReplacementNamed(
          ListingErectorWidgets.makeRouteName('setAmenities'),
          arguments: Args(listing: listing),
        );
        break;

      case 'Description':
        Navigator.of(context).pushReplacementNamed(
          ListingErectorWidgets.makeRouteName('setDescription'),
          arguments: Args(listing: listing),
        );
        break;

      case 'Location':
        Navigator.of(context).pushReplacementNamed(
          ListingErectorWidgets.makeRouteName('setLocation'),
          arguments: Args(listing: listing),
        );
        break;

      case 'Photos':
        Navigator.of(context).pushReplacementNamed(
          ListingErectorWidgets.makeRouteName('setPhotos'),
          arguments: Args(listing: listing),
        );
        break;

      case 'Preview':
        Navigator.of(context).pushNamed(
          ListingErectorWidgets.makeRouteName('preview'),
          arguments: Args(listing: listing),
        );
        break;

      case 'Price, Size, and Dates':
        Navigator.of(context).pushReplacementNamed(
          ListingErectorWidgets.makeRouteName('setPriceAndSize'),
          arguments: Args(listing: listing),
        );
        break;

      case 'Rooms and Spaces':
        Navigator.of(context).pushReplacementNamed(
          ListingErectorWidgets.makeRouteName('setRoomsAndSpaces'),
          arguments: Args(listing: listing),
        );
        break;

      case 'Title':
        Navigator.of(context).pushReplacementNamed(
          ListingErectorWidgets.makeRouteName('setTitle'),
          arguments: Args(listing: listing),
        );
        break;

      default:
        throw ('ERROR: no doPush case for title: $title');
    }
  }

  Widget _sectionStatus() {
    //TODO: report if missing from completedSteps
    switch (this.title) {
      case 'Title':
        if (listing.validTitle) {
          return Observer(
            builder: (_) => Text(listing.title),
          );
        } else {
          return SectionNeedsWork();
        }
        break;

      case 'Description':
        if (listing.validDescription) {
          return SizedBox.shrink();
        } else {
          return SectionNeedsWork();
        }
        break;

      case 'Rooms and Spaces':
        if (listing.validRoomsAndSpaces) {
          return SizedBox.shrink();
        } else {
          return SectionNeedsWork();
        }
        break;

      default:
        print('ERROR: no sectionStatus case for title $title');
        return SizedBox.shrink();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: InkWell(
        onTap: () {
          this.doPush(context);
        },
        child: Padding(
          padding: EdgeInsets.fromLTRB(16, 16, 16, 16),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                Text(this.title, style: TextStyle(fontSize: 18)),
                SizedBox(height: 4),
                this._sectionStatus(),
              ]),
        ),
      ),
    );
  }
}

class SectionNeedsWork extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
      Icon(
        Icons.notification_important,
        color: Colors.red,
      ),
      Text(
        'This section needs work',
        style: TextStyle(
          fontSize: 14,
          fontStyle: FontStyle.italic,
        ),
      ),
    ]);
  }
}
