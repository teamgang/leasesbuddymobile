import 'package:flutter/material.dart';

import 'package:exe/dad/listing_cache.dart';
import 'package:exe/king/king.dart';
import 'package:exe/lip/lip.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/erectors/listing/listing_erector_widgets.dart';

class SetDescriptionPage extends StatefulWidget {
  static const String currentStep = 'setDescription';
  Listing listing;
  SetDescriptionPage({this.listing});

  @override
  SetDescriptionPageState createState() => SetDescriptionPageState(currentStep);
}

class SetDescriptionPageState extends State<SetDescriptionPage> {
  String currentStep;
  String _description;
  String _failure = '';
  FocusNode focusNode;
  final _formKey = GlobalKey<FormState>();
  int maxLength = 800;
  int minLength = 40;
  bool _requestInProgress = false;

  SetDescriptionPageState(this.currentStep);

  @override
  initState() {
    super.initState();
    focusNode = FocusNode();
    focusNode.requestFocus();
    _description = widget.listing.description;
  }

  @override
  dispose() {
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BasedScaffold(
      body: Form(
        key: _formKey,
        child: CustomScrollView(slivers: <Widget>[
          ListingErectorWidgets.upperSliverAppBar(
            context,
            canSave: this.canSave,
            currentStep: this.currentStep,
            listing: widget.listing,
            saveFields: () async {
              return await this.saveFields(context);
            },
          ),
          ListingErectorWidgets.lowerSliverAppBar(
            context,
            currentStep: this.currentStep,
            canSave: this.canSave,
            listing: widget.listing,
            saveFields: () async {
              return await this.saveFields(context);
            },
          ),
          SliverPadding(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            sliver: SliverList(
              delegate: SliverChildListDelegate([
                SizedBox(height: 8),
                Text(
                  'Description',
                  style: Theme.of(context).textTheme.headline4,
                ),
                SizedBox(height: 8),
                Text(
                    'Give a detailed description. Tell users about the neighborhood, things to do, and your opinion of the property.',
                    style: Theme.of(context).textTheme.subtitle1),
                SizedBox(height: 20),
                //

                TextFormField(
                  autovalidate: true,
                  enabled: !_requestInProgress,
                  focusNode: focusNode,
                  initialValue: _description,
                  keyboardType: TextInputType.multiline,
                  maxLength: this.maxLength,
                  maxLines: 12,
                  minLines: 6,
                  style: Theme.of(context).textTheme.headline6,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Description',
                    counterText: '${_description.length}/$maxLength',
                  ),
                  onChanged: (newValue) {
                    setState(() {
                      _description = newValue;
                    });
                  },
                  validator: (value) {
                    if (value.length < ListingConstants.minDescription) {
                      return 'Description must be at least ${ListingConstants.minDescription} characters';
                    }
                    if (value.length > ListingConstants.maxDescription) {
                      return 'Description must be under ${ListingConstants.maxDescription} characters';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 8),
                Text(_failure),
              ]),
            ),
          ),
        ]),
      ),
    );
  }

  bool get canSave {
    if (this._formKey.currentState != null &&
        this._formKey.currentState.validate()) {
      return true;
    }
    return false;
  }

  Future<bool> saveFields(BuildContext context) async {
    String failureText = '';
    var king = King.of(context);
    if (!_requestInProgress) {
      this.setState(() {
        _requestInProgress = true;
      });

      failureText = await this._submitForm(context);

      this.setState(() {
        _requestInProgress = false;
        _failure = failureText;
      });
    }

    king.dad.listingCache.getItemFresh(widget.listing.listingUuid);
    return failureText == '' ? true : false;
  }

  Future<String> _submitForm(BuildContext context) async {
    var king = King.of(context);
    String endpoint = 'agents/listings/${widget.listing.listingUuid}';
    var payload = {
      //TODO: remove interior_
      'interior_description': _description,
      'completed_steps':
          widget.listing.getUpdatedCompletedStepsAsList(this.currentStep),
    };

    ApiResponse ares = await king.lip.api(
      endpoint,
      payload: payload,
      method: HttpMethod.patch,
    );

    String failureText = '';
    if (!ares.isOk()) {
      failureText = 'Failed to save description.';
    }
    return failureText;
  }
}
