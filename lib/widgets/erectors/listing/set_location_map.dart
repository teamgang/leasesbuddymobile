import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:meta/meta.dart';

import 'package:exe/dad/dad.dart';
import 'package:exe/king/king.dart';
import 'package:exe/lip/lip.dart';
import 'package:exe/shared/map.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/erectors/listing/listing_erector_widgets.dart';

// placeId is set to the empty string to imply no place found yet
class SetLocationMap extends StatefulWidget {
  // This object is to pass values by reference to this widget
  LatLongBuddy pinLoc;
  String placeId;
  SetLocationMap({@required this.pinLoc, @required this.placeId});

  @override
  SetLocationMapState createState() => SetLocationMapState();
}

class SetLocationMapState extends State<SetLocationMap> {
  GoogleMapController _mapController;
  // _manualLatLng is where the user has navigated to
  LatLng _manualLatLng;
  // _userLatLng is used to reset the pin to the user's location
  LatLng _userLatLng;
  // _placeLatLng is used to reset the pin to the google places location
  LatLng _placeLatLng;
  GooglePlace _place;
  // _usePlacesLocation let's us know if we are currently using the
  // _placeLatLng or a manual LatLng input
  bool _usePlacesLocation = false;

  @override
  initState() {
    super.initState();
    var user = King.of(context).user;
    _userLatLng = LatLng(user.gps.lat, user.gps.long);
  }

  @override
  didChangeDependencies() {
    if (_placesLocationAvailable) {
      setState(() {
        _usePlacesLocation = true;
        _moveCamera();
      });
    }
    super.didChangeDependencies();
  }

  CameraPosition _currentCamera() {
    if (_usePlacesLocation) {
      return _place.camera;
    } else {
      return CameraPosition(
        target: _userLatLng,
        zoom: 14.4746,
      );
    }
  }

  bool get _placesLocationAvailable {
    if (widget.placeId.isEmpty) {
      return false;
    }
    return true;
  }

  Future<void> _moveCamera() async {
    _mapController
        .animateCamera(CameraUpdate.newCameraPosition(_currentCamera()));
  }

  @override
  Widget build(BuildContext context) {
    var user = King.of(context).user;
    //TODO: test that the camera moves to _place when _place changes. Might
    // need to use an Observer here
    _place = King.of(context).dad.googlePlaceCache.getItem(widget.placeId);

    return Padding(
      padding: EdgeInsets.fromLTRB(0, 16, 30, 16),
      child: ConstrainedBox(
        constraints: BoxConstraints(maxHeight: 400),
        child: Stack(
          children: <Widget>[
            GoogleMap(
              mapType: MapType.hybrid,
              initialCameraPosition: user.gps.camera,
              onMapCreated: (GoogleMapController controller) {
                _mapController = controller;
              },
            ),
            _placesLocationAvailable
                ? Positioned(
                    child: DecoratedBox(
                      decoration: BoxDecoration(
                        color: _usePlacesLocation
                            ? Color.fromRGBO(255, 255, 255, 1)
                            : Color.fromRGBO(255, 255, 255, 0.75),
                        borderRadius: BorderRadius.circular(2),
                      ),
                      child: Row(
                        children: <Widget>[
                          SizedBox(width: 16),
                          Text('Use predicted location'),
                          Checkbox(
                              value: _usePlacesLocation,
                              onChanged: (bool value) {
                                setState(() {
                                  _usePlacesLocation = value;
                                  _moveCamera();
                                });
                              }),
                        ],
                      ),
                    ),
                    top: 10,
                    left: 10,
                  )
                : SizedBox.shrink(),
          ],
        ),
      ),
    );
  }
}
