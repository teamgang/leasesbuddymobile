import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';

import 'package:exe/dad/dad.dart';
import 'package:exe/king/king.dart';
import 'package:exe/lip/lip.dart';
import 'package:exe/shared/map.dart';
import 'package:exe/shared/mock.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/erectors/listing/listing_erector_widgets.dart';
import 'package:exe/widgets/erectors/listing/set_location_map.dart';

class SetLocationPage extends StatefulWidget {
  static const String currentStep = 'setLocation';
  Listing listing;
  SetLocationPage({this.listing});

  @override
  SetLocationPageState createState() => SetLocationPageState(currentStep);
}

class SetLocationPageState extends State<SetLocationPage> {
  String _address;
  String currentStep;
  String _failure = '';
  FocusNode _focusNode;
  final _formKey = GlobalKey<FormState>();
  LatLongBuddy _pinLoc = LatLongBuddy();
  String _placeId = '';
  MapsacPredictionListCacher _predictionsCacher;
  bool _requestInProgress = false;

  SetLocationPageState(this.currentStep);

  @override
  initState() {
    super.initState();
    _focusNode = FocusNode();
    _focusNode.requestFocus();
    _address = widget.listing.address;
    var user = King.of(context).user;
    _pinLoc = LatLongBuddy(lat: user.gps.lat, long: user.gps.long);
  }

  @override
  dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var king = King.of(context);
    _predictionsCacher = new MapsacPredictionListCacher(king);
    _predictionsCacher.setFields(
      input: _address,
      lat: king.user.gps.lat,
      long: king.user.gps.long,
    );
    PredictionList predictionList = _predictionsCacher.currentPredictions;

    return BasedScaffold(
      body: Form(
        key: _formKey,
        child: CustomScrollView(slivers: <Widget>[
          ListingErectorWidgets.upperSliverAppBar(
            context,
            canSave: this.canSave,
            currentStep: this.currentStep,
            listing: widget.listing,
            saveFields: () async {
              return await this.saveFields(context);
            },
          ),
          ListingErectorWidgets.lowerSliverAppBar(
            context,
            currentStep: this.currentStep,
            canSave: this.canSave,
            listing: widget.listing,
            saveFields: () async {
              return await this.saveFields(context);
            },
          ),
          SliverPadding(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            sliver: SliverList(
              delegate: SliverChildListDelegate([
                SizedBox(height: 8),
                Text(
                  'Address',
                  style: Theme.of(context).textTheme.headline4,
                ),
                SizedBox(height: 8),
                //

                TextFormField(
                  autocorrect: false,
                  autovalidate: true,
                  enabled: !_requestInProgress,
                  focusNode: _focusNode,
                  initialValue: widget.listing.address,
                  maxLength: ListingConstants.maxAddressLength,
                  style: Theme.of(context).textTheme.headline6,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Addresses',
                    counterText:
                        '${_address.length}/${ListingConstants.maxAddressLength}',
                  ),
                  onChanged: (newValue) {
                    setState(() {
                      _address = newValue;
                    });
                  },
                  validator: (value) {
                    if (value.length < ListingConstants.minAddressLength) {
                      return 'Address must be at least ${ListingConstants.minAddressLength} characters';
                    }
                    if (value.length > ListingConstants.maxAddressLength) {
                      return 'Address must be under ${ListingConstants.maxAddressLength} characters';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 8),
                _buildPredictionList(context, predictionList.predictions),
                SizedBox(height: 8),
                Text(_failure),
                SizedBox(height: 8),
                SetLocationMap(pinLoc: _pinLoc, placeId: _placeId),
              ]),
            ),
          ),
        ]),
      ),
    );
  }

  Widget _buildPredictionList(
      BuildContext context, ObservableList<Prediction> predictions) {
    return Observer(builder: (BuildContext context) {
      return predictions.isNotEmpty
          ? Expanded(
              child: ListView.builder(
                itemCount: predictions.length,
                itemBuilder: (context, index) {
                  var prediction = predictions[index];
                  return Card(
                    child: InkWell(
                      child: ListTile(
                        leading: FlutterLogo(),
                        title: Text(prediction.address),
                      ),
                      onTap: () {
                        setState(() {
                          _address = prediction.address;
                          _placeId = prediction.placeId;
                        });
                        //Navigator.push(
                        //context,
                        //MaterialPageRoute(
                        //builder: (BuildContext context) =>
                        //SetVenueLocation(
                        //placeId: prediction.placeId)));
                      },
                    ),
                  );
                },
              ),
            )
          : Text('No matching addresses found.');
    });
  }

  bool get canSave {
    if (this._formKey.currentState != null &&
        this._formKey.currentState.validate()) {
      return true;
    }
    return false;
  }

  // saveFields returns true if fields saved successfully
  Future<bool> saveFields(BuildContext context) async {
    String failureText = '';
    var king = King.of(context);
    if (!_requestInProgress) {
      this.setState(() {
        _requestInProgress = true;
      });

      failureText = await this._submitForm(context);

      this.setState(() {
        _requestInProgress = false;
        _failure = failureText;
      });
    }

    // update the listing in memory; this will update completedSteps
    king.dad.listingCache.getItemFresh(widget.listing.listingUuid);
    return failureText == '' ? true : false;
  }

  Future<String> _submitForm(BuildContext context) async {
    var king = King.of(context);
    String endpoint = 'agents/listings/${widget.listing.listingUuid}';
    var payload = {
      'address': _address,
      //TODO: use lat/long from the address
      'lat': randLat(),
      'lng': randLong(),
      'completed_steps':
          widget.listing.getUpdatedCompletedStepsAsList(this.currentStep),
    };

    ApiResponse ares = await king.lip.api(
      endpoint,
      payload: payload,
      method: HttpMethod.patch,
    );

    String failureText = '';
    if (!ares.isOk()) {
      failureText = 'Failed to save address.';
    }
    return failureText;
  }
}
