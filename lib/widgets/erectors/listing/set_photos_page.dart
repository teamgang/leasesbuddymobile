import 'package:flutter/material.dart';

import 'package:exe/dad/listing_cache.dart';
import 'package:exe/king/king.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/erectors/listing/listing_erector_widgets.dart';

class SetPhotosPage extends StatefulWidget {
  static const String currentStep = 'setPhotos';
  Listing listing;
  SetPhotosPage({this.listing});

  @override
  SetPhotosPageState createState() => SetPhotosPageState(currentStep);
}

class SetPhotosPageState extends State<SetPhotosPage> {
  String currentStep;
  String endpoint = 'listings/change/title';
  String failure = '';
  final _formKey = GlobalKey<FormState>();
  Listing changesListing = Listing();
  bool requestInProgress = false;

  SetPhotosPageState(this.currentStep);

  @override
  Widget build(BuildContext context) {
    final Args args = ModalRoute.of(context).settings.arguments;
    return BasedScaffold(
      body: CustomScrollView(slivers: <Widget>[
        ListingErectorWidgets.upperSliverAppBar(
          context,
          canSave: this.canSave,
          currentStep: this.currentStep,
          listing: args.listing,
          saveFields: () async {
            return await this.saveFields(
              context,
              args.listing.listingUuid,
              this.changesListing,
            );
          },
        ),
        ListingErectorWidgets.lowerSliverAppBar(
          context,
          currentStep: this.currentStep,
          canSave: this.canSave,
          listing: args.listing,
          saveFields: () async {
            return await this.saveFields(
              context,
              args.listing.listingUuid,
              this.changesListing,
            );
          },
        ),
        SliverPadding(
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          sliver: SliverList(
            delegate: SliverChildListDelegate([
              SizedBox(height: 8),
              Text('Photos'),
            ]),
          ),
        ),
      ]),
    );
  }

  bool get canSave {
    return true;
    //if (this._formKey.currentState != null &&
    //this._formKey.currentState.validate()) {
    //return true;
    //}
    //return false;
  }

  Future<bool> saveFields(
    BuildContext context,
    String listingUuid,
    Listing changesListing,
  ) async {
    return true;
  }
}
