import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
//import 'package:flutter_datetime_formfield/flutter_datetime_formfield.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:intl/intl.dart'; // DateFormat

import 'package:exe/dad/listing_cache.dart';
import 'package:exe/king/king.dart';
import 'package:exe/lip/lip.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/erectors/listing/listing_erector_widgets.dart';

class SetPriceAndSizePage extends StatefulWidget {
  static const String currentStep = 'setPriceAndSize';
  Listing listing;
  SetPriceAndSizePage({this.listing});

  @override
  SetPriceAndSizePageState createState() =>
      SetPriceAndSizePageState(currentStep);
}

//TODO: handle these digit input fields nicely
class SetPriceAndSizePageState extends State<SetPriceAndSizePage> {
  DateTime _dateAvailable;
  String currentStep;
  int _deposit;
  String _failure = '';
  final _formKey = GlobalKey<FormState>();
  int _monthlyRent;
  int _squareFeet;
  bool _requestInProgress = false;

  SetPriceAndSizePageState(this.currentStep);

  @override
  initState() {
    super.initState();
    _monthlyRent = widget.listing.monthlyRent;
    _deposit = widget.listing.deposit;
    _squareFeet = widget.listing.squareFeet;
    _dateAvailable = widget.listing.dateAvailable;
  }

  @override
  Widget build(BuildContext context) {
    return BasedScaffold(
      body: Form(
        key: _formKey,
        child: CustomScrollView(slivers: <Widget>[
          ListingErectorWidgets.upperSliverAppBar(
            context,
            canSave: this.canSave,
            currentStep: this.currentStep,
            listing: widget.listing,
            saveFields: () async {
              return await this.saveFields(
                context,
              );
            },
          ),
          ListingErectorWidgets.lowerSliverAppBar(
            context,
            currentStep: this.currentStep,
            canSave: this.canSave,
            listing: widget.listing,
            saveFields: () async {
              return await this.saveFields(
                context,
              );
            },
          ),
          SliverPadding(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            sliver: SliverList(
              delegate: SliverChildListDelegate([
                //

                SizedBox(height: 8),
                Text(
                  'Monthly Rent',
                  style: Theme.of(context).textTheme.headline4,
                ),
                SizedBox(height: 14),
                Text('Monthly rent tenant will pay to the owner or agent.',
                    style: Theme.of(context).textTheme.subtitle1),
                SizedBox(height: 20),

                TextFormField(
                  autovalidate: true,
                  enabled: !_requestInProgress,
                  initialValue: _monthlyRent.toString(),
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  keyboardType: TextInputType.number,
                  maxLength: 12,
                  style: Theme.of(context).textTheme.headline6,
                  onChanged: (newValue) {
                    if (newValue == '') {
                      setState(() {
                        _monthlyRent = 0;
                      });
                    } else {
                      setState(() {
                        _monthlyRent = int.parse(newValue);
                      });
                    }
                  },
                  validator: (value) {
                    if (value.length < ListingConstants.minMonthlyRent) {
                      //TODO: internationalize
                      return 'Montly rent must be at least ${ListingConstants.minMonthlyRent}';
                    }
                    if (value.length > ListingConstants.maxMonthlyRent) {
                      return 'Montly rent must be under ${ListingConstants.maxMonthlyRent}';
                    }
                    return null;
                  },
                ),
                //

                SizedBox(height: 8),
                Text(
                  'Deposit',
                  style: Theme.of(context).textTheme.headline4,
                ),
                SizedBox(height: 14),
                Text('Deposit due for the property',
                    style: Theme.of(context).textTheme.subtitle1),
                SizedBox(height: 20),

                TextFormField(
                  autovalidate: true,
                  enabled: !_requestInProgress,
                  keyboardType: TextInputType.number,
                  initialValue: _deposit.toString(),
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  maxLength: 12,
                  style: Theme.of(context).textTheme.headline6,
                  onChanged: (newValue) {
                    if (newValue == '') {
                      setState(() {
                        _deposit = 0;
                      });
                    } else {
                      setState(() {
                        _deposit = int.parse(newValue);
                      });
                    }
                  },
                  validator: (value) {
                    if (value.length < ListingConstants.minDeposit) {
                      //TODO: internationalize
                      return 'Deposit must be at least ${ListingConstants.minDeposit}';
                    }
                    if (value.length > ListingConstants.maxDeposit) {
                      return 'Deposit must be under ${ListingConstants.maxDeposit}';
                    }
                    return null;
                  },
                ),
                //

                SizedBox(height: 8),
                Text(
                  'Size',
                  style: Theme.of(context).textTheme.headline4,
                ),
                SizedBox(height: 14),
                //TODO: internationalize - allow square meters
                Text('Size of the property in square feet.',
                    style: Theme.of(context).textTheme.subtitle1),
                SizedBox(height: 20),

                TextFormField(
                  autovalidate: true,
                  enabled: !_requestInProgress,
                  initialValue: _squareFeet.toString(),
                  inputFormatters: [FilteringTextInputFormatter.digitsOnly],
                  keyboardType: TextInputType.number,
                  maxLength: 12,
                  style: Theme.of(context).textTheme.headline6,
                  onChanged: (newValue) {
                    if (newValue == '') {
                      setState(() {
                        _squareFeet = 0;
                      });
                    } else {
                      setState(() {
                        _squareFeet = int.parse(newValue);
                      });
                    }
                  },
                  validator: (value) {
                    if (value.length < ListingConstants.minSize) {
                      //TODO: internationalize
                      return 'Size must be at least 0';
                    }
                    if (value.length > ListingConstants.maxSize) {
                      return 'Size must be less than 999,999,999';
                    }
                    return null;
                  },
                ),
                //

                SizedBox(height: 8),
                Text(
                  'Date Available',
                  style: Theme.of(context).textTheme.headline4,
                ),
                SizedBox(height: 14),
                Text('Date that the property will be available for rent.',
                    style: Theme.of(context).textTheme.subtitle1),
                SizedBox(height: 20),

                DateTimeField(
                  format: DateFormat('EEEE, d LLLL y'),
                  initialValue: _dateAvailable,
                  onChanged: (DateTime date) {
                    setState(() {
                      _dateAvailable = date;
                    });
                  },
                  onShowPicker: (context, currentValue) async {
                    final date = await showDatePicker(
                      context: context,
                      firstDate: DateTime(1990),
                      //initialDate: currentValue,
                      initialDate: _dateAvailable,
                      lastDate: DateTime(2100),
                    );
                    return date;
                  },
                ),

                //DateTimeFormField(
                //initialValue: _dateAvailable,
                //label: 'Date Available',
                //onlyDate: true,
                ////TODO: internationalize date
                //formatter: DateFormat('EEEE, d LLLL y'),
                //autovalidate: true,
                //validator: (DateTime date) {
                //if (date == null) {
                //return 'Date Required';
                //}
                ////NOTE: setting autovalidate to true and setting this value
                //// here is a hack, but I like this date picker
                //_dateAvailable = date;
                //print("saving date as $date");
                //},
                //),

                SizedBox(height: 8),
                _failure == null ? SizedBox.shrink() : Text(_failure),
              ]),
            ),
          ),
        ]),
      ),
    );
  }

  bool get canSave {
    if (this._formKey.currentState != null &&
        this._formKey.currentState.validate()) {
      return true;
    }
    return false;
  }

  // saveFields returns true if fields saved successfully
  Future<bool> saveFields(BuildContext context) async {
    String failureText = '';
    var king = King.of(context);
    if (!_requestInProgress) {
      this.setState(() {
        _requestInProgress = true;
      });

      failureText = await this._submitForm(context);

      this.setState(() {
        _requestInProgress = false;
        _failure = failureText;
      });
    }

    king.dad.listingCache.getItemFresh(widget.listing.listingUuid);
    return failureText == '' ? true : false;
  }

  // _submitForm returns an empty string if no errors occurred
  Future<String> _submitForm(BuildContext context) async {
    var king = King.of(context);
    String endpoint = 'agents/listings/${widget.listing.listingUuid}';
    var payload = {
      'date_available': DateFormat('yyyy-MM-dd').format(_dateAvailable),
      'deposit': _deposit,
      'monthly_rent': _monthlyRent,
      'square_feet': _squareFeet,
      'completed_steps':
          widget.listing.getUpdatedCompletedStepsAsList(this.currentStep),
    };

    ApiResponse ares = await king.lip.api(
      endpoint,
      payload: payload,
      method: HttpMethod.patch,
    );

    String failureText = '';
    if (!ares.isOk()) {
      failureText = 'Form failed to save';
    }
    return failureText;
  }
}
