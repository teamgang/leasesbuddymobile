import 'package:flutter/material.dart';

import 'package:exe/dad/listing_cache.dart';
import 'package:exe/king/king.dart';
import 'package:exe/lip/lip.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/erectors/listing/listing_erector_widgets.dart';
import 'package:exe/shared/rooms_oprow_gang.dart';
import 'package:exe/widgets/shared/oprows.dart';
import 'package:exe/widgets/shared/radio_oprows.dart';

class SetRoomsAndSpacesPage extends StatefulWidget {
  static const String currentStep = 'setRoomsAndSpaces';
  Listing listing;
  SetRoomsAndSpacesPage({this.listing});

  @override
  SetRoomsAndSpacesPageState createState() =>
      SetRoomsAndSpacesPageState(currentStep);
}

class SetRoomsAndSpacesPageState extends State<SetRoomsAndSpacesPage> {
  String currentStep;
  String _failure = '';
  final _formKey = GlobalKey<FormState>();
  HousingTypeRadioSet _housingTypeRadioSet;
  ParkingTypeRadioSet _parkingTypeRadioSet;
  //TODO: freeze page if true
  bool _requestInProgress = false;
  RoomsOprowGang _roomsOprowGang;

  SetRoomsAndSpacesPageState(this.currentStep);

  @override
  initState() {
    super.initState();
    _roomsOprowGang = RoomsOprowGang();
    _roomsOprowGang.setOprow('bedrooms', widget.listing.bedrooms);
    _roomsOprowGang.setOprow('bathroomsFull', widget.listing.bathroomsFull);
    _roomsOprowGang.setOprow('bathroomsHalf', widget.listing.bathroomsHalf);

    _housingTypeRadioSet =
        HousingTypeRadioSet(initialSelected: widget.listing.housingType);
    _parkingTypeRadioSet =
        ParkingTypeRadioSet(initialSelected: widget.listing.parkingType);
  }

  @override
  Widget build(BuildContext context) {
    return BasedScaffold(
      body: Form(
        key: _formKey,
        child: CustomScrollView(slivers: <Widget>[
          ListingErectorWidgets.upperSliverAppBar(
            context,
            canSave: this.canSave,
            currentStep: this.currentStep,
            listing: widget.listing,
            saveFields: () async {
              return await this.saveFields(context);
            },
          ),
          ListingErectorWidgets.lowerSliverAppBar(
            context,
            currentStep: this.currentStep,
            canSave: this.canSave,
            listing: widget.listing,
            saveFields: () async {
              return await this.saveFields(context);
            },
          ),
          SliverPadding(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            sliver: SliverList(
              delegate: SliverChildListDelegate([
                SizedBox(height: 8),
                Text(
                  'Rooms and Spaces',
                  style: Theme.of(context).textTheme.headline4,
                ),
                SizedBox(height: 14),
                Text('Set the number of rooms of each type of space.',
                    style: Theme.of(context).textTheme.subtitle1),
                SizedBox(height: 20),
                OprowCluster(title: 'Housing Type', children: <Widget>[
                  OprowsRadio(radioSet: _housingTypeRadioSet),
                ]),
                OprowCluster(title: 'Parking', children: <Widget>[
                  OprowsRadio(radioSet: _parkingTypeRadioSet),
                ]),
                //

                SizedBox(height: 8),
                OprowCluster(title: 'Rooms', children: <Widget>[
                  OprowCounter(_roomsOprowGang.getOprow('bedrooms'),
                      title: 'Bedrooms'),
                  OprowCounter(_roomsOprowGang.getOprow('bathroomsFull'),
                      title: 'Full Bathrooms'),
                  OprowCounter(_roomsOprowGang.getOprow('bathroomsHalf'),
                      title: 'Half Bathrooms'),
                ]),
                Text(_failure),
              ]),
            ),
          ),
        ]),
      ),
    );
  }

  bool get canSave {
    return _roomsOprowGang.isValid;
  }

  Future<bool> saveFields(BuildContext context) async {
    String failureText = '';
    var king = King.of(context);
    if (!_requestInProgress) {
      this.setState(() {
        _requestInProgress = true;
      });

      failureText = await this._submitForm(context);

      this.setState(() {
        _requestInProgress = false;
        _failure = failureText;
      });
    }

    king.dad.listingCache.getItemFresh(widget.listing.listingUuid);
    return failureText == '' ? true : false;
  }

  Future<String> _submitForm(BuildContext context) async {
    var king = King.of(context);
    String endpoint = 'agents/listings/${widget.listing.listingUuid}';
    var payload = {
      'bedrooms': _roomsOprowGang.getOprowValue('bedrooms'),
      'full_baths': _roomsOprowGang.getOprowValue('bathroomsFull'),
      'half_baths': _roomsOprowGang.getOprowValue('bathroomsHalf'),
      'housing_type': _housingTypeRadioSet.selected.value,
      'parking_type': _parkingTypeRadioSet.selected.value,
      'completed_steps':
          widget.listing.getUpdatedCompletedStepsAsList(this.currentStep),
    };

    ApiResponse ares = await king.lip.api(
      endpoint,
      payload: payload,
      method: HttpMethod.patch,
    );

    String failureText = '';
    if (!ares.isOk()) {
      failureText = 'Form failed to save';
    }
    return failureText;
  }
}
