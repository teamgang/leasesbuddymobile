import 'package:flutter/material.dart';

import 'package:exe/dad/listing_cache.dart';
import 'package:exe/king/king.dart';
import 'package:exe/lip/lip.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/erectors/listing/listing_erector_widgets.dart';

class SetTitlePage extends StatefulWidget {
  static const String currentStep = 'setTitle';
  Listing listing;
  SetTitlePage({this.listing});

  @override
  SetTitlePageState createState() => SetTitlePageState(currentStep);
}

class SetTitlePageState extends State<SetTitlePage> {
  String currentStep;
  String _failure = '';
  FocusNode focusNode;
  final _formKey = GlobalKey<FormState>();
  bool _requestInProgress = false;
  String _title;

  SetTitlePageState(this.currentStep);

  @override
  initState() {
    super.initState();
    focusNode = FocusNode();
    focusNode.requestFocus();
    _title = widget.listing.title;
  }

  @override
  dispose() {
    focusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BasedScaffold(
      body: Form(
        key: _formKey,
        child: CustomScrollView(slivers: <Widget>[
          ListingErectorWidgets.upperSliverAppBar(
            context,
            canSave: this.canSave,
            currentStep: this.currentStep,
            listing: widget.listing,
            saveFields: () async {
              return await this.saveFields(context);
            },
          ),
          ListingErectorWidgets.lowerSliverAppBar(
            context,
            currentStep: this.currentStep,
            canSave: this.canSave,
            listing: widget.listing,
            saveFields: () async {
              return await this.saveFields(context);
            },
          ),
          SliverPadding(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            sliver: SliverList(
              delegate: SliverChildListDelegate([
                SizedBox(height: 8),
                Text(
                  'Title',
                  style: Theme.of(context).textTheme.headline4,
                ),
                SizedBox(height: 14),
                Text(
                    'The title is the first description users will see of your property. Be descriptive and creative.',
                    style: Theme.of(context).textTheme.subtitle1),
                SizedBox(height: 20),
                TextFormField(
                  autovalidate: true,
                  enabled: !_requestInProgress,
                  focusNode: focusNode,
                  initialValue: _title,
                  maxLength: ListingConstants.maxTitle,
                  style: Theme.of(context).textTheme.headline6,
                  onChanged: (newValue) {
                    setState(() {
                      _title = newValue;
                    });
                  },
                  validator: (value) {
                    if (value.length < ListingConstants.minTitle) {
                      return 'Title must be at least 8 characters';
                    }
                    if (value.length > ListingConstants.maxTitle) {
                      return 'Title must be under 100 characters';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 8),
                Text(_failure),
              ]),
            ),
          ),
        ]),
      ),
    );
  }

  bool get canSave {
    if (this._formKey.currentState != null &&
        this._formKey.currentState.validate()) {
      return true;
    }
    return false;
  }

  // saveFields returns true if fields saved successfully
  Future<bool> saveFields(BuildContext context) async {
    String failureText = '';
    var king = King.of(context);
    if (!_requestInProgress) {
      this.setState(() {
        _requestInProgress = true;
      });

      failureText = await this._submitForm(context);

      this.setState(() {
        _requestInProgress = false;
        _failure = failureText;
      });
    }

    // update the listing in memory; this will update completedSteps
    king.dad.listingCache.getItemFresh(widget.listing.listingUuid);
    return failureText == '' ? true : false;
  }

  Future<String> _submitForm(BuildContext context) async {
    String failureText = '';
    var king = King.of(context);

    if (widget.listing.isSavedInApi) {
      // update the existing listing
      String endpoint = 'agents/listings/${widget.listing.listingUuid}';
      var payload = {
        'title': _title,
        'completed_steps':
            widget.listing.getUpdatedCompletedStepsAsList(this.currentStep),
      };

      ApiResponse ares = await king.lip.api(
        endpoint,
        payload: payload,
        method: HttpMethod.patch,
      );

      if (!ares.isOk()) {
        failureText = 'Failed to save title.';
      }
    } else {
      // create new listing with this title
      String endpoint = 'agents/listings';
      var payload = {
        'id': widget.listing.listingUuid,
        'title': _title,
        'completed_steps': ["set_title"],
      };

      ApiResponse ares = await king.lip.api(
        endpoint,
        payload: payload,
        method: HttpMethod.post,
      );

      if (ares.isOk()) {
        //TODO: this should be useless now
        //widget.listing.listingUuid = ares.body['id'];
        //listing.isSavedInApi = true;
      } else {
        failureText = 'Failed to make new listing.';
      }
    }
    return failureText;
  }
}
