import 'package:flutter/material.dart';

import 'package:exe/king/king.dart';
import 'package:exe/lip/lip.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/erectors/verify/verify_erector_widgets.dart';

class SetNamePage extends StatefulWidget {
  static const String currentStep = 'setName';

  @override
  SetNamePageState createState() => SetNamePageState(currentStep);
}

class SetNamePageState extends State<SetNamePage> {
  String currentStep;
  String failure = '';
  String firstName = '';
  final _formKey = GlobalKey<FormState>();
  String lastName = '';
  String middleName = '';
  bool requestInProgress = false;

  SetNamePageState(this.currentStep);

  @override
  Widget build(BuildContext context) {
    var king = King.of(context);
    final Args args = ModalRoute.of(context).settings.arguments;
    return BasedScaffold(
      body: Form(
        key: _formKey,
        child: CustomScrollView(slivers: <Widget>[
          VerifyErectorWidgets.upperSliverAppBar(
            context,
            canSave: this.canSave,
            currentStep: this.currentStep,
            saveFields: () async {
              return await this.saveFields(
                context,
                this.firstName,
              );
            },
          ),
          VerifyErectorWidgets.lowerSliverAppBar(
            context,
            currentStep: this.currentStep,
            canSave: this.canSave,
            saveFields: () async {
              return await this.saveFields(
                context,
                this.firstName,
              );
            },
          ),
          SliverPadding(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            sliver: SliverList(
              delegate: SliverChildListDelegate([
                SizedBox(height: 8),
                Text(
                  'Name',
                  style: Theme.of(context).textTheme.headline4,
                ),
                SizedBox(height: 8),
                Text(
                    'To unlock Agent Mode you must verify your identity with LeaseBuddy. Please provide your name:',
                    style: Theme.of(context).textTheme.subtitle1),
                SizedBox(height: 20),
                TextFormField(
                  autovalidate: true,
                  enabled: !this.requestInProgress,
                  maxLength: 80,
                  style: Theme.of(context).textTheme.headline6,
                  decoration: const InputDecoration(
                    icon: Icon(Icons.person),
                    labelText: 'First Name *',
                  ),
                  onChanged: (newValue) {
                    setState(() {
                      this.firstName = newValue;
                    });
                  },
                  validator: (value) {
                    if (value.length < 8) {
                      return 'First name must be at least 1 characters';
                    }
                    if (value.length > 101) {
                      return 'Last name must be under 80 characters';
                    }
                    return null;
                  },
                ),
                SizedBox(height: 8),
                Text(this.failure),
              ]),
            ),
          ),
        ]),
      ),
    );
  }

  bool get canSave {
    if (this._formKey.currentState != null &&
        this._formKey.currentState.validate()) {
      return true;
    }
    return false;
  }

  Future<bool> saveFields(
    BuildContext context,
    String firstName,
  ) async {
    String failureText = '';
    if (!this.requestInProgress) {
      this.setState(() {
        this.requestInProgress = true;
        this.failure = '';
      });

      failureText = await this._submitForm(context, firstName);

      this.setState(() {
        this.requestInProgress = false;
        this.failure = failureText;
      });
    }

    return failureText == '' ? true : false;
  }

  Future<String> _submitForm(
    BuildContext context,
    String firstName,
  ) async {
    var payload = {
      'first_name': this.firstName,
    };

    var king = King.of(context);
    ApiResponse ares = await king.lip.api('agents/${king.user.userUuid}',
        payload: payload, method: HttpMethod.patch);

    if (ares.isOk()) {
    } else {
      failure = 'Failed to submit name';
    }
    return failure;
  }
}
