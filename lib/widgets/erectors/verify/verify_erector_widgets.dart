import 'package:flutter/material.dart';

import 'package:exe/widgets/erectors/verify/set_name.dart';
//import 'package:exe/widgets/erectors/verify/upload_id.dart';

class VerifyErectorWidgets {
  static final Map<String, Widget> widgetMap = {
    'setName': SetNamePage(),
    'uploadId': SetNamePage(),
    //'uploadId': UploadIdPage(),
  };

  static final List<String> steps = [
    'setName',
    'uploadeId',
  ];

  static String get firstStepRoute {
    return 'verify/${steps[0]}';
  }

  static String makeRouteName(String stepName) {
    return 'verify/${stepName}';
  }

  static String getNextStep(String currentStep) {
    int nextIndex = steps.indexOf(currentStep) + 1;
    if (nextIndex < steps.length) {
      return steps[nextIndex];
    } else {
      return '';
    }
  }

  static String getPreviousStep(String currentStep) {
    int prevIndex = steps.indexOf(currentStep) - 1;
    if (prevIndex > -1) {
      return steps[prevIndex];
    } else {
      return '';
    }
  }

  static upperSliverAppBar(
    BuildContext context, {
    bool canSave = false,
    @required String currentStep,
    @required Function saveFields,
  }) {
    return SliverAppBar(
      floating: true,
      pinned: false,
      snap: true,
      leading: InkWell(
          child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 14),
              child: Icon(Icons.close)),
          onTap: () {
            Navigator.of(context).pop();
          }),
    );
  }

  static lowerSliverAppBar(
    BuildContext context, {
    bool canSave = false,
    @required String currentStep,
    @required Function saveFields,
  }) {
    String nextStep = getNextStep(currentStep);
    String previousStep = getPreviousStep(currentStep);
    return SliverAppBar(
      floating: true,
      pinned: false,
      snap: true,
      automaticallyImplyLeading: false,
      actions: <Widget>[
        previousStep != ''
            ? navButton(context,
                canSave: canSave,
                children: <Widget>[
                  Icon(Icons.arrow_left),
                  Text('Previous'),
                ],
                onPressed: canSave
                    ? () async {
                        bool saved = await saveFields();

                        if (saved) {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      widgetMap[previousStep]));
                        }
                      }
                    : null)
            : SizedBox.shrink(),
        //

        nextStep != ''
            ? navButton(context,
                canSave: canSave,
                children: <Widget>[
                  Text('Next'),
                  Icon(Icons.arrow_right),
                ],
                onPressed: canSave
                    ? () async {
                        bool saved = await saveFields();

                        if (saved) {
                          Navigator.pushReplacement(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      widgetMap[nextStep]));
                        }
                      }
                    : null)
            : navButton(context,
                canSave: canSave,
                children: <Widget>[
                  Text('Finish'),
                ],
                onPressed: canSave
                    ? () async {
                        bool saved = await saveFields();
                        if (saved) {
                          Navigator.of(context).pop();
                        }
                      }
                    : null),
      ],
    );
  }

  static Widget navButton(
    BuildContext context, {
    bool canSave,
    @required List<Widget> children,
    @required Function onPressed,
  }) {
    return FlatButton(
      color: Colors.blue,
      textColor: Colors.white,
      //disabledColor: Colors.grey,
      disabledTextColor: Colors.grey,
      padding: EdgeInsets.all(8.0),
      splashColor: Colors.blueAccent,
      onPressed: onPressed,
      child: Row(children: <Widget>[
        ...children,
      ]),
    );
  }
}
