import 'package:flutter/material.dart';

import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/explore/search_mode.dart';

class ExplorePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BasedScaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(height: 28),
          Row(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Expanded(child: SizedBox(width: 1)),
              RaisedButton(
                padding: EdgeInsets.all(14),
                onPressed: () {
                  Navigator.of(context).push(pullUpSearch());
                },
                elevation: 8.0,
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(24),
                ),
                child: Row(
                  children: <Widget>[
                    SizedBox(width: 18),
                    Icon(Icons.search, color: Colors.red, size: 24),
                    SizedBox(width: 6),
                    Text(
                      'Where would you like to live?',
                      style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                    SizedBox(width: 20),
                  ],
                ),
              ),
              Expanded(child: SizedBox(width: 1)),
            ],
          ),
          SizedBox(height: 28),
          Text('Localized Advert 1'),
          Text('Localized Advert 2'),
          Text('Localized Advert 3'),
        ],
      ),
    );
  }
}
