import 'package:flutter/material.dart';

import 'package:exe/shared/explore_query.dart';

class BottomBar extends StatelessWidget {
  ExploreQuery equery;
  ExploreFilterGang transFilterGang;

  BottomBar(this.equery, this.transFilterGang);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 64,
      decoration: BoxDecoration(
        color: Colors.grey[50],
        border: Border.all(
          color: Colors.grey[400],
        ),
      ),
      child: Stack(children: <Widget>[
        Align(
          alignment: FractionalOffset(0.05, 0.5),
          child: FlatButton(
            padding: EdgeInsets.fromLTRB(16, 12, 20, 12),
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            child: Text('Clear all',
                style: TextStyle(fontSize: 14, color: Colors.black)),
            onPressed: () {
              // clear filters and close filter screen
              this.equery.clearFilterGang();
              Navigator.of(context).pop();
            },
          ),
        ),
        Align(
          alignment: FractionalOffset(0.95, 0.5),
          child: FlatButton(
            padding: EdgeInsets.fromLTRB(16, 12, 20, 12),
            color: Colors.black,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
            // TODO: replace ## with result count
            child: Text('Show ## results',
                style: TextStyle(fontSize: 16, color: Colors.white)),
            onPressed: () {
              // save filters and close filter screen
              this.equery.filterGang = this.transFilterGang;
              Navigator.of(context).pop();
            },
          ),
        ),
      ]),
    );
  }
}
