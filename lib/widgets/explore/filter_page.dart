import 'package:flutter/material.dart';
//import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:exe/dad/mapsac_cacher.dart';
import 'package:exe/king/king.dart';
import 'package:exe/shared/explore_query.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/query_map/page.dart';
import 'package:exe/widgets/explore/filter_bottom_bar.dart';
import 'package:exe/widgets/shared/oprows.dart';

class FilterPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var equery = King.of(context).equery;
    var transFilterGang = equery.cloneFilterGang();

    return Scaffold(
      bottomNavigationBar: BottomBar(equery, transFilterGang),
      body: SafeArea(
          child: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            floating: true,
            pinned: false,
            snap: true,
            leading: IconButton(
              tooltip: 'Cancel filter changes',
              icon: Icon(Icons.close),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            //flexibleSpace: FlexibleSpaceBar(
            //title: Text('Search Filters'),
            //),
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              OprowCluster(title: 'Types of places', children: <Widget>[
                OprowCheckbox(transFilterGang.getOprow('apartment'),
                    title: 'Apartment'),
                OprowCheckbox(transFilterGang.getOprow('house'),
                    title: 'House'),
                OprowCheckbox(transFilterGang.getOprow('townhome'),
                    title: 'Townhome'),
              ]),
              OprowCluster(title: 'Rooms', children: <Widget>[
                OprowCounter(transFilterGang.getOprow('bedrooms'),
                    title: 'Bedrooms'),
                OprowCounter(transFilterGang.getOprow('bathrooms'),
                    title: 'Bathrooms'),
              ]),
            ]),
          ),
        ],
      )),
    );
  }
}

Route pullUpFilterPage() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => FilterPage(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(0.0, 1.0);
      var end = Offset.zero;
      var curve = Curves.easeIn;
      var curveTween = CurveTween(curve: curve);
      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
      return SlideTransition(position: animation.drive(tween), child: child);
    },
  );
}
