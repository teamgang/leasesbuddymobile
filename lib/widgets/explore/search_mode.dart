import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';

import 'package:exe/dad/dad.dart';
import 'package:exe/dad/mapsac_cacher.dart';
import 'package:exe/king/king.dart';
import 'package:exe/shared/explore_query.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/query_map/page.dart';

Route pullUpSearch() {
  return PageRouteBuilder(
    pageBuilder: (context, animation, secondaryAnimation) => SearchModePage(),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(0.0, 1.0);
      var end = Offset.zero;
      var curve = Curves.easeIn;
      var curveTween = CurveTween(curve: curve);
      var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
      return SlideTransition(position: animation.drive(tween), child: child);
      // no curve tween
      //var offsetAnimation = animation.drive(tween);
      //return SlideTransition(position: offsetAnimation, child: child);
    },
  );
}

class SearchModePage extends StatefulWidget {
  @override
  SearchModePageState createState() {
    return SearchModePageState();
  }
}

class SearchModePageState extends State<SearchModePage> {
  String _input = '';
  //MapsacPredictionListCacher _predictionsCacher;

  @override
  Widget build(BuildContext context) {
    var king = King.of(context);
    // TODO: lol i think we're destroying our state yo
    //_predictionsCacher = new MapsacPredictionListCacher(king);
    //_predictionsCacher.setFields(
    //input: _input,
    //lat: king.user.gps.lat,
    //long: king.user.gps.long,
    //);

    PredictionList predictionList =
        Dad.of(context).mapsacPredictionListCache.getPredictions(
              input: _input,
              lat: king.user.gps.lat,
              long: king.user.gps.long,
            );

    //PredictionList predictionList = _predictionsCacher.currentPredictions;

    return Scaffold(
      body: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            SizedBox(height: 28),
            Row(
              //mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(
                  flex: 2,
                  child: FlatButton(
                    child: Icon(Icons.arrow_back),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ),
                Flexible(
                  flex: 8,
                  child: TextField(
                    autofocus: true,
                    key: Key('explore/autocomplete'),
                    autocorrect: false,
                    onChanged: (value) {
                      setState(() {
                        _input = value;
                      });
                    },
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                ),
                Flexible(child: SizedBox(width: 0)),
              ],
            ),
            SizedBox(height: 10),
            Text(_input),
            SizedBox(height: 10),
            _buildPredictionList(context, predictionList.predictions),
          ],
        ),
      ),
    );
  }

  Widget _buildPredictionList(
      BuildContext context, ObservableList<Prediction> predictions) {
    return Observer(builder: (BuildContext context) {
      return predictions.isNotEmpty
          ? Expanded(
              child: ListView.builder(
                itemCount: predictions.length,
                itemBuilder: (context, index) {
                  var prediction = predictions[index];

                  return Card(
                    child: InkWell(
                      child: ListTile(
                        leading: FlutterLogo(),
                        title: Text(prediction.address),
                      ),
                      onTap: () {
                        var equery = King.of(context).equery;
                        //equery.lat = prediction.lat;
                        //equery.lng = prediction.long;
                        getGooglePlaceAndSetEquery(
                            context, prediction.placeId, equery);

                        navPush(context, QueryMapPage(query: equery));
                        //Navigator.push(
                        //context,
                        //MaterialPageRoute(
                        //builder: (BuildContext context) =>
                        //QueryMapPage(query: equery)));
                      },
                    ),
                  );
                },
              ),
            )
          : Text('No matching addresses found.');
    });
  }
}
