import 'package:flutter/material.dart';

class ProgressIndicatorGib extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 24,
      width: 24,
      // TODO: figure out wtf is going on with this indicator shape
      margin: EdgeInsets.fromLTRB(0, 16, 12, 16),
      child: Theme(
        data: Theme.of(context).copyWith(accentColor: Colors.white),
        child: CircularProgressIndicator(
          backgroundColor: Colors.green,
        ),
      ),
    );
  }
}
