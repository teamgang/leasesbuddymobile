import 'package:flutter/material.dart';

import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class RatingBarGib extends StatelessWidget {
  bool hasReviews;
  double reviewAverage;
  RatingBarGib({this.hasReviews, this.reviewAverage});

  @override
  Widget build(BuildContext context) {
    if (this.hasReviews) {
      return Row(children: <Widget>[
        RatingBarIndicator(
          rating: this.reviewAverage,
          itemBuilder: (context, index) => Icon(
            Icons.star,
            color: Colors.amber,
          ),
          itemCount: 5,
          itemSize: 30.0,
        ),
        SizedBox(width: 14),
        Text('${reviewAverage.toStringAsFixed(1)}',
            style: TextStyle(fontSize: 16)),
      ]);
    } else {
      return Stack(children: <Widget>[
        RatingBarIndicator(
          rating: 0,
          itemBuilder: (context, index) => Icon(
            Icons.star,
            color: Colors.amber,
          ),
          itemCount: 5,
          itemSize: 30.0,
        ),
        Padding(
            padding: EdgeInsets.symmetric(vertical: 6, horizontal: 39),
            child: Text('No Reviews',
                style: TextStyle(fontStyle: FontStyle.italic))),
      ]);
    }
  }
}
