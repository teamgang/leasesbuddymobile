import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:exe/dad/listing_cache.dart';
import 'package:exe/widgets/based/images.dart';
import 'package:exe/widgets/based/scaffold.dart';

class ListingCarrousselPage extends StatelessWidget {
  Listing listing;
  ListingCarrousselPage(this.listing);

  @override
  Widget build(BuildContext context) {
    return BasedScaffold(
      useBasedAppBar: true,
      body: Container(
        color: Colors.black,
        height: MediaQuery.of(context).size.height - kBottomNavigationBarHeight,
        child: ListingCarroussel(listing),
      ),
    );
  }
}

class ListingCarroussel extends StatefulWidget {
  Listing listing;
  ListingCarroussel(this.listing, {key}) : super(key: key);

  @override
  ListingCarrousselState createState() => ListingCarrousselState();
}

class ListingCarrousselState extends State<ListingCarroussel> {
  PageController controller;

  @override
  initState() {
    super.initState();
    this.controller = PageController(
      initialPage: widget.listing.carrousselIndex,
      viewportFraction: 1,
    );
  }

  @override
  dispose() {
    controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 240,
      child: Observer(
        builder: (_) => Container(
          child: PageView.builder(
              onPageChanged: (value) {
                widget.listing.carrousselIndex = value;
              },
              controller: this.controller,
              itemCount: widget.listing.pictures.length,
              itemBuilder: (context, index) => builder(context, index)),
        ),
      ),
    );
  }

  builder(BuildContext context, int index) {
    return AnimatedBuilder(
      animation: this.controller,
      builder: (context, child) {
        double value = 1.0;
        if (this.controller.position.haveDimensions) {
          value = this.controller.page - index;
          value = (1 - (value.abs() * 0.5)).clamp(0.0, 1.0);
        }

        return Center(
          child: SizedBox(
            height: 240,
            width: MediaQuery.of(context).size.width,
            child: child,
          ),
        );
      },
      child: BasedSizedImage(
        src: widget.listing.pictures[index],
        altText: widget.listing.pictures[index],
        height: 240,
        usePhotoview: true,
      ),
    );
  }
}
