import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:exe/dad/dad.dart';
import 'package:exe/king/king.dart';
import 'package:exe/lip/lip.dart';
import 'package:exe/pac/pac.dart';
import 'package:exe/pac/contact_agent_via_listing_pac.dart';
import 'package:exe/widgets/connect/connect_page.dart';
import 'package:exe/widgets/gibs/progress_indicator.dart';

class ContactAgentViaListingPage extends StatefulWidget {
  Listing listing;
  ContactAgentViaListingPage({this.listing});

  @override
  ContactAgentViaListingPageState createState() =>
      ContactAgentViaListingPageState();
}

final String defaultMessage =
    'Hello! I would like to learn more about this propterty. Thank you!';

class ContactAgentViaListingPageState
    extends State<ContactAgentViaListingPage> {
  String _failure = '';
  int maxLength = 800;
  String message = '';
  FocusNode messageFocusNode;
  String pacUuid = '';
  bool _requestInProgress = false;
  bool useDefaultMessage = true;
  TextEditingController textController;
  // TODO: select list of days available for viewing?

  @override
  initState() {
    super.initState();
    this.pacUuid = makeUuidForContactAgentViaListingPac(
        listingUuid: widget.listing.listingUuid);
    var cached =
        getItemFromPac(context, PacTypes.contactAgentViaListing, this.pacUuid);
    this.message = cached.message;
    this.useDefaultMessage = cached.useDefaultMessage;

    textController = TextEditingController(text: defaultMessage);
    messageFocusNode = FocusNode();
  }

  @override
  dispose() {
    this.pacUuid = makeUuidForContactAgentViaListingPac(
        listingUuid: widget.listing.listingUuid);
    var cached =
        getItemFromPac(context, PacTypes.contactAgentViaListing, this.pacUuid);
    cached.message = this.message;
    cached.useDefaultMessage = this.useDefaultMessage;

    textController.dispose();
    messageFocusNode.dispose();
    super.dispose();
  }

  toggleUseDefaultMessage() {
    if (useDefaultMessage) {
      setState(() {
        this.useDefaultMessage = false;
      });
      this.textController.text = this.message;
    } else {
      setState(() {
        this.useDefaultMessage = true;
      });
      this.textController.text = defaultMessage;
    }
    Timer(Duration(milliseconds: 100), () => messageFocusNode.requestFocus());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.green,
        actions: <Widget>[
          _requestInProgress
              ? ProgressIndicatorGib()
              : SizedBox(height: 0, width: 0),
        ],
      ),
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 14, horizontal: 14),
        child: CustomScrollView(slivers: <Widget>[
          SliverList(
            delegate: SliverChildListDelegate([
              InkWell(
                onTap: () {
                  this.toggleUseDefaultMessage();
                },
                child: Padding(
                  padding: EdgeInsets.fromLTRB(16, 14, 16, 14),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('Customize your request',
                          style: TextStyle(color: Colors.black, fontSize: 20)),
                      Switch(
                        value: !this.useDefaultMessage,
                        onChanged: (value) {
                          this.toggleUseDefaultMessage();
                        },
                      ),
                    ],
                  ),
                ),
              ),
              TextFormField(
                  autovalidate: true,
                  controller: this.textController,
                  enabled: this.useDefaultMessage ? false : true,
                  focusNode: messageFocusNode,
                  keyboardType: TextInputType.multiline,
                  maxLines: 12,
                  maxLength: maxLength,
                  minLines: 6,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Message',
                    counterText: '${this.message.length}/$maxLength',
                  ),
                  onChanged: (message) {
                    setState(() {
                      this.message = message;
                    });
                  },
                  validator: (value) {
                    if (this.textController.text.length > maxLength) {
                      return 'Message is too long';
                    }
                    return null;
                  }),
              SizedBox(height: 10),
              Align(
                alignment: Alignment.topRight,
                child: Card(
                  color: Colors.blue,
                  child: InkWell(
                    child: Container(
                      width: 100,
                      child: ListTile(
                        title: Center(child: Text('Send')),
                      ),
                    ),
                    onTap: () async {
                      bool success = await _submitForm(context);
                      if (success) {
                        Navigator.of(context).pop();
                        navPush(context, ConnectPage());
                      }
                      //navPush(
                      //context,
                      //ContactAgentViaListingPage(
                      //listingUuid: this.listing.listingUuid));
                    },
                  ),
                ),
              ),
              SizedBox(height: 20),
              Text(_failure),
              SizedBox(height: 40),
            ]),
          ),
        ]),
      ),
    );
  }

  Future<bool> _submitForm(BuildContext context) async {
    String failureText = '';
    var king = King.of(context);
    if (!_requestInProgress) {
      this.setState(() {
        _requestInProgress = true;
      });

      failureText = await this._submitToApi(context);

      this.setState(() {
        _requestInProgress = false;
        _failure = failureText;
      });
    }

    king.dad.listingCache.getItemFresh(widget.listing.listingUuid);
    return failureText == '' ? true : false;
  }

  Future<String> _submitToApi(BuildContext context) async {
    var king = King.of(context);
    String endpoint = 'tenant/conversations';
    var payload = {
      'agent_id': widget.listing.agentUuid,
      'listing_id': widget.listing.listingUuid,
    };

    ApiResponse ares = await king.lip.api(
      endpoint,
      payload: payload,
      method: HttpMethod.post,
    );

    String failureText = '';
    if (!ares.isOk()) {
      failureText = 'Failed to send message.';
    }
    return failureText;
  }
}
