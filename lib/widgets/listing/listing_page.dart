import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:exe/dad/dad.dart';
import 'package:exe/king/king.dart';
import 'package:exe/widgets/agent/agent_page.dart';
import 'package:exe/widgets/based/images.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/gibs/rating_bar_gib.dart';
import 'package:exe/widgets/listing/carroussel.dart';
import 'package:exe/widgets/listing/contact_agent_via_listing_page.dart';

class ListingPage extends StatefulWidget {
  String listingUuid;
  ListingPage({this.listingUuid});

  @override
  ListingPageState createState() => ListingPageState();
}

class ListingPageState extends State<ListingPage> {
  var carrousselKey = GlobalKey<ListingCarrousselState>();

  @override
  Widget build(BuildContext context) {
    var listing = King.of(context).dad.listingCache.getItem(widget.listingUuid);

    return BasedScaffold(
      body: CustomScrollView(
        slivers: <Widget>[
          SliverAppBar(
            backgroundColor: Colors.black,
            expandedHeight: 240,
            flexibleSpace: FlexibleSpaceBar(
              background: InkWell(
                  child: ListingCarroussel(listing, key: carrousselKey),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              ListingCarrousselPage(listing)),
                    ).then((value) {
                      carrousselKey.currentState.controller
                          .jumpToPage(listing.carrousselIndex);
                    });
                  }),
            ),
            floating: false,
            pinned: false,
            snap: false,
            leading: IconButton(
              tooltip: 'Back',
              icon: Icon(Icons.close),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ),
          SliverPadding(
            padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
            sliver: SliverList(
              delegate: SliverChildListDelegate([
                ListingAddress(listing),
                SizedBox(height: 20),
                ListingSize(listing, fontSize: 16),
                SizedBox(height: 20),
                ListingAgent(listing),
                SizedBox(height: 20),
                ListingAgency(listing),
                SizedBox(height: 20),
                ListingContactBanner(listing),
                SizedBox(height: 20),
                Text('lease details'),
                SizedBox(height: 20),
                Text('accommodaations'),
                SizedBox(height: 20),
                Text('disability accommodations'),
                SizedBox(height: 20),
                Text(
                    'other special accommodations - lgbt community? active on nextdoor?'),
                SizedBox(height: 20),
                Text('Recommended income'),
                SizedBox(height: 20),
                Text('Similar rentals nearby'),
                SizedBox(height: 20),
                Text('Similar rentals nearby'),
                SizedBox(height: 20),
                Text('Similar rentals nearby'),
                SizedBox(height: 20),
                Text('Similar rentals nearby'),
                SizedBox(height: 20),
                Text('Similar rentals nearby'),
                SizedBox(height: 20),
                Text('Similar rentals nearby'),
                SizedBox(height: 20),
                Text('Similar rentals nearby'),
                SizedBox(height: 20),
                Text('Similar rentals nearby'),
                SizedBox(height: 20),
              ]),
            ),
          ),
        ],
      ),
    );
  }
}

class ListingSize extends StatelessWidget {
  Listing listing;
  double fontSize;
  ListingSize(this.listing, {this.fontSize = 14});

  @override
  Widget build(BuildContext context) {
    TextStyle style = TextStyle(fontSize: this.fontSize);
    return Observer(
      builder: (_) => Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('Beds: ${listing.bedrooms.toString()}', style: style),
            Text('Baths: ${listing.printBathrooms}', style: style),
            Text('${listing.printSize}', style: style),
          ]),
    );
  }
}

class ListingAddress extends StatelessWidget {
  Listing listing;
  ListingAddress(this.listing);

  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) => Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(children: <Widget>[
              Text(listing.printAddressSectionOne,
                  style: TextStyle(fontSize: 20)),
              Text(listing.printAddressSectionTwo),
            ]),
            Text(listing.printMonthlyRent, style: TextStyle(fontSize: 20)),
          ]),
    );
  }
}

class ListingAgency extends StatelessWidget {
  Listing listing;
  ListingAgency(this.listing);

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      if (!this.listing.hasAgency) {
        return SizedBox.shrink();
      } else {
        var agency =
            King.of(context).dad.agencyCache.getItem(this.listing.agencyUuid);
        return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text('Listing Agency:',
                  style: TextStyle(fontSize: 12, fontStyle: FontStyle.italic)),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text('${agency.name}',
                              style: TextStyle(fontSize: 20)),
                          RatingBarGib(
                              hasReviews: agency.hasReviews,
                              reviewAverage: agency.reviewAverage),
                        ]),
                    Text('Pic', style: TextStyle(fontSize: 20)),
                  ]),
            ]);
      }
    });
  }
}

class ListingAgent extends StatelessWidget {
  Listing listing;
  ListingAgent(this.listing);

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      String lead = '';
      if (this.listing.agentIsOwner) {
        lead = 'Property owner:';
      } else {
        lead = 'Property manager:';
      }
      var agent =
          King.of(context).dad.agentCache.getItem(this.listing.agentUuid);

      return Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
          Widget>[
        Text(lead, style: TextStyle(fontSize: 12, fontStyle: FontStyle.italic)),
        Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    InkWell(
                        child: Text('listed by ${agent.name}',
                            style: TextStyle(fontSize: 20)),
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (BuildContext context) =>
                                      AgentPage(agentUuid: agent.agentUuid)));
                        }),
                    RatingBarGib(
                        hasReviews: agent.hasReviews,
                        reviewAverage: agent.reviewAverage),
                  ]),
              Text('Pic', style: TextStyle(fontSize: 20)),
            ]),
      ]);
    });
  }
}

class ListingContactBanner extends StatelessWidget {
  Listing listing;
  ListingContactBanner(this.listing);

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      var agent =
          King.of(context).dad.agentCache.getItem(this.listing.agentUuid);
      return Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              flex: 4,
              child: Card(
                color: Colors.blue,
                child: InkWell(
                  child: ListTile(title: Text('One-tap Apply')),
                  onTap: () {},
                ),
              ),
            ),
            Expanded(child: Container(width: 0, height: 0)),
            Expanded(
              flex: 4,
              child: Card(
                color: Colors.blue,
                child: InkWell(
                  // TODO: if listing has already been contacted, open the
                  // messages
                  child: ListTile(title: Text('Message for Info')),
                  onTap: () {
                    navPush(context,
                        ContactAgentViaListingPage(listing: this.listing));
                  },
                ),
              ),
            ),
          ]);
    });
  }
}
