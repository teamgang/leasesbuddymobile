import 'package:meta/meta.dart';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:exe/king/king.dart';
import 'package:exe/dad/listing_cache.dart';
import 'package:exe/dad/transient/listing.dart';
import 'package:exe/widgets/based/images.dart';
import 'package:exe/widgets/listing/listing_page.dart';

class ListingSummaryRow extends StatelessWidget {
  Listing listing;
  String pushToPage;
  ListingSummaryRow(this.listing, {@required this.pushToPage});

  doPush(BuildContext context) {
    switch (this.pushToPage) {
      case 'listingPage':
        navPush(context, ListingPage(listingUuid: listing.listingUuid));
        break;
      case 'manageListingPage':
        //navPush(context, ListingPage(listingUuid: listing.listingUuid));
        Navigator.pushNamed(
          context,
          'manage/overview',
          arguments: Args(listing: listing),
        );
        break;
      case 'none':
        // TODO: right thing here
        //navPush(context, ConvoPage(listingUuid: listing.listingUuid));
        break;
      default:
        throw ('ERROR: cannot push to ${this.pushToPage}');
    }
  }

  @override
  Widget build(BuildContext context) {
    double imgHeight = 90, imgWidth = 90;
    return Observer(builder: (_) {
      return Row(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            BasedSizedImage(
              src: listing.firstPicture,
              altText: 'pic',
              height: imgHeight,
              width: imgWidth,
            ),
            SizedBox(width: 8),
            Expanded(
              child: InkWell(
                onTap: () {
                  this.doPush(context);
                },
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        listing.title,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 22),
                      ),
                      SizedBox(height: 2),
                      Text(
                        listing.printAddressSectionOne,
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(fontSize: 14),
                      ),
                      Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              listing.printAddressSectionTwo,
                              overflow: TextOverflow.ellipsis,
                              style: TextStyle(fontSize: 14),
                            ),
                            Text(
                              listing.printMonthlyRent,
                              style: TextStyle(fontSize: 14),
                            ),
                          ]),
                      SizedBox(height: 2),
                      ListingSize(listing),
                    ]),
              ),
            ),
          ]);
    });
  }
}

class TransientListingSummaryRow extends StatelessWidget {
  TransientListing transientListing;
  String pushToPage;
  TransientListingSummaryRow(
      {@required this.transientListing, @required this.pushToPage});

  doPush(BuildContext context) {
    switch (this.pushToPage) {
      case 'listingPage':
        navPush(
            context, ListingPage(listingUuid: transientListing.listingUuid));
        break;
      case 'manageListingPage':
        var listing = King.of(context)
            .dad
            .listingCache
            .getItem(transientListing.listingUuid);
        Navigator.pushNamed(
          context,
          'manage/overview',
          arguments: Args(listing: listing),
        );
        break;
      case 'none':
        // TODO: right thing here
        //navPush(context, ConvoPage(listingUuid: listing.listingUuid));
        break;
      default:
        throw ('ERROR: cannot push to ${this.pushToPage}');
    }
  }

  @override
  Widget build(BuildContext context) {
    double imgHeight = 90, imgWidth = 90;
    return Row(crossAxisAlignment: CrossAxisAlignment.start, children: <Widget>[
      BasedSizedImage(
        src: transientListing.firstPicture,
        altText: 'pic',
        height: imgHeight,
        width: imgWidth,
      ),
      SizedBox(width: 8),
      Expanded(
        child: InkWell(
          onTap: () {
            this.doPush(context);
          },
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  transientListing.title,
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: 18),
                ),
                SizedBox(height: 38),
                transientListing.status == 0
                    ? Row(children: <Widget>[
                        Text('Continue drafting'),
                        Icon(Icons.arrow_right),
                      ])
                    : SizedBox.shrink(),
                //Text(
                //transientListing.printAddressSectionOne,
                //overflow: TextOverflow.ellipsis,
                //style: TextStyle(fontSize: 14),
                //),
                //Row(
                //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                //children: <Widget>[
                //Text(
                //transientListing.printAddressSectionTwo,
                //overflow: TextOverflow.ellipsis,
                //style: TextStyle(fontSize: 14),
                //),
                //Text(
                //transientListing.printMonthlyRent,
                //style: TextStyle(fontSize: 14),
                //),
                //]),
                //SizedBox(height: 2),
                //ListingSize(transientListing),
              ]),
        ),
      ),
    ]);
  }
}
