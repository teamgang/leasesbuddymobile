import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:mobx/mobx.dart';

import 'package:exe/dad/dad.dart';
import 'package:exe/dad/listing_cache.dart';
import 'package:exe/dad/transient/listing.dart';
import 'package:exe/king/king.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/erectors/listing/create_listing_page.dart';
import 'package:exe/widgets/listing/summary_row.dart';

class ManagePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var user = King.of(context).user;
    var managedListings =
        King.of(context).dad.managedListingTransientsCache.getItem(
              user.userUuid,
              //path: 'agent/listing_previews',
            );

    return BasedScaffold(
      body: Observer(
        // NOTE: observer is for TabBar()
        builder: (_) => DefaultTabController(
          length: 4,
          child: CustomScrollView(
            shrinkWrap: true,
            slivers: <Widget>[
              SliverAppBar(
                floating: true,
                pinned: false,
                snap: true,
                actions: <Widget>[
                  InkWell(
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 14),
                        child: Center(child: Text('Create Listing')),
                      ),
                      onTap: () {
                        navPush(context, CreateListingPage());
                      }),
                  SizedBox(width: 8),
                ],
                bottom: TabBar(isScrollable: true, tabs: <Widget>[
                  Tab(text: 'Vacant (${managedListings.vacant.length})'),
                  Tab(text: 'Unlisted (${managedListings.unlisted.length})'),
                  Tab(text: 'Rented (${managedListings.rented.length})'),
                  Tab(text: 'Drafts (${managedListings.drafts.length})'),
                ]),
              ),
              SliverToBoxAdapter(child: SizedBox(height: 10)),
              SliverFillRemaining(
                child: TabBarView(children: <Widget>[
                  // TODO: search & filters
                  TabForAgentListings(managedListings.vacant),
                  TabForAgentListings(managedListings.unlisted),
                  TabForAgentListings(managedListings.rented),
                  TabForAgentListings(managedListings.drafts),
                ]),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class TabForAgentListings extends StatelessWidget {
  ObservableList<TransientListing> transientListings;
  TabForAgentListings(this.transientListings);

  @override
  Widget build(BuildContext context) {
    var dad = King.of(context).dad;
    return Observer(builder: (_) {
      return ListView.separated(
          itemCount: this.transientListings.length,
          separatorBuilder: (BuildContext context, int index) =>
              SizedBox(height: 2),
          itemBuilder: (context, index) {
            var transientListing = this.transientListings[index];
            return Card(
              elevation: 5,
              child: TransientListingSummaryRow(
                transientListing: transientListing,
                pushToPage: 'manageListingPage',
              ),
            );
          });
    });
  }
}
