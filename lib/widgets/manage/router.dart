import 'package:flutter/material.dart';

import 'package:exe/king/king.dart';
import 'package:exe/widgets/manage/manage_page.dart';
import 'package:exe/widgets/listing/listing_page.dart';
import 'package:exe/widgets/erectors/listing/overview.dart';
import 'package:exe/widgets/erectors/listing/set_description_page.dart';
import 'package:exe/widgets/erectors/listing/set_location_page.dart';
import 'package:exe/widgets/erectors/listing/set_photos_page.dart';
import 'package:exe/widgets/erectors/listing/set_price_and_size_page.dart';
import 'package:exe/widgets/erectors/listing/set_rooms_and_spaces_page.dart';
import 'package:exe/widgets/erectors/listing/set_title_page.dart';

class ManageRouter extends StatelessWidget {
  Key manageNavKey;
  ManageRouter({this.manageNavKey});

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: this.manageNavKey,
      initialRoute: 'manage',
      onGenerateRoute: (RouteSettings settings) {
        WidgetBuilder builder;
        switch (settings.name) {
          case 'manage':
            builder = (c) => ManagePage();
            break;

          case 'manage/overview':
            builder = (BuildContext context) {
              final Args args = ModalRoute.of(context).settings.arguments;
              King.of(context)
                  .dad
                  .listingCache
                  .getItemFresh(args.listing.listingUuid);
              return ListingDraftOverviewPage(listing: args.listing);
            };
            break;

          case 'manage/preview':
            builder = (BuildContext context) {
              final Args args = ModalRoute.of(context).settings.arguments;
              return ListingPage(listingUuid: args.listing.listingUuid);
            };
            break;

          case 'manage/${SetDescriptionPage.currentStep}':
            builder = (BuildContext context) {
              final Args args = ModalRoute.of(context).settings.arguments;
              return SetDescriptionPage(listing: args.listing);
            };
            break;

          case 'manage/${SetLocationPage.currentStep}':
            builder = (BuildContext context) {
              final Args args = ModalRoute.of(context).settings.arguments;
              return SetLocationPage(listing: args.listing);
            };
            break;

          case 'manage/${SetPhotosPage.currentStep}':
            builder = (BuildContext context) {
              final Args args = ModalRoute.of(context).settings.arguments;
              return SetPhotosPage(listing: args.listing);
            };
            break;

          case 'manage/${SetPriceAndSizePage.currentStep}':
            builder = (BuildContext context) {
              final Args args = ModalRoute.of(context).settings.arguments;
              return SetPriceAndSizePage(listing: args.listing);
            };
            break;

          case 'manage/${SetTitlePage.currentStep}':
            builder = (BuildContext context) {
              final Args args = ModalRoute.of(context).settings.arguments;
              return SetTitlePage(listing: args.listing);
            };
            break;

          case 'manage/${SetRoomsAndSpacesPage.currentStep}':
            builder = (BuildContext context) {
              final Args args = ModalRoute.of(context).settings.arguments;
              return SetRoomsAndSpacesPage(listing: args.listing);
            };
            break;

          default:
            throw Exception('Invalid route: ${settings.name}');
        }
        return MaterialPageRoute(builder: builder, settings: settings);
      },
    );
  }
}
