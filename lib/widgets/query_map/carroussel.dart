import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:exe/shared/explore_query.dart';
import 'package:exe/widgets/listing/listing_page.dart';

class ExploreMapCarroussel extends StatefulWidget {
  ExploreQuery query;
  ExploreMapCarroussel({this.query});

  @override
  ExploreMapCarrousselState createState() => ExploreMapCarrousselState();
}

class ExploreMapCarrousselState extends State<ExploreMapCarroussel> {
  PageController controller;
  // isPageAnimating is used to block the index from changing while PageView
  // is animating. Without it, calling an animation such as animateToPage()
  // will trigger a series of onPageChanged()
  bool isPageAnimating = false;

  @override
  initState() {
    super.initState();
    this.controller = PageController(
      initialPage: widget.query.selectedMarker,
      viewportFraction: 0.70,
    );
    Timer(Duration(milliseconds: 250),
        () => this.animateToPage(widget.query.selectedMarker));

    widget.query.animateToPage = this.animateToPage;
  }

  @override
  dispose() {
    controller.dispose();
    super.dispose();
  }

  animateToPage(int index) async {
    this.isPageAnimating = true;
    await this.controller.animateToPage(
          index,
          duration: const Duration(milliseconds: 400),
          curve: Curves.easeInOut,
        );
    this.isPageAnimating = false;
  }

  onPageViewScroll(int index) {
    if (!this.isPageAnimating) {
      widget.query.selectedMarker = index;
      widget.query.rebuildMarkersWithSelected(index);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      child: Observer(
        builder: (_) => !widget.query.hasMarkers
            ? SizedBox(height: 0)
            : Container(
                child: PageView.builder(
                    onPageChanged: (value) {
                      this.onPageViewScroll(value);
                    },
                    controller: this.controller,
                    itemCount: widget.query.markers.length,
                    itemBuilder: (context, index) => builder(context, index)),
              ),
      ),
    );
  }

  builder(BuildContext context, int index) {
    var markerInfo = widget.query.getMarkerInfoFromIndex(index);

    return AnimatedBuilder(
      animation: this.controller,
      builder: (context, child) {
        double value = 1.0;
        if (this.controller.position.haveDimensions) {
          value = this.controller.page - index;
          value = (1 - (value.abs() * 0.5)).clamp(0.0, 1.0);
        }

        return Center(
          child: SizedBox(
            height: 240,
            width: MediaQuery.of(context).size.width * 0.65,
            child: child,
          ),
        );
      },
      child: Card(
        elevation: 3,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(14)),
        ),
        child: InkWell(
          onTap: () {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (BuildContext context) =>
                        ListingPage(listingUuid: 'listingUuid-01')));
          },
          child: ListTile(
            leading: FlutterLogo(),
            title: Text(markerInfo.address),
          ),
        ),
      ),
    );
  }
}
