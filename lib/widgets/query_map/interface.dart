import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:exe/shared/explore_query.dart';
import 'package:exe/widgets/explore/filter_page.dart';
import 'package:exe/widgets/explore/search_mode.dart';
import 'package:exe/widgets/query_map/carroussel.dart';

class ExploreMapInterface extends StatelessWidget {
  ExploreQuery query;
  ExploreMapInterface({this.query});

  @override
  Widget build(BuildContext context) {
    return Stack(children: <Widget>[
      Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(children: <Widget>[
              SizedBox(height: 12),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Material(
                      color: Colors.yellow[200],
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(8)),
                      child: IconButton(
                        icon: Icon(Icons.close),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ),
                    Observer(
                      builder: (_) => Visibility(
                          visible: this.query.showRefresh,
                          maintainSize: true,
                          maintainState: true,
                          maintainAnimation: true,
                          child: FlatButton.icon(
                            padding: EdgeInsets.fromLTRB(16, 12, 20, 12),
                            color: Colors.yellow[200],
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(8)),
                            icon: Icon(Icons.refresh),
                            label: Text('Search this area'),
                            onPressed: () {
                              this.query.tapRefresh();
                            },
                          )),
                    ),
                    Column(children: <Widget>[
                      Material(
                        color: Colors.yellow[200],
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)),
                        child: IconButton(
                          // TODO: show number of active filters
                          icon: Icon(Icons.tune),
                          onPressed: () {
                            Navigator.of(context).push(pullUpFilterPage());
                          },
                        ),
                      ),
                      SizedBox(height: 12),
                      Material(
                        color: Colors.yellow[200],
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(8)),
                        child: IconButton(
                          icon: Icon(Icons.place),
                          onPressed: () {
                            Navigator.of(context).push(pullUpSearch());
                          },
                        ),
                      ),
                    ]),
                  ]),
            ]),
            Column(children: <Widget>[
              ExploreMapCarroussel(query: this.query),
              SizedBox(height: 12),
            ]),
          ]),
    ]);
  }
}
