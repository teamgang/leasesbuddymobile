import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:exe/shared/explore_query.dart';
import 'package:exe/widgets/query_map/interface.dart';

class ExploreMap extends StatefulWidget {
  ExploreQuery query;
  ExploreMap({this.query});

  @override
  ExploreMapState createState() => ExploreMapState();
}

// TODO: could use a stack so we don't need to reload the map every time?
class ExploreMapState extends State<ExploreMap> {
  GoogleMapController mapController;

  @override
  initState() {
    widget.query.needToCallApi = true;
    widget.query.loadExploreList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    print('building');

    final CameraPosition _initialCameraPosition = CameraPosition(
      target: LatLng(widget.query.lat, widget.query.long),
      zoom: 12,
    );

    return Stack(children: <Widget>[
      Observer(
        builder: (_) => GoogleMap(
          mapType: MapType.normal,
          //liteModeEnabled: true,  // could be useful for miniimages?
          initialCameraPosition: _initialCameraPosition,
          markers: widget.query.markers.toSet(),
          rotateGesturesEnabled: false,
          onCameraIdle: () {
            widget.query.onCameraIdle();
          },
          onMapCreated: (GoogleMapController controller) {
            mapController = controller;
            // TODO: initialLoad = false due to an action instead of timeout
            widget.query.dontTriggerShowRefresh = true;
            Timer(Duration(seconds: 3),
                () => widget.query.dontTriggerShowRefresh = false);
          },
        ),
      ),
      SafeArea(
        child: ExploreMapInterface(query: widget.query),
      ),
    ]);
  }
}
