import 'package:flutter/material.dart';

import 'package:exe/shared/explore_query.dart';
import 'package:exe/widgets/query_map/map.dart';

class QueryMapPage extends StatelessWidget {
  ExploreQuery query;
  QueryMapPage({this.query});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ExploreMap(query: this.query),
    );
  }
}
