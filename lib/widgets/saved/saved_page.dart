import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:exe/dad/dad.dart';
import 'package:exe/dad/listing_cache.dart';
import 'package:exe/king/king.dart';
import 'package:exe/widgets/based/images.dart';
import 'package:exe/widgets/based/scaffold.dart';
import 'package:exe/widgets/listing/listing_page.dart';
import 'package:exe/widgets/listing/summary_row.dart';

class SavedPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var user = King.of(context).user;
    //var saved =
    //King.of(context).dad.savedListingUuidsCache.getItem(user.userUuid);
    var saved =
        Dad.of(context).managedListingTransientsCache.getItem(user.userUuid);

    // TODO: show users how many properties they have saved, somehow
    // TODO: easy way for users to clear rented properties
    return BasedScaffold(
      body: Padding(
        padding: EdgeInsets.symmetric(vertical: 12, horizontal: 8),
        child: CustomScrollView(
          slivers: <Widget>[
            Observer(
              builder: (_) => SliverList(
                delegate: SliverChildBuilderDelegate(
                  (BuildContext context, int index) {
                    if (index % 2 == 0) {
                      //TODO: bring this back in
                      //var uuid = saved.uuids[(index / 2).toInt()];

                      var uuid = saved.vacant[(index / 2).toInt()].listingUuid;
                      var listing = Dad.of(context).listingCache.getItem(uuid);

                      return Card(elevation: 5, child: ListingTile(listing));
                    } else {
                      return const SizedBox(height: 4);
                    }
                  },
                  //childCount: saved.uuids.length * 2 - 1,
                  childCount: saved.vacant.length * 2 - 1,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class ListingTile extends StatelessWidget {
  Listing listing;
  ListingTile(this.listing);

  @override
  Widget build(BuildContext context) {
    double imgHeight = 90, imgWidth = 90;
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          ListingSummaryRow(listing, pushToPage: 'listingPage'),
          SizedBox(height: 2),
          BasedSizedImage(
            src: listing.mapPreviewPicture,
            altText: 'minimap',
            height: imgHeight,
            width: imgWidth,
          ),
        ]);
  }
}
