import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:exe/shared/oprows.dart';

class OprowCheckbox extends StatelessWidget {
  BoolOprow oprow;
  String title;

  OprowCheckbox(this.oprow, {this.title});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        this.oprow.toggle();
      },
      child: Padding(
        padding: EdgeInsets.fromLTRB(16, 14, 16, 14),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text(this.title,
                style: TextStyle(color: Colors.black, fontSize: 20)),
            Observer(
              builder: (_) => Checkbox(
                value: this.oprow.value.b,
                onChanged: (value) {
                  this.oprow.toggle();
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class OprowCounter extends StatelessWidget {
  IntOprow oprow;
  String title;

  OprowCounter(this.oprow, {this.title});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.fromLTRB(16, 14, 16, 14),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: Text(this.title,
                style: TextStyle(color: Colors.black, fontSize: 20)),
          ),
          IconButton(
            icon: Icon(Icons.remove_circle_outline, size: 30),
            onPressed: () {
              this.oprow.decrement();
            },
          ),
          SizedBox(width: 14),
          Observer(
            builder: (_) => Text(this.oprow.value.i.toString(),
                style: TextStyle(fontSize: 20)),
          ),
          SizedBox(width: 14),
          IconButton(
            icon: Icon(Icons.add_circle_outline, size: 30),
            onPressed: () {
              this.oprow.increment();
            },
          ),
        ],
      ),
    );
  }
}

class OprowCluster extends StatelessWidget {
  String title;
  List<Widget> children;
  OprowCluster({this.title, this.children});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.fromLTRB(16, 16, 16, 4),
            child: Text(this.title,
                style: TextStyle(
                    color: Colors.grey[900],
                    fontSize: 20,
                    fontWeight: FontWeight.w600)),
          ),
          ...this.children,
        ],
      ),
    );
  }
}
