import 'package:flutter/material.dart';

class OprowsRadio extends StatefulWidget {
  RadioSet radioSet;
  OprowsRadio({this.radioSet});

  @override
  _OprowsRadioStore createState() => _OprowsRadioStore();
}

class _OprowsRadioStore extends State<OprowsRadio> {
  @override
  Widget build(BuildContext context) {
    return ListView.builder(
      physics: NeverScrollableScrollPhysics(),
      itemCount: widget.radioSet.radios.length,
      shrinkWrap: true,
      itemBuilder: (context, index) {
        var option = widget.radioSet.getAtIndex(index);

        return RadioListTile<RadioOption>(
          title: Text(option.title),
          value: option,
          groupValue: widget.radioSet.selected,
          onChanged: (RadioOption value) {
            setState(() {
              widget.radioSet.selected = value;
            });
          },
        );
      },
    );
  }
}

class RadioOption {
  String title;
  String value;
  RadioOption({this.title, this.value});
}

class RadioSet {
  // final result will be in this.selected.value
  RadioOption selected;
  List<RadioOption> radios;

  RadioSet({String initialSelected}) {
    int index = -1;
    for (int i = 0; i < this.radios.length; i++) {
      var option = this.radios[i];
      if (option.value == initialSelected) {
        index = i;
        break;
      }
    }
    this.selected = index == -1 ? RadioOption() : this.radios[index];
  }

  RadioOption getAtIndex(int index) {
    return this.radios[index];
  }

  int get length {
    return this.radios.length;
  }
}

class HousingTypeRadioSet extends RadioSet {
  @override
  List<RadioOption> radios = [
    RadioOption(title: 'Apartment', value: 'apartment'),
    RadioOption(title: 'Entire Apartment', value: 'entire_apartment'),
    RadioOption(title: 'Room', value: 'room'),
    RadioOption(title: 'House', value: 'House'),
    RadioOption(title: 'Entire House', value: 'entire_house'),
  ];

  HousingTypeRadioSet({String initialSelected})
      : super(initialSelected: initialSelected);
}

class ParkingTypeRadioSet extends RadioSet {
  @override
  List<RadioOption> radios = [
    RadioOption(title: 'Garage', value: 'Garage'),
    RadioOption(title: 'Driveway', value: 'driveway'),
    RadioOption(title: 'Street', value: 'Street'),
    RadioOption(title: 'Parking Deck', value: 'parking_deck'),
    RadioOption(title: 'None', value: 'none'),
  ];

  ParkingTypeRadioSet({String initialSelected})
      : super(initialSelected: initialSelected);
}
