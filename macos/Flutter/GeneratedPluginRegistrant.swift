//
//  Generated file. Do not edit.
//

import FlutterMacOS
import Foundation

import fast_flutter_driver
import firebase_core

func RegisterGeneratedPlugins(registry: FlutterPluginRegistry) {
  WindowUtils.register(with: registry.registrar(forPlugin: "WindowUtils"))
  FLTFirebaseCorePlugin.register(with: registry.registrar(forPlugin: "FLTFirebaseCorePlugin"))
}
