import 'dart:async' show Future;
import 'package:flutter/services.dart';
import 'package:yaml/yaml.dart';

class Conf {
  final String apiUrl = 'http://127.0.0.1:9090/v1/';
  final String env = 'dev';
  final String logLevel = 'info';

  final String tzFormat = 'standard';

  final double defaultLat = 40.7228;
  final double defaultLng = -74.0000;
  final double defaultLatDelta = 0.0230;
  final double defaultLngDelta = 0.0105;
}
