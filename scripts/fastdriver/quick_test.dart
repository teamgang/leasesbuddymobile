import 'dart:convert';

import 'package:fast_flutter_driver/tool.dart';
import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

import 'generic/test_configuration.dart';

void main(List<String> args) {
  FlutterDriver driver;
  final properties = TestProperties(args);

  setUpAll(() async {
    driver = await FlutterDriver.connect(dartVmServiceUrl: properties.vmUrl);
  });

  tearDownAll(() async {
    await driver?.close();
  });

  Future<void> restart() {
    return driver.requestData(
      json.encode(
        TestConfiguration(
          resolution: properties.resolution,
          platform: properties.platform,
        ),
      ),
    );
  }

  group('nav tests', () {
    test('nav to explore', () async {
      await restart();

      //await driver.waitFor(find.byType('ExeApp'));
      final accountNavButton = find.text('Account');
      await driver.waitFor(accountNavButton);
      await driver.tap(accountNavButton);
    });

    test('nav to Inbox', () async {
      await restart();

      final savedNavButton = find.text('Saved');
      await driver.waitFor(savedNavButton);
      await driver.tap(savedNavButton);
    });
  });
}
