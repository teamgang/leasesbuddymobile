package main

import (
	"crypto/tls"
	"log"
	"net/http"
	"os"
)

func serveAssetLinks(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/javascript")
	http.ServeFile(w, req, "./assetlinks.json")
}

func main() {
	//fs := http.FileServer(http.Dir("app"))
	//http.Handle("/static/", fs)
	http.HandleFunc("/.well-known/assetlinks.json", serveAssetLinks)
	//http.HandleFunc("/", serveIndex)

	tlsCfg := &tls.Config{
		PreferServerCipherSuites: true,
		CurvePreferences: []tls.CurveID{
			tls.CurveP256,
			tls.X25519, // Go 1.8 only
		},
		MinVersion: tls.VersionTLS12,
		CipherSuites: []uint16{
			tls.TLS_ECDHE_ECDSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_RSA_WITH_AES_256_GCM_SHA384,
			tls.TLS_ECDHE_ECDSA_WITH_AES_128_GCM_SHA256,
			tls.TLS_ECDHE_RSA_WITH_AES_128_GCM_SHA256,
		},
	}

	log.Println("Listening...")
	argRequired := "oto must be passed an argument of http or https"
	if len(os.Args) == 1 {
		log.Fatal(argRequired)
	}

	if os.Args[1] == "https" {
		srv := &http.Server{
			Addr: ":443",
		}
		srv.TLSConfig = tlsCfg
		log.Fatal(srv.ListenAndServeTLS("./cert.pem", "./key.pem"))
	} else if os.Args[1] == "http" {
		srv := &http.Server{
			Addr: ":80",
		}
		log.Fatal(srv.ListenAndServe())
	} else {
		log.Fatal(argRequired)
	}
}
