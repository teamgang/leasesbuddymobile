import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:exe/king/king.dart';

class BasedAppBar extends StatefulWidget implements PreferredSizeWidget {
  BasedAppBar({Key key})
      : preferredSize = Size.fromHeight(kToolbarHeight),
        super(key: key);

  @override
  final Size preferredSize; // default is 56.0

  @override
  BasedAppBarState createState() => BasedAppBarState();
}

class BasedAppBarState extends State<BasedAppBar> {
  @override
  Widget build(BuildContext context) {
    var king = fromKing(context, Viceroys.king);
    var navman = king.navman;
    final selectedItemColor = Colors.yellow;
    final unselectedItemColor = Colors.grey[100];

    return AppBar(
      backgroundColor: Colors.green,
      actions: <Widget>[
        Expanded(child: SizedBox(width: 10)),
        IconButton(
          icon: Icon(Icons.account_circle,
              color: navman.isCurrentIndex(NavIndices.account)
                  ? selectedItemColor
                  : unselectedItemColor),
          onPressed: () {
            navman.switchToNav(NavIndices.account);
          },
        ),
        Expanded(child: SizedBox(width: 10)),
        IconButton(
          icon: Icon(Icons.camera_roll,
              color: navman.isCurrentIndex(NavIndices.browse)
                  ? selectedItemColor
                  : unselectedItemColor),
          onPressed: () {
            navman.switchToNav(NavIndices.browse);
          },
        ),
        Expanded(child: SizedBox(width: 10)),
        IconButton(
          icon: Icon(Icons.chat_bubble,
              color: navman.isCurrentIndex(NavIndices.convo)
                  ? selectedItemColor
                  : unselectedItemColor),
          onPressed: () {
            navman.switchToNav(NavIndices.convo);
          },
        ),
        Expanded(child: SizedBox(width: 10)),
      ],
    );
  }
}

//class NavLinkWeb extends StatelessWidget {
//String text;
//NavIndices index;
//NavLinkWeb(this.text, this.index);

//@override
//Widget build(BuildContext context) {
//var king = fromKing(context, Viceroys.king);
//return InkWell(
//child: SelectableText(this.text, style: TextStyle(fontSize: 20)),
//onTap: () {
//king.navman.switchToNav(this.index);
//});
//}
//}
