import 'dart:async';
//import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:uni_links/uni_links.dart';

import 'package:exe/king/king.dart';
import 'package:exe/widgets/explore/listing_page.dart';

import 'package:exe/nav_stack.dart';

class AppLinkHandler extends StatefulWidget {
  Widget child;
  AppLinkHandler(this.child) : super(key: key);

  @override
  AppLinkHandlerState createState() => AppLinkHandlerState();
}

class AppLinkHandlerState extends State<AppLinkHandler> {
  Uri uri = Uri();
  StreamSubscription sub;

  @override
  initState() {
    super.initState();
    initPlatformStateForUriUniLinks(context);
  }

  @override
  dispose() {
    if (this.sub != null) this.sub.cancel();
    super.dispose();
  }

  /// An implementation using the [Uri] convenience helpers
  Future<void> initPlatformStateForUriUniLinks(BuildContext context) async {
    // Get the initial Uri
    try {
      print('getting uri');
      this.uri = await getInitialUri();
      print('initial uri: ${this.uri?.path}'
          ' ${this.uri?.queryParametersAll}');
    } on PlatformException {
      print('ERROR: Failed to get initial uri');
    } on FormatException {
      print('ERROR: Bad parse the initial link as uri');
    }

    // Attach a listener to the Uri links stream
    this.sub = getUriLinksStream().listen((Uri uri) {
      print('first stream');
      print('got uri: ${uri?.path} ${uri?.queryParametersAll}');
      if (!mounted) return;
      // TODO: save the link so it can be accessed again in the feed
      this.uri = uri;
      processAppLink(context);
    }, onError: (Object err) {
      print('got err: $err');
      if (!mounted) return;
      this.uri = Uri();
    });
  }

  processAppLink(BuildContext context) {
    var navman = fromKing(context, Viceroys.navman);
    var key = navman.getCurrentKey();
    if (!this.uri.hasEmptyPath) {
      print('============');
      print(uri.path);
      switch (this.uri.path) {
        case '/listing':
          key.currentState.push(MaterialPageRoute(
              builder: (BuildContext context) =>
                  ListingPage(listingUuid: 'listingUuid-01')));
          break;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
