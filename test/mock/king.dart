import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:exe/conf.dart';
import 'package:exe/king/king.dart';

King mockKing() {
  Conf conf = Conf(whichApi: ApiChoices.web, mockAutoSignIn: false);
  King king = King(conf: conf);
  return king;
}

printSuccess([String name = '']) {
  print('.-.-.-.-.-. $name TEST SUCCESSFUL  .-.-.-.-.');
}

//printText(WidgetTester tester) {
//var finder = find.text(search);
printText(Finder finder) {
  var text = finder.evaluate().single.widget as TextFormField;
  print(text.toString());
}
