import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

void navToCreateEvent(WidgetTester tester) async {
  await tester.tap(find.text('Sign in as 2'));
  await tester.pump();

  await tester.tap(find.byIcon(Icons.local_activity));
  await tester.pump();
}
