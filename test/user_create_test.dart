import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import 'package:exe/conf.dart';
import 'package:exe/main.dart';
import 'package:exe/king/king.dart';

import 'mock/king.dart';

void main() {
  HttpOverrides.global = null;
  testWidgets('TestName: Create User', (WidgetTester tester) async {
    await tester.pumpWidget(ExeApp(king: mockKing()));
    await tester.pumpAndSettle();

    expect(find.byIcon(Icons.account_circle), findsNWidgets(4));
    await tester.tap(find.byIcon(Icons.account_circle));
    await tester.pumpAndSettle();

    var emailField = find.byKey(Key('authForm/email'));
    expect(emailField, findsOneWidget);
    await tester.enterText(emailField, 'a@a.a');
    await tester.pumpAndSettle();
    // have to wait a second time as profile is loaded asynchronously and has a wait
    await tester.pumpAndSettle();

    var passwordField = find.byKey(Key('authForm/password'));
    expect(passwordField, findsOneWidget);
    await tester.enterText(passwordField, 'a@a.a');
    await tester.pumpAndSettle();
    // have to wait a second time as profile is loaded asynchronously and has a wait
    await tester.pumpAndSettle();

    //debugDumpApp();
    printSuccess();
  });
}
