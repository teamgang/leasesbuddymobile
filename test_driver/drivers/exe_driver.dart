import 'dart:convert';
import 'package:meta/meta.dart';
import 'package:fast_flutter_driver/tool.dart'; // for TestProperties
import 'package:flutter_driver/flutter_driver.dart';
import 'package:http/http.dart' as http;

import '../conf.dart';
import '../generic/test_configuration.dart'; // fast_futter

//TODO: implement these
// waitForAbsent

//NOTE: to follow stacktrace of catchError, look in the left column for the
// name of the function of the currently running test. For example, if main()
// is running in users_test.dart, look for a line that says main.<fn>.<fn>
class ExeDriver {
  FlutterDriver _driver;
  final properties;

  ExeDriver({this.properties});

  Future<void> clearTimeline({Duration timeout: kUnusuallyLongTimeout}) async {
    await _driver.clearTimeline(timeout: timeout);
  }

  Future<void> close() async {
    _driver.close();
  }

  Future<void> enterText(String text, {Duration timeout}) async {
    await _driver.enterText(text, timeout: timeout).catchError((e) {
      throw Exception('ERROR: enterText failed for text: ${text}');
    });
  }

  Future<String> requestData(String message, {Duration timeout}) async {
    //TODO: should i await this before returning it?
    return _driver.requestData(message, timeout: timeout);
  }

  Future<void> tap(SerializableFinder finder, {Duration timeout}) async {
    await _driver.tap(finder, timeout: timeout).catchError((e) {
      throw Exception('ERROR: tap failed for finder: ${finder.toString()}');
    });
  }

  Future<void> waitFor(SerializableFinder finder, {Duration timeout}) async {
    await _driver.waitFor(finder, timeout: timeout).catchError((e) {
      throw Exception('ERROR: waitFor failed for finder: ${finder.toString()}');
    });
  }

  Future<void> printHealth() async {
    Health health = await _driver.checkHealth();
    print('health: ${health.status}');
  }

  Future<void> scrollUntilVisible(
    SerializableFinder scrollable,
    SerializableFinder item, {
    double alignment: 0.0,
    double dxScroll: 0.0,
    double dyScroll: 0.0,
    Duration timeout,
  }) async {
    await _driver
        .scrollUntilVisible(
      scrollable,
      item,
      alignment: alignment,
      dxScroll: dxScroll,
      dyScroll: dyScroll,
      timeout: timeout,
    )
        .catchError((e) {
      throw Exception('ERROR: failed to scroll to finder: ${item.toString()}');
    });
  }

  // Call at the start of every test
  Future<void> resetAndPrep() async {
    await this._restart();
    await this._resetDatabase();
    await this.clearTimeline();
    await this.waitFor(find.byType('ExeApp'));
  }

  Future<void> _resetDatabase() async {
    http.Response response =
        await http.Client().get('${conf.apiUrlNoSuffix}admin/reset');
  }

  // Generic function from fast_driver docs (iirc)
  Future<void> _restart() {
    return _driver.requestData(
      json.encode(
        TestConfiguration(
          resolution: this.properties.resolution,
          platform: this.properties.platform,
        ),
      ),
    );
  }

  static Future<ExeDriver> connect({
    @required TestProperties properties,
    String dartVmServiceUrl,
    bool printCommunication: false,
    bool logCommunicationToFile: true,
    int isolateNumber,
    Pattern fuchsiaModuleTarget,
    Duration timeout,
    Map<String, dynamic> headers,
  }) async {
    var driver = ExeDriver(properties: properties);
    driver._driver = await FlutterDriver.connect(
        dartVmServiceUrl: dartVmServiceUrl,
        printCommunication: printCommunication,
        logCommunicationToFile: logCommunicationToFile,
        isolateNumber: isolateNumber,
        fuchsiaModuleTarget: fuchsiaModuleTarget,
        timeout: timeout,
        headers: headers);
    return driver;
  }
}
