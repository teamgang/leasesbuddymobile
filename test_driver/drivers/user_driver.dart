import 'package:flutter_driver/flutter_driver.dart';

import 'exe_driver.dart';
import '../utils.dart';
import '../xfinder.dart';

class UserDriver {
  ExeDriver driver;
  String email;
  String password;
  UserDriver(this.driver, {this.email, this.password});

  Future<void> registerOrSignIn() async {
    await driver.waitFor(XFinder.navProfile);
    await driver.tap(XFinder.navProfile);

    await driver.waitFor(XFinder.authFormEmailField);
    await driver.tap(XFinder.authFormEmailField);
    await driver.enterText(this.email);

    await driver.waitFor(XFinder.authFormPasswordField);
    await driver.tap(XFinder.authFormPasswordField);
    await driver.enterText(this.password);

    await Task.delay(secs: 1);

    await driver.waitFor(XFinder.authFormRegisterOrSignInButton);
    await driver.tap(XFinder.authFormRegisterOrSignInButton);

    //final signInTabButton = find.byValueKey(XKeys.authFormSignInTabButton);
    //await driver.waitFor(signInTabButton);
    //await driver.tap(signInTabButton);
  }

  Future<void> signOut() async {
    await driver.waitFor(XFinder.navProfile);
    await driver.tap(XFinder.navProfile);

    // use dxScroll for horizontal list
    await driver.scrollUntilVisible(
      XFinder.profileOptionsList,
      XFinder.profileSignOutButton,
      dyScroll: -300.0,
    );
    await driver.waitFor(XFinder.profileSignOutButton);
    await driver.tap(XFinder.profileSignOutButton);
  }
}
