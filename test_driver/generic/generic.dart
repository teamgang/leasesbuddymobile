import 'dart:convert';

import 'package:fast_flutter_driver/driver.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/scheduler.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_driver/driver_extension.dart';

import 'test_configuration.dart';
import 'package:exe/app.dart';
import 'package:exe/king/king.dart';

import '../conf.dart';

void main() {
  debugDefaultTargetPlatformOverride = TargetPlatform.fuchsia;
  timeDilation = 0.1;
  enableFlutterDriverExtension(
    handler: (playload) => configureTest(
      TestConfiguration.fromJson(json.decode(playload)),
    ),
  );

  King king = King(conf: conf);
  runApp(
    RestartWidget<TestConfiguration>(
      builder: (_, config) => ExeApp(king: king),
    ),
  );
}
