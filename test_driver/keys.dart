//NOTE: importing anything flutter related will crash tests
class Xkeys {
  static String authFormRegisterTabButton = 'authForm/registerTab';
  static String authFormSignInTabButton = 'authForm/signInTabButton';
  static String authFormRegisterOrSignInButton =
      'authFormRegisterOrSignInButton';
  static String authFormEmailField = 'authForm/emailField';
  static String authFormPasswordField = 'authForm/passwordField';

  static String togglePovButton = 'togglePovButton';
  static String profileSignOutButton = 'profileSignOutButton';

  static String navManage = 'navManage';
  static String navExplore = 'navExplore';
  static String navFavorite = 'navFavorite';
  static String navConnect = 'navConnect';
  static String navProfile = 'navProfile';
}
