import 'package:fast_flutter_driver/tool.dart'; // for TestProperties
import 'package:test/test.dart';

import 'drivers/exe_driver.dart';
import 'drivers/user_driver.dart';
import 'utils.dart';

void main(List<String> args) {
  ExeDriver driver;
  final properties = TestProperties(args);

  setUpAll(() async {
    driver = await ExeDriver.connect(
        properties: properties, dartVmServiceUrl: properties.vmUrl);
  });

  tearDownAll(() async {
    await driver?.close();
  });

  group(
      'GROUP++create user then agent, make listing, then contact through listing++',
      () {
    test('create first user', () async {
      await driver.resetAndPrep();

      var user = UserDriver(driver, email: '4@4.4', password: 'password');
      await user.registerOrSignIn();
      await driver.printHealth();
      await user.signOut();
    }, timeout: Timeout(Duration(seconds: 10)));

    test('nav to saved tab', () async {
      //await restart();

      //final savedNavButton = find.text('Saved');
      //await driver.waitFor(savedNavButton);
      //await driver.tap(savedNavButton);
    }, timeout: Timeout(Duration(seconds: 10)));
  });
}
