# Run the tests/seed the project

## Mobile

To test on iOS or Android, launch an Android Emulator, iOS Simulator, or connect your computer to a real iOS / Android device.

Then, run the following command from the root of the project:

`flutter drive --target=test_driver/app.dart`

This command performs the following:
* Builds the --target app and installs it on the emulator / device.
* Launches the app.
* Runs the app_test.dart test suite located in test_driver/ folder.


## Web

To test for web, determine which browser you want to test against and download the corresponding web driver:

* Chrome: Download ChromeDriver
* Firefox: Download GeckoDriver
* Safari: Safari can only be tested on a Mac; the SafariDriver is already installed on Mac machines.
* Edge Download EdgeDriver

Launch the WebDriver, for example:

`./chromedriver --port=4444`

From the root of the project, run the following command:

`flutter drive --target=test_driver/app.dart --browser-name=[browser name] --release`

To simulate different screen dimensions, you can use the --browser-dimension argument, for example:

`flutter drive --target=test_driver/app.dart --browser-name=chrome --browser-dimension 300,550 --release`

Will run the tests in the chrome browser in a window with dimensions 300 by 550.


