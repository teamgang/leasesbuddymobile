import 'package:flutter/material.dart';
import 'package:flutter_driver/driver_extension.dart';

import 'package:exe/app.dart' as exeApp;
import 'package:exe/conf.dart';
import 'package:exe/king/king.dart';

void main() {
  // This line enables the extension.
  enableFlutterDriverExtension();

  // Call the `main()` function of the app, or call `runApp` with
  // any widget you are interested in testing.
  //Conf conf = Conf(whichApi: ApiChoices.web, mockAutoSignIn: false);
  //King king = King(conf: conf);
  //runApp(ExeApp(king: king));
  runApp(exeApp.ExeApp());
}
