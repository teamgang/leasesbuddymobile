import 'package:flutter_driver/flutter_driver.dart';
import 'package:test/test.dart';

void main(List<String> args) {
  FlutterDriver driver;

  Future<void> delay([int milliseconds = 250]) async {
    await Future<void>.delayed(Duration(milliseconds: milliseconds));
  }

  // Connect to the Flutter driver before running any tests.
  setUpAll(() async {
    driver = await FlutterDriver.connect();
  });

  // Close the connection to the driver after the tests have completed.
  tearDownAll(() async {
    if (driver != null) {
      driver.close();
    }
  });

  test('check flutter driver health', () async {
    final health = await driver.checkHealth();
    expect(health.status, HealthStatus.ok);
  });

  test('sign in anonymously, sign out', () async {
    final accountNavButton = find.text('Account');
    await driver.waitFor(accountNavButton);
    await driver.tap(accountNavButton);

    //final anonymousSignInButton = find.byValueKey(Keys.anonymous);
    //// Check to fail early if the auth state is authenticated
    //await driver.waitFor(anonymousSignInButton);
    ////await delay(1000); // for video capture
    //await driver.tap(anonymousSignInButton);

    //final logoutButton = find.byValueKey(Keys.logout);
    //await driver.waitFor(logoutButton);
    ////await delay(1000); // for video capture
    //await driver.tap(logoutButton);

    //final confirmLogoutButton = find.byValueKey(Keys.alertDefault);
    //await driver.waitFor(confirmLogoutButton);
    ////await delay(1000); // for video capture
    //await driver.tap(confirmLogoutButton);

    //// try to find anonymous sign in button again
    //await driver.waitFor(anonymousSignInButton);
  });
}
