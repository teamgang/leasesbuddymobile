class Task {
  //TODO default from config
  static delay({int secs = 1}) async {
    await Future.delayed(Duration(seconds: secs), () {});
  }
}
