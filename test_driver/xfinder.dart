import 'package:flutter_driver/flutter_driver.dart';

import '../lib/king/xkeys.dart';

class XFinder {
  static final authFormRegisterTabButton =
      find.byValueKey(XKeys.authFormRegisterTabButton);
  static final authFormSignInTabButton =
      find.byValueKey(XKeys.authFormSignInTabButton);
  static final authFormRegisterOrSignInButton =
      find.byValueKey(XKeys.authFormRegisterOrSignInButton);
  static final authFormEmailField = find.byValueKey(XKeys.authFormEmailField);
  static final authFormPasswordField =
      find.byValueKey(XKeys.authFormPasswordField);

  static final erectAgentConfirmNo = find.byValueKey(XKeys.erectAgentConfirmNo);
  static final erectAgentConfirmYes =
      find.byValueKey(XKeys.erectAgentConfirmYes);
  static final erectAgentStart = find.byValueKey(XKeys.erectAgentStart);

  static final profileOptionsList = find.byValueKey(XKeys.profileOptionsList);
  static final profileSignOutButton =
      find.byValueKey(XKeys.profileSignOutButton);
  static final profileTogglePovButton =
      find.byValueKey(XKeys.profileTogglePovButton);

  static final navManage = find.byValueKey(XKeys.navManage);
  static final navExplore = find.byValueKey(XKeys.navExplore);
  static final navFavorite = find.byValueKey(XKeys.navFavorite);
  static final navConnect = find.byValueKey(XKeys.navConnect);
  static final navProfile = find.byValueKey(XKeys.navProfile);
}
